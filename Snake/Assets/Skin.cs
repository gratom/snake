﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Skin : MonoBehaviour
{
    public int index;
    public Image[] images;
    public ColorTemplate colorTemp;
    public float[] numbers;
    public HorizontalLayoutGroup layout;
    public MaskGUIScript maskGui;
    public Image img;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupSkin(ColorTemplate template)
    {
        colorTemp = template;
        int colorCount = template.colors.Length;
        for (int x = 0; x < images.Length; x++)
        {
            if (x < colorCount)
            {
                images[x].gameObject.SetActive(true);
                images[x].color = template.colors[x];
            }
            else
                images[x].gameObject.SetActive(false);
        }

        layout.spacing = numbers[colorCount-2];
    }

    public void SelectSkin()
    {
        img.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
        maskGui.SetSkin(this);
    }

    public void Deselect()
    {
        img.color = new Color(0, 0, 0, 0);
    }
}
