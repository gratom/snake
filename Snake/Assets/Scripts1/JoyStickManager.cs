﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;
using UnityEngine.UI;

public class JoyStickManager : MonoBehaviour
{
  // Use this for initialization
  public SimpleJoystick selectedJoyStick;
  public SimpleJoystick leftJoyStick;
  public SimpleJoystick rightJoyStick;
  public ArrowController leftArrowController;
  public ArrowController rightArrowController;
  public Image arrowImage;

  void OnEnable()
  {
    CheckJoyStickSettings();
  }

  public void CheckJoyStickSettings()
  {
        if (PlayerPrefs.GetInt(GameUtils.PREFS_JOYSTICK_CONTROL_MODE, 0) == 0)  //0
        {
            //Debug.Log("Inside if joystickControl Mode = 0");
            leftJoyStick.gameObject.SetActive(false);
            rightJoyStick.gameObject.SetActive(false);
            arrowImage.gameObject.SetActive(true);

            if (PlayerPrefs.GetInt("initialJ", 0) == 0)
            {
                PlayerPrefs.SetInt("initialJ", 1);
                PlayerPrefs.SetInt(GameUtils.PREFS_JOYSTICK_CONTROL_HAND, 1);
            }

            if (PlayerPrefs.GetInt(GameUtils.PREFS_JOYSTICK_CONTROL_HAND, 1) == 1)
            {
                //Debug.Log("Inside if control mode = 0,  if joystickControl Hand = 0");
                leftArrowController.gameObject.SetActive(true);
                rightArrowController.gameObject.SetActive(false);
            }
            else
            {
                //Debug.Log("Inside if controlMode = 0, else joystickControl Hand = 1");
                leftArrowController.gameObject.SetActive(false);
                rightArrowController.gameObject.SetActive(true);
            }
        }
        else
        {
            //Debug.Log("Inside else joystickControl Mode = 1");
            leftArrowController.gameObject.SetActive(false);
            rightArrowController.gameObject.SetActive(false);
            arrowImage.gameObject.SetActive(false);
      
            if (PlayerPrefs.GetInt(GameUtils.PREFS_JOYSTICK_CONTROL_HAND, 1) == 1)
            {
                //Debug.Log("Inside else controlmode = 1,  if joystickControl Hand = 0");
                leftJoyStick.gameObject.SetActive(true);
                rightJoyStick.gameObject.SetActive(false);
                selectedJoyStick = leftJoyStick;
            }
            else
            {
                //Debug.Log("Inside else controlmode = 1,  if joystickControl Hand = 1");
                leftJoyStick.gameObject.SetActive(false);
                rightJoyStick.gameObject.SetActive(true);
                selectedJoyStick = rightJoyStick;
            }

            selectedJoyStick.MoveBase = false;
            selectedJoyStick.HideOnRelease = false;
            selectedJoyStick.SnapsToFinger = false;
            foreach (Transform t in selectedJoyStick.gameObject.transform)
            {
                t.gameObject.SetActive(true);
            }

            selectedJoyStick.gameObject.transform.GetChild(2).gameObject.SetActive(true);
        }
  }
}