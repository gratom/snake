﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class Population : MonoBehaviour
{
    public int small;   //20;
    public int medium;  //10;
    public int big; //5;
    public int reallybig;   //5;
    public int superbig;    //1;
    public int MaxPopulation;   //20
    public static Population instance;
    public float spawnCircleLenght;   //300
    public float minimalSpawnDistanceToPlayer = 60f;  
    // public GameObject snakePrefab;
    public int updateScoreFrequency = 0;

    public Text[] nameText, scoreText;
    public Text[] nameTextDuel, scoreTextDuel;
    public Text[] teamsScore,teamsLabels;
    public GameObject[] teamsScoreObj;
  //  public List<Snake> allSnakes;
    // Dictionary<Snake, string> currSnakes = new Dictionary<Snake, string>();

    string[] defaultNames = new string[]{"I should study", "Lol", "Pop", "Your mooom", "eater eatest", "nom nom nom",
        "longer", "lilli", "areum", "kom", "nuna", "ophidiophobia", "viper", "anaconda", "remember me?", "ignore me !!",
        "YOUR KING", "Eat Me", "What’s this?", "smile more", "cari", "beea", "Im lost", "undeath", "fluffy", "eat him",
        "boom", "(*_*)", "Help! I’m Lost!", "I’m Doomed", "((^▽^))", "AlaskanBullWorm", "Ion lee", "babayaga", "babushka",
        "antonio demaciado", "lodesea", "playas", "the D", "I m batman!", "your bla bla", "jsjsjsjs"};

    public List<string> tempNames = new List<string>();
    public string topperName = "none";
    public string lastTopperName = "none";

    public float transparentPercent;
    public float noMaskPercent;
    public float transparentCount;
    public float noMaskCount;
    public float snakeCounter;
    public Vector3 duelBot1_position;
    public Vector3 duelBot2_position;
    public Vector3 duelBot3_position;
    public Vector3 duelPlayer_position;
    public Vector3[] duelMode_Position;
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        SetDuelPositions();
        snakeCounter = 1;
        transparentCount = (transparentPercent / 100) * SnakeManager.instance.totalSnakes;
        noMaskCount = (noMaskPercent / 100) * SnakeManager.instance.totalSnakes;
        noMaskCount = Mathf.RoundToInt(noMaskCount);
        Initialize();
    }

    private void SetDuelPositions()
    {
        var lowerLeftScreen = new Vector2(0, 0);
        var lowerRightScreen = new Vector2(Screen.width, 0);
        var upperLeftScreen = new Vector2(0, Screen.height);
        var upperRightScreen = new Vector2(Screen.width, Screen.height);

        duelPlayer_position = Camera.main.ScreenToWorldPoint(lowerLeftScreen) + GameManager.instance.duelScreenOffsetPosition;
        duelBot1_position = Camera.main.ScreenToWorldPoint(lowerRightScreen) - new Vector3(GameManager.instance.duelScreenOffsetPosition.x , 0f, -GameManager.instance.duelScreenOffsetPosition.z); 
        duelBot2_position = Camera.main.ScreenToWorldPoint(upperRightScreen) - GameManager.instance.duelScreenOffsetPosition;
        duelBot3_position = Camera.main.ScreenToWorldPoint(upperLeftScreen) + new Vector3(GameManager.instance.duelScreenOffsetPosition.x, 0f, -GameManager.instance.duelScreenOffsetPosition.z);

        duelPlayer_position.y = duelBot1_position.y = duelBot2_position.y = duelBot3_position.y = 0f;
    }

    public void StopAllRoutines()
    {
        StopAllCoroutines();
        StopCoroutine("checkSpawnSnake");
        StopCoroutine("spawnSnakePopulation");
    }

    public void Initialize()
    {
        StartCoroutine(ImposeDelay());
    }

    IEnumerator ImposeDelay()
    {
        yield return new WaitForSeconds(.5f);
        tempNames = defaultNames.ToList();
        //        currSnakes.Clear();
        //  currSnakes = new Dictionary<Snake, string>();
        //allSnakes = new List<Snake>();
      /*  while (SnakeSpawner.Instance.snakes.Count < SnakeManager.instance.totalSnakes)
        {
            if (SnakeSpawner.Instance.snakes.Count == SnakeManager.instance.totalSnakes)
                break;
           // Debug.Log(SnakeManager.instance.notUsedSnakes.Count + " : number " + SnakeManager.instance.totalSnakes);
            yield return null;
        }*/

        while (!SnakeManager.instance.isLoaded)
        {
            yield return null;
        }

        // yield return new WaitForSeconds(0.5f);
        SpawnPopulation();
        //InvokeRepeating("UpdatePerSec", 0, 1f);
    }

    public void DestroyAllSnakes()
    {
     /*   for (int i = 0; i < allSnakes.Count; i++)
        {
            allSnakes[i].gameObject.SetActive(false);
        }*/
    }

    public void SpawnPopulation()
    {
        Debug.Log("spawning snake population");
        StopCoroutine("checkSpawnSnake");
        StopCoroutine("spawnSnakePopulation");
        StartCoroutine("spawnSnakePopulation");
    }

    public void StopSnakeLoading()
    {
        StopAllCoroutines();
       // Debug.Log("Snake loading stopped");
    }

    IEnumerator spawnSnakePopulation()
    {
        if (!GameManager.instance.IsTeamMode())
        {

        //    for (int i = 0; i < mega; i++)
       //     {
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(Random.Range(25000, 35000), Snake.snakeType.superbig, "");  //10000
                    yield return new WaitForSeconds(0.5f);
                }
         //   }

            for (int i = 0; i < superbig; i++)
            {
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(10000, Snake.snakeType.superbig, "");  //10000
                    yield return new WaitForSeconds(0.5f);
                }
            }


            for (int i = 0; i < reallybig; i++)
            {
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(6000, Snake.snakeType.reallybig, "");
                    yield return new WaitForSeconds(0.5f);
                }
            }

            for (int i = 0; i < big; i++)
            {
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(4000, Snake.snakeType.big, "");
                    yield return new WaitForSeconds(0.5f);
                }
            }

            for (int i = 0; i < medium; i++)
            {
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(Random.Range(1000, 2000), Snake.snakeType.medium, "");
                    yield return new WaitForSeconds(0.5f);
                }
            }

            for (int i = 0; i < small; i++)
            {
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "");
                    yield return new WaitForSeconds(0.5f);
                }

            }
        }
        else
        {

            if (GameManager.instance.teamMode == TeamMode._2x2)
            {
                for (int x = 0; x < 31; x++)
                {
                    string nSelectedTeam = "B";
                    Debug.Log(((x + 2) % 2).ToString());
                    if ((x + 2) % 2 == 0)
                        nSelectedTeam = "A";
                   

                        SpawnSnakeAITeam(nSelectedTeam, "2/4/6/8/11");
                    yield return new WaitForSeconds(0.5f);
                }

                
            }
            else
            {
                for (int x = 0; x < 32; x++)
                {
                    string nSelectedTeam = "C";
                    Debug.Log(((x +3) % 3).ToString());
                    if ((x + 3) % 3 == 0)
                        nSelectedTeam = "A";
                    else if ((x + 3) % 3 == 1)
                        nSelectedTeam = "B";


                        SpawnSnakeAITeam(nSelectedTeam, "1/3/4/9/14");
                    yield return new WaitForSeconds(0.5f);
                }

           /*     for (int x = 0; x < 10; x++)
                {
                    SpawnSnakeAITeam("A", "1/1/1/4/3");
                    yield return new WaitForSeconds(0.5f);
                }

                for (int x = 0; x < 11; x++)
                {
                    SpawnSnakeAITeam("B", "1/1/1/4/4");
                    yield return new WaitForSeconds(0.5f);
                }

                for (int x = 0; x < 11; x++)
                {
                    SpawnSnakeAITeam("C", "1/1/1/4/4");
                    yield return new WaitForSeconds(0.5f);
                }*/

            }
        }
     //   Debug.Log("spawning snake with check up");
        StartCoroutine("checkSpawnSnake");
    }

    public void SpawnSnakeAITeam(string team, string numbers)
    {
        string[] nNums = numbers.Split('/');
        int superbigSnakes = int.Parse(nNums[0]);
        int realybigSnakes = int.Parse(nNums[1]);
        int bigSnakes = int.Parse(nNums[2]);
        int medSnakes = int.Parse(nNums[3]);
        int smallSnakes = int.Parse(nNums[4]);

        int NsuperbigSnakes = 0;
        int NrealybigSnakes = 0;
        int NbigSnakes = 0;
        int NmedSnakes = 0;
        int NsmallSnakes = 0;
        int x = 0;
        for(int i= 0;i< SnakeSpawner.Instance.snakes.Length; i++)
        {
            
            if (SnakeSpawner.Instance.snakes[i] != null)
            {
                ECSSnake ecsSnake = SnakeSpawner.Instance.snakes[i];
                if (!ecsSnake.isDestroyed)
                {
                    if (ecsSnake.points>=10000)
                        NsuperbigSnakes++;

                    if (ecsSnake.points >= 6000)
                        NrealybigSnakes++;

                    if (ecsSnake.points >= 4000)
                        NbigSnakes++;

                    if (ecsSnake.points >= 1000)
                        NmedSnakes++;

                    if (ecsSnake.points >= 150)
                        NsmallSnakes++;
                }
            }
        }

        if (NsuperbigSnakes < superbigSnakes)
            x = 0;
        else if (NrealybigSnakes < realybigSnakes)
            x = 1;
        else if (NbigSnakes < bigSnakes)
            x = 2;
        else if (NmedSnakes < medSnakes)
            x = 3;
        else if (NsmallSnakes < smallSnakes)
            x = 4;

            switch (x)
            {
                case 0:
                   
                        if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                        {
                            SpawnSnake(10000, Snake.snakeType.superbig, team);  //10000
                            return;
                        }
                   
                    break;
                case 1:
                   
                        if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                        {
                            SpawnSnake(6000, Snake.snakeType.reallybig, team);  //10000
                            return;
                        }
                 
                    break;
                 case 2:
                  
                        if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                        {
                            SpawnSnake(4000, Snake.snakeType.big, team);  //10000
                            return;
                        }
                   
                    break;
                case 3:
                   
                        if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                        {
                            SpawnSnake(Random.Range(1000, 2000), Snake.snakeType.medium, team);  //10000
                            return;
                        }
                  
                    break;
                case 4:
                  
                        if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                        {
                            SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, team);  //10000
                            return;
                        }
                   
                    break;

            }
        
    }

    public void AddSnakesDuel()
    {
     //   SnakeManager.instance.usedSnakes.Clear();
        //int counter = 0;
        //var length = SnakeManager.instance.notUsedSnakes.Count;
        //int i = 0;

        for (int i = 1; i < 4; i++)
        {
            ColorTemplate colorChosen = SnakeSpawner.Instance.stockColorTemplates[Random.Range(0, SnakeSpawner.Instance.stockColorTemplates.Count)];//SkinManager._instance.availableColorTemplate[Random.Range(2, SkinManager._instance.availableColorTemplate.Length - 1)];


            bool ifMask = (Random.Range(0, 3) == 0);

            SnakeSpawner.Instance.CreateNewSnake(150, GetRandomName(), duelMode_Position[i], colorChosen, ifMask ? MaskManager.instance.GetRandomMask() : null, false);
            // newsnake.SetActive(true);
          //  Debug.Log("snake is created successfully");
          //  SnakeManager.instance.InstantiateForDuel(i);
            //var snakeObject = SnakeManager.instance.usedSnakes[0].GetComponent<Snake>();     //SnakeManager.instance.notUsedSnakes[i].GetComponent<Snake>();
           
            //if (snakeObject.gameObject.activeInHierarchy == false && 
              //  snakeObject.val == Snake.snakeType.small)
            //{
                //SnakeManager.instance.InstantiateSnake_Duel(Vector3.zero, Quaternion.identity,snakeObject);
     

                //snakeObject.enabled = false;
                //counter++;
                ////i++;
                //if (counter > 2)
                //{
                    
                //    break;
                //}
            //Debug.Log("Counter:- " + counter);
            // }
        }
    }

    public void SpawnSnake(int points, Snake.snakeType type,string team="")
    {
      //  Debug.Log("spawning snake");
        if (SnakeSpawner.Instance.GetSnakesRealCount() >= MaxPopulation)
            return;

    //    Debug.Log(type.ToString());
    //    GameObject newsnake = null;
    //    Snake snakeparams = null;
       
   //     newsnake = SnakeManager.instance.InstantiateSnake(Vector3.zero, Quaternion.identity);
        

        //int tryCount = 2;
      //  Vector3 spawnPoint;
    //    var randomSpawnCircleVector2 = Random.insideUnitCircle * spawnCircleLenght;
      //  spawnPoint = new Vector3(randomSpawnCircleVector2.x, 0,
       //     randomSpawnCircleVector2.y);
        /*   Vector3 spawnPoint;
           do
           {
               var randomSpawnCircleVector2 = Random.insideUnitCircle * spawnCircleLenght;
               spawnPoint = new Vector3(randomSpawnCircleVector2.x, newsnake.transform.position.y,
                   randomSpawnCircleVector2.y);

               tryCount--;
               if (tryCount < 0)
               {
                   spawnPoint = Vector3.zero;
                   break;
               }
             //  Debug.Log("finding spot");
           }
           while (IsSpawnNearPlayer(spawnPoint));
           Vector3 spawningPos = Vector3.zero;
           if (spawnPoint != Vector3.zero)
               spawningPos = spawnPoint;
           else
           {
               Transform snakePosition = SnakeManager.instance.GetRandomSpawnPoint(snakeparams);
               spawningPos = snakePosition.position;
           }*/
    //   Transform snakePosition = SnakeManager.instance.GetRandomSpawnPoint(snakeparams);
     //  spawningPos = snakePosition.position;
        ColorTemplate colorChosen = SnakeSpawner.Instance.stockColorTemplates[Random.Range(0, SnakeSpawner.Instance.stockColorTemplates.Count)];//SkinManager._instance.availableColorTemplate[Random.Range(2, SkinManager._instance.availableColorTemplate.Length - 1)];
        // Debug.Log("snake is spawning at : " + spawnPoint);
        if (team!="")
            colorChosen = SkinManager._instance.GetTeamBasedColorTemplate(team);

        bool ifMask = (Random.Range(0, 4) == 0);
        SnakeSpawner.Instance.CreateNewSnake(points, GetRandomName(), Vector3.zero, colorChosen,ifMask ? MaskManager.instance.GetRandomMask() : null,false, team);
        // newsnake.SetActive(true);
        Debug.Log("snake is created successfully");

      //  snakeparams = newsnake.GetComponent<Snake>();
       
     /*   if (team != "")
        {
            snakeparams.snakeTeam = team;
            snakeparams.aiModule.aiType = AIType.Team;
            snakeparams.colorTemplate = null;
            snakeparams.ApplySnakeColor();
        }

        snakeparams.val = type;
        if (snakeCounter <= noMaskCount)
        {
            snakeparams.hasNoMask = true;
        }
        
        snakeparams.points = points;
        snakeparams.snakeHead.IsPlayer = false;
        snakeparams.isPlayer = false;
        
        
        if(snakeCounter <= transparentCount)
        {
            snakeparams.isTransparent = true;
        }*/

        snakeCounter++;

          
        /* Transform snakePosition = SnakeManager.instance.GetRandomSpawnPoint(snakeparams);
       //  newsnake.transform.position = snakePosition.position;
          if (snakePosition == null)
          {
              //  Debug.Log(newsnake.transform.position.y);
                Vector3 spawnPoint;
                do
                {
                    var randomSpawnCircleVector2 = Random.insideUnitCircle * spawnCircleLenght;
                    spawnPoint = new Vector3(randomSpawnCircleVector2.x, newsnake.transform.position.y,
                        randomSpawnCircleVector2.y);
                }
                while (IsSpawnNearPlayer(spawnPoint));
               newsnake.transform.position = spawnPoint;
          }
          else
          {

              newsnake.transform.position = snakePosition.position;
          }*/
        //  Debug.Log("Is Snake active in heirarcy " + newsnake.activeInHierarchy);
        // snakeparams.immuneTimer = 1f;

      //  AddSnake(snakeparams, GetRandomName());

    }

    private bool IsSpawnNearPlayer(Vector3 spawnPoint)
    {
        if (GameManager.instance != null)
        {
            if (GameManager.instance.GetPlayerSnakeParams() != null)
            {
                return GameManager.instance.IsPlayerActive && Vector3.Distance(spawnPoint, GameManager.instance.GetPlayerSnakeParams().GetSnakeHeadPosition()) < minimalSpawnDistanceToPlayer;
            }
            else
                return false;
        }
        else
            return false;
    }

   /* public Snake SpawnRemoteSnake(string name, float posx, float posy, int points)
    {
        if (allSnakes.Count >= MaxPopulation)
        {
            foreach (Snake snake in allSnakes)
            {
                if (snake.IsAutoPlayer())
                {
                    allSnakes.Remove(snake);
                    GameManager.instance.DestroySnake(snake);
                    break;
                }
            }
        }
        GameObject newsnake = null;
        Snake snakeparams = null;
        newsnake = SnakeManager.instance.InstantiateSnake(Vector3.zero, Quaternion.identity);
        snakeparams = newsnake.GetComponent<Snake>();
        snakeparams.points = points;
        snakeparams.snakeHead.IsPlayer = false;
        snakeparams.isPlayer = false;
        newsnake.transform.position = new Vector3(posx, newsnake.transform.position.y, posy);
        AddSnake(snakeparams, name);
        return snakeparams;
    }*/

    public string GetRandomName()
    {
        if (tempNames.Count == 0)
        {
            tempNames = defaultNames.ToList();
        }
        int rand = Random.Range(0, tempNames.Count);
        var randName = tempNames[rand];
        tempNames.RemoveAt(rand);

        return randName;
    }

 /*   public void AddSnake(Snake snake, string name)
    {
        snake.snakeName = name.ToString();
       
        allSnakes.Add(snake);
        // currSnakes.Add(snake, name);
    }*/

 /*   public void RemoveSnake(Snake snake)
    {
        allSnakes.Remove(snake);
       
    }*/

    public void StopAllSnakeIncrease()
    {
        StopCoroutine("checkSpawnSnake");
        StopCoroutine("spawnSnakePopulation");
    }

    IEnumerator checkSpawnSnake()
    {
        Debug.Log("Spawner started");
        while (true)
        {
            Debug.Log("checking");
            if (GameManager.instance.IsTeamMode())
            {
               SnakeManager.instance.TeamASnakes = 0;
                SnakeManager.instance.TeamBSnakes = 0;
                SnakeManager.instance.TeamCSnakes = 0;
                for (int x = 0; x < SnakeSpawner.Instance.snakes.Length; x++)
                {
                    if (SnakeSpawner.Instance.snakes[x] != null)
                    {
                        if (!SnakeSpawner.Instance.snakes[x].isDestroyed)
                        {
                            if (SnakeSpawner.Instance.snakes[x].team == "A")
                                SnakeManager.instance.TeamASnakes++;
                            else if (SnakeSpawner.Instance.snakes[x].team == "B")
                                SnakeManager.instance.TeamBSnakes++;
                            else if (SnakeSpawner.Instance.snakes[x].team == "C")
                                SnakeManager.instance.TeamCSnakes++;
                        }
                    }
                }

                if (GameManager.instance.teamMode == TeamMode._2x2)
                {
                    if (SnakeManager.instance.TeamASnakes < 15)
                    {
                        SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "A");
                    }

                    if (SnakeManager.instance.TeamBSnakes < 16)
                        SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "B");
                }
                else if (GameManager.instance.teamMode == TeamMode._3x3)
                {
                    if (SnakeManager.instance.TeamASnakes < 10)
                    {
                        SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "A");
                    }

                    if (SnakeManager.instance.TeamBSnakes < 11)
                        SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "B");

                    if (SnakeManager.instance.TeamCSnakes < 11)
                        SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "C");
                }
            }
            else
            {
                //Debug.Log("Check spawn snake running!");
                if (SnakeSpawner.Instance.GetSnakesRealCount() < MaxPopulation)
                {
                    SpawnSnake(Random.Range(150, 500), Snake.snakeType.small, "");

                }

              

            }

            if (SnakeSpawner.Instance.playerSnake==null)
            {
                //AddSnake(GameManager.instance.playerSnake.GetComponent<Snake>(), PlayerPrefs.GetString("PlayerName", "You"));
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    public void UpdateScoreBoard()
    {
        if (SnakeSpawner.Instance.GetSnakesRealCount() < 6 && !GameManager.instance.IsDuelMode())
            return;

        if (GameManager.instance.IsTeamMode())
        {
            teamsScore[0].text = GetTotalTeamScore("A").ToString();
            teamsScore[1].text = GetTotalTeamScore("B").ToString();
            teamsScore[0].color = SkinManager._instance.teamBasedColorTemplate[0].colors[0];
            teamsScore[1].color = SkinManager._instance.teamBasedColorTemplate[1].colors[0];
            teamsLabels[0].color = SkinManager._instance.teamBasedColorTemplate[0].colors[0];
            teamsLabels[1].color = SkinManager._instance.teamBasedColorTemplate[1].colors[0];
            if (GameManager.instance.teamMode == TeamMode._3x3)
            {
                teamsScore[2].text = GetTotalTeamScore("C").ToString();
                teamsScoreObj[2].SetActive(true);
                teamsScore[2].color = SkinManager._instance.teamBasedColorTemplate[2].colors[0];
                teamsLabels[2].color = SkinManager._instance.teamBasedColorTemplate[2].colors[0];
            }
            else
                teamsScoreObj[2].SetActive(false);
        }
        else
        {

            Dictionary<string, int> individualSnakes = new Dictionary<string, int>();
            individualSnakes.Clear();
            foreach (ECSSnake snake in SnakeSpawner.Instance.snakes)
            {
                if (snake != null)
                {
                    if (!snake.isDestroyed)
                    {
                        if (!individualSnakes.ContainsKey(snake.snakeName))
                            individualSnakes.Add(snake.snakeName, snake.points);
                    }
                }
            }

            List<KeyValuePair<string, int>> scoreList = individualSnakes.ToList();

            scoreList.Sort(
                delegate (KeyValuePair<string, int> pair1,
                    KeyValuePair<string, int> pair2)
                {
                    return pair1.Value.CompareTo(pair2.Value);
                }
            );

            if (GameManager.instance.IsDuelMode())
            {
                int highScoreDisplayCount = nameTextDuel.Length;
                if (highScoreDisplayCount >= 1 && individualSnakes.Count > 0)
                {
                    for (int i = 1; i <= highScoreDisplayCount; i++)
                    {
                        if (scoreList.Count - i >= 0)
                        {
                            if (i == 1)
                                topperName = scoreList[scoreList.Count - i].Key.ToString();
                            var t = i - 1;
                            var d = scoreList.Count;
                            nameTextDuel[i - 1].text = scoreList[scoreList.Count - i].Key.ToString();
                            scoreTextDuel[i - 1].text = scoreList[scoreList.Count - i].Value.ToString();
                        }
                        else
                        {
                            nameTextDuel[i - 1].text = "-";
                            scoreTextDuel[i - 1].text = "-";
                        }
                    }
                }

                if (lastTopperName != topperName)
                    UpdateCrownedSnake(topperName);
            }
            else
            {

                int highScoreDisplayCount = nameText.Length;
                if (highScoreDisplayCount >= 1 && individualSnakes.Count > 0)
                {
                    for (int i = 1; i <= highScoreDisplayCount; i++)
                    {
                        if (i == 1)
                            topperName = scoreList[scoreList.Count - i].Key.ToString();
                        var t = i - 1;
                        var d = scoreList.Count;
                        nameText[i - 1].text = scoreList[scoreList.Count - i].Key.ToString();
                        scoreText[i - 1].text = scoreList[scoreList.Count - i].Value.ToString();//individualSnakes [scoreList [scoreList.Count - i].ToString()].ToString();
                    }
                }

                if (lastTopperName != topperName)
                    UpdateCrownedSnake(topperName);


            }
        }
    }

    public int GetTotalTeamScore(string team)
    {
        int totalTeamScore = 0;
        for (int x = 0; x < SnakeSpawner.Instance.snakes.Length; x++)
        {
            
            if (SnakeSpawner.Instance.snakes[x] != null)
            {
                ECSSnake snake = SnakeSpawner.Instance.snakes[x];
                if (snake.team == team)
                    totalTeamScore += snake.points;
            }
        }

        return totalTeamScore;

    }

    public void UpdateCrownedSnake(string _topperName)
    {
        string lastTopperNameFound = lastTopperName;
        lastTopperName = topperName;
        topperName = _topperName;
        for (int j = 0; j < SnakeSpawner.Instance.snakes.Length; j++)
        {
            if (SnakeSpawner.Instance.snakes[j] != null)
            {
                ECSSnake snake = SnakeSpawner.Instance.snakes[j];
                if (snake.snakeName == lastTopperNameFound)
                {
                    SnakeSpawner.Instance.ToggleCrown(snake, false);
                }
                if (snake.snakeName == topperName)
                {
                    SnakeSpawner.Instance.ToggleCrown(snake, true);
                }
            }
        }
    }

    void FixedUpdate()
    {
        //if (!GameManager.instance.isDuelModeOn)
        //{
        if (SnakeSpawner.Instance == null)
            return;

        if (SnakeSpawner.Instance.snakes == null)
            return;

            if (SnakeSpawner.Instance.GetSnakesRealCount() > 0)
            {
                if (updateScoreFrequency % 10 == 0)
                {
                    UpdateScoreBoard();
                }
                updateScoreFrequency++;
            }
        }
   // }
}
