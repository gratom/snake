﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class FoodManager : MonoBehaviour
{
    public int foodQuantity;
    float spawnRange;
    public float spawnRangeDuel;
    float staticFoodMinSize = 2f;
    float staticFoodMaxSize = 6f;
    public static FoodManager instance;
    public GameObject foodPrefab;
    public Color[] foodColorRandomList;
    public List<Food> foodDiedList = new List<Food>();
    public bool isduelModeOn = false;

    void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        isduelModeOn = GameManager.instance.IsDuelMode();
        StartCoroutine(SpawnAllFood());
    }

    IEnumerator SpawnAllFood()
    {
        yield return new WaitForSeconds(0.5f);
        if (GameManager.instance.IsDuelMode())
        {
            spawnRange = spawnRangeDuel;
            foodQuantity = 60;
        }
        else
        {
            spawnRange = Population.instance.spawnCircleLenght;

        }

        ResetAllFood();
        for (int i = 0; i < foodQuantity; i++)
        {
            SpawnFood(Random.Range(staticFoodMinSize, staticFoodMaxSize), foodColorRandomList[Random.Range(0, foodColorRandomList.Length)], Vector3.zero);
        }
    }

    public void ResetAllFood()
    {
        foodDiedList.Clear();
        foreach (Food food in FindObjectsOfType<Food>())
        {
            Destroy(food.gameObject);
        }
    }

    public void SpawnFood(float power, Color foodColor, Vector3 spawningPosition, bool randomPosition = true)
    {
        //Search free food
        GameObject newFood = null;
        if (foodDiedList.Count > 0)
        {
            foodDiedList[0].enabled = true;
            newFood = foodDiedList[0].gameObject;
            foodDiedList[0].col.enabled = true;
            foodDiedList.RemoveAt(0);
        }
        else
        {
            newFood = (GameObject)Instantiate(foodPrefab, Vector3.zero, foodPrefab.transform.rotation);
        }
        foodColor.a = 1f;
        Food newFoodParameter = newFood.GetComponent<Food>();
        newFoodParameter.enabled = true;
        newFoodParameter.Init();
        newFoodParameter.SetSize(power);
        newFoodParameter.SetColor(foodColor);
        if (randomPosition)
        {
            MoveTransformOnCircle(newFood.transform);
        }
        else
        {
            newFood.transform.position = spawningPosition;
        }
        newFood.transform.parent = this.transform;
    }

    public void MoveTransformOnCircle(Transform foodTransform)
    {
        if (isduelModeOn)
        {
            Vector3 screen = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, Screen.height));
            var width = Random.Range(-screen.x, screen.x);
            var height = Random.Range(-screen.z, screen.z);
            foodTransform.position = new Vector3(width, 0, height);
        }
        else
        {
            Vector3 rangeVector = Random.onUnitSphere * spawnRange;
            rangeVector.y = 0;
            foodTransform.position = Vector3.zero + rangeVector;
        }

    }
}
