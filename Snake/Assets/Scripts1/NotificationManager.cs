//using System;
////using DeadMosquito.AndroidGoodies;
////using DeadMosquito.AndroidGoodies.Internal;   //Commented out recently
//using UnityEngine;

//namespace Scripts1
//{
//    public class NotificationManager : MonoBehaviour
//    {
//#if UNITY_ANDROID && !UNITY_EDITOR
//            private static int NOTIFICATION_ID_1 = 1001;
//            private static int NOTIFICATION_ID_2 = 1002;
//            private static int NOTIFICATION_ID_3 = 1003;
//            private static int NOTIFICATION_ID_4 = 1004;
//            private static int NOTIFICATION_ID_5 = 1005;
//            private static int NOTIFICATION_ID_6 = 1006;
//            private static int NOTIFICATION_ID_7 = 1007;
//            private static string NOTIFICATION_CHANNEL_ID = "slitherworm_channel_id";
//            private static string NOTIFICATION_CHANNEL_NAME = "Slither Worm Channel";
//            private static string NOTIFICATION_GROUP_ID = "slitherworm_group_id";
//            private static string NOTIFICATION_GROUP_NAME = "Slither Worm Group";

//            private string _lastChannelId;
//            private string _lastGroupId;

//            private void Start()
//            {
//              AGNotificationManager.CancelAll();
//              CreateScheduledNotification();
//            }

//            public void CreateScheduledNotification()
//            {
//              var time = DateTime.Now;
//              time = time.AddDays(1);
//              AGNotificationManager.Notify(NOTIFICATION_ID_1, CreateBaseNotificationBuilder().Build(), time, 0);

//              var time2 = DateTime.Now;
//              time2 = time2.AddDays(2);
//              AGNotificationManager.Notify(NOTIFICATION_ID_2, CreateBaseNotificationBuilder().Build(), time2, 1);

//              var time3 = DateTime.Now;
//              time3 = time3.AddDays(3);
//              AGNotificationManager.Notify(NOTIFICATION_ID_3, CreateBaseNotificationBuilder().Build(), time3, 2);

//              var time4 = DateTime.Now;
//              time4 = time4.AddDays(4);
//              AGNotificationManager.Notify(NOTIFICATION_ID_4, CreateBaseNotificationBuilder().Build(), time4, 3);

//              var time5 = DateTime.Now;
//              time5 = time5.AddDays(5);
//              AGNotificationManager.Notify(NOTIFICATION_ID_5, CreateBaseNotificationBuilder().Build(), time5, 4);

//              var time6 = DateTime.Now;
//              time6 = time6.AddDays(6);
//              AGNotificationManager.Notify(NOTIFICATION_ID_6, CreateBaseNotificationBuilder().Build(), time6, 5);

//              var time7 = DateTime.Now;
//              time7 = time7.AddDays(7);
//              AGNotificationManager.Notify(NOTIFICATION_ID_7, CreateBaseNotificationBuilder().Build(), time7, 6);

//            }

//            Notification.Builder CreateBaseNotificationBuilder()
//            {
//              if (AGNotificationManager.AreNotificationChannelsSupported)
//              {
//                CreateNotificationChannel();
//              }

//              var channelId = AGNotificationManager.AreNotificationChannelsSupported ? _lastChannelId : null;
//              var groupId = AGNotificationManager.AreNotificationChannelsSupported ? _lastGroupId : null;
//#pragma warning disable 0618
//              var builder = new Notification.Builder(channelId)
//                .SetGroup(groupId)
//                .SetGroupAlertBehavior(Notification.GroupAlert.All)
//                .SetChannelId(channelId)
//          			.SetSmallIcon("notify_icon_small")
//                .SetLocalOnly(true)
//                .SetBadgeIconType(Notification.BadgeIcon.Small)
//                .SetContentTitle("Snaker.io")
//                .SetContentText("Don't miss out the new masks. Play now!")
//                .SetPriority(Notification.Priority.High)
//                .SetVibrate(new long[] {1000, 100, 1000, 100, 100})
//                .SetDefaults(Notification.Default.All)
//                .SetWhen(DateTime.Now.ToMillisSinceEpoch())
//                .SetShowWhen(true)
//                .SetSortKey("xyz")
//                .SetAutoCancel(true)
//                .SetVisibility(Notification.Visibility.Public);
//#pragma warning restore 0618

//              return builder;
//            }

//            public void CreateNotificationChannel()
//            {
//              if (NotificationChannelsApiCheck())
//              {
//                return;
//              }

//              var group = new NotificationChannelGroup(NOTIFICATION_GROUP_ID, NOTIFICATION_GROUP_NAME)
//              {
//                Description = "Slither Worm notification channel group"
//              };
//              AGNotificationManager.CreateNotificationChannelGroup(group);
//              _lastGroupId = group.Id;

//              var channel = NewNotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, group.Id);
//              channel.ShowBadge = true;
//              AGNotificationManager.CreateNotificationChannel(channel);

//              _lastChannelId = channel.Id;
//            }

//            NotificationChannel NewNotificationChannel(string channelId, string channelName, string group)
//            {
//              var channel = new NotificationChannel(channelId, channelName, AGNotificationManager.Importance.Low)
//              {
//                Group = group,
//                Description = "Slither Worm notification channel",
//                VibrationPattern = new long[] {0, 400, 1000, 600, 1000, 800, 1000, 1000},
//                BypassDnd = true,
//                ShowBadge = true,
//                LightColor = Color.red,
//                EnableLights = true,
//                EnableVibration = true,
//                LockscreenVisibility = Notification.Visibility.Public
//              };

//              return channel;
//            }

//            static bool NotificationChannelsApiCheck()
//            {
//              if (!AGNotificationManager.AreNotificationChannelsSupported)
//              {
//                return true;
//              }

//              return false;
//            }
//#endif
//    }
//}