using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsPanel : MonoBehaviour
{
  [Header("Manager")] 
  [SerializeField] private PlayerStatsManager _playerStatsManager;
  
  [Header("Best")] 
  [SerializeField] private Text _bestScore;
  [SerializeField] private Text _bestKills;
  [SerializeField] private Text _bestTopOne;
  [SerializeField] private Text _bestTime;

  [Header("Last")] 
  [SerializeField] private Text _lastScore;
  [SerializeField] private Text _lastKills;
  [SerializeField] private Text _lastTopOne;
  [SerializeField] private Text _lastTime;

  private void OnEnable()
  {
    var bestSurvivingTime = TimeSpan.FromTicks(_playerStatsManager.LoadBestSurvivalTime());
    var lastSurvivingTime = TimeSpan.FromTicks(_playerStatsManager.LoadLastSurvivalTime());
    
    var bestTopOneTime = TimeSpan.FromTicks(_playerStatsManager.LoadBestTopOneTime());
    var lastTopOneTime = TimeSpan.FromTicks(_playerStatsManager.LoadLastTopOneTime());

    _bestScore.text = _playerStatsManager.LoadBestScore().ToString();
    _bestKills.text = _playerStatsManager.LoadBestKills().ToString();
    _bestTime.text = string.Format("{0:D2}:{1:D2}:{2:D2}", bestSurvivingTime.Hours, bestSurvivingTime.Minutes, bestSurvivingTime.Seconds);
    _bestTopOne.text = string.Format("{0:D2}:{1:D2}:{2:D2}", bestTopOneTime.Hours, bestTopOneTime.Minutes, bestTopOneTime.Seconds);
    
    _lastScore.text = _playerStatsManager.LoadLastScore().ToString();
    _lastKills.text = _playerStatsManager.LoadLastKills().ToString();
    _lastTime.text = string.Format("{0:D2}:{1:D2}:{2:D2}", lastSurvivingTime.Hours, lastSurvivingTime.Minutes, lastSurvivingTime.Seconds);
    _lastTopOne.text = string.Format("{0:D2}:{1:D2}:{2:D2}", lastTopOneTime.Hours, lastTopOneTime.Minutes, lastTopOneTime.Seconds);
  }
}