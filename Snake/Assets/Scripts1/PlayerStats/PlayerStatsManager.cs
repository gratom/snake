using System;
//using GooglePlayGames.BasicApi;
//using GooglePlayGames.Native.Cwrapper;    //Commented out recently
using UnityEngine;

public class PlayerStatsManager : MonoBehaviour
{

  private const string KEY_BEST_SCORE = "KEY_BEST_SCORE";
  private const string KEY_BEST_KILLS = "KEY_BEST_KILS";
  private const string KEY_BEST_TOP = "KEY_BEST_TOP";
  private const string KEY_BEST_SURVIVAL_TIME = "KEY_BEST_SURVIVAL_TIME";
  
  private const string KEY_LAST_SCORE = "KEY_LAST_SCORE";
  private const string KEY_LAST_KILLS = "KEY_LAST_KILLS";
  private const string KEY_LAST_TOP = "KEY_LAST_TOP";
  private const string KEY_LAST_SURVIVAL_TIME = "KEY_LAST_SURVIVAL_TIME";
  
  public void SaveScore(int score)
  {
    PlayerPrefs.SetInt(KEY_LAST_SCORE, score);

    if (score > LoadBestScore())
    {
      PlayerPrefs.SetInt(KEY_BEST_SCORE, score);
    }
  }

  public void SaveKills(int kills)
  {
    PlayerPrefs.SetInt(KEY_LAST_KILLS, kills);

    if (kills > LoadBestKills())
    {
      PlayerPrefs.SetInt(KEY_BEST_KILLS, kills);
    }
  }
  
  public void SaveSurvivalTime(long time)
  {
    PlayerPrefs.SetString(KEY_LAST_SURVIVAL_TIME, time.ToString());

    if (time > LoadBestSurvivalTime())
    {
      PlayerPrefs.SetString(KEY_BEST_SURVIVAL_TIME, time.ToString());
    }
  }
  
  public void SaveTopOneTime(long time)
  {
    PlayerPrefs.SetString(KEY_LAST_TOP, time.ToString());

    if (time > LoadBestTopOneTime())
    {
      PlayerPrefs.SetString(KEY_BEST_TOP, time.ToString());
    }
  }
  
  public int LoadBestScore()
  {
    return PlayerPrefs.GetInt(KEY_BEST_SCORE, 0);
  }

  public int LoadBestKills()
  {
    return PlayerPrefs.GetInt(KEY_BEST_KILLS, 0);
  }

  public long LoadBestSurvivalTime()
  {
    return Convert.ToInt64(PlayerPrefs.GetString(KEY_BEST_SURVIVAL_TIME, "0"));
  }
  
  public long LoadBestTopOneTime()
  {
    return Convert.ToInt64(PlayerPrefs.GetString(KEY_BEST_TOP, "0"));
  }
  
  public int LoadLastScore()
  {
    return PlayerPrefs.GetInt(KEY_LAST_SCORE, 0);
  }

  public int LoadLastKills()
  {
    return PlayerPrefs.GetInt(KEY_LAST_KILLS, 0);
  }
  
  public long LoadLastSurvivalTime()
  {
    return Convert.ToInt64(PlayerPrefs.GetString(KEY_LAST_SURVIVAL_TIME, "0"));
  }
  
  public long LoadLastTopOneTime()
  {
    return Convert.ToInt64(PlayerPrefs.GetString(KEY_LAST_TOP, "0"));
  }

}