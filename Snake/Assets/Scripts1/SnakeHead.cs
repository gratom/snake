﻿using UnityEngine;
using System.Collections;
using CnControls;

public class SnakeHead : MonoBehaviour
{
    public Snake snakeParameters;
    public bool IsPlayer;
    public SpriteRenderer spriteRenderer;
    public SphereCollider sc;
    public AI aimodule;
    public float playerCircleOffset;
    [SerializeField]
    private float duelPlayAreaValue;
    int aiHeadRotateWaiter;
    
  public void OnEnable()
  {
    SetColorBasedOnTemplate();
    gameObject.tag = "Snake";
    gameObject.GetComponent<SphereCollider>().enabled = true;
    duelPlayAreaValue = 18f;
  }

  public void SetColorBasedOnTemplate()
  {
    spriteRenderer.color = snakeParameters.GetColorTemplate().colors[0];
  }


    void OnTriggerEnter(Collider obj)
    {
        if (snakeParameters.immuneTimer > 0)
            return;

        if (snakeParameters.dieing)
            return;

        if (obj.transform.root == transform.root)
            return;

        if (!snakeParameters.isActiveAndEnabled)
            return;

        //if (obj == null || obj.transform.root.gameObject == null)
        //    return;


        if (obj.transform.CompareTag("Snake"))
        {
          
            if (GameManager.instance.IsTeamMode() || snakeParameters.isPlayer||snakeParameters.IsBabySnake)
            {
                Snake otherSnake = obj.transform.root.GetComponent<Snake>();
                if (otherSnake == null || snakeParameters == null)
                    return;

                if (otherSnake.snakeTeam == snakeParameters.snakeTeam)
                    return;
           }

      if (snakeParameters.isPlayer)
      {
        GameManager.instance.lastSnakePoints = snakeParameters.points;
      }

      SnakeHead snakeHead = obj.transform.GetComponent<SnakeHead>();
      if (snakeHead != null)
      {
        if (snakeHead.snakeParameters.points < snakeParameters.points)
        {
         // Debug.Log("Snake Head - On trigger 1st");
                    //Debug.Log("obj = " + obj.transform.root.name);
                   // Debug.Log("current:- " + transform.root.name);
          snakeParameters.DeathRoutine();
        }
      }
      else
      {
          //Debug.Log("Snake Head - On trigger 2nd"+ obj.transform.name);
          snakeParameters.DeathRoutine();
      }

      if(obj != null)
      {
        if (obj.transform.root.gameObject != null)
        {
            if(obj.transform.root.gameObject.GetComponent<Snake>() != null)
            {
                if (obj.transform.root.gameObject.GetComponent<Snake>().isPlayer)
                {
                    AchievementManager._instance.EnemySnakeKillCounter();     //All plugins disabled because of game crashing situation
                    GameManager.instance.PlayerKills++;
                }
            }
        }
      }
      
    }

    else if (obj.transform.CompareTag("Food"))
    {
      Food food = obj.GetComponent<Food>();
            if (!snakeParameters.IsBabySnake)
                this.snakeParameters.points += Mathf.RoundToInt(food.foodValue * 1.3f);
            else
            {
                if(GameManager.instance.GetPlayerSnakeParams()!=null)
                    GameManager.instance.GetPlayerSnakeParams().points += Mathf.RoundToInt(food.foodValue * 1.3f);
            }
      if (this.snakeParameters != null)
      {
        this.snakeParameters.ControlSnakeScale();
      }

      food.MoveAndDisappear(obj.transform);
    }
  }

    public void Die()
    {
    }

    public void Duel_HeadUpdate()
    {
        if (snakeParameters.dieing)
            return;

        Move();
        //transform.localScale = new Vector3(snakeParameters.referenceScale, snakeParameters.referenceScale,snakeParameters.referenceScale);

        var screenPos = Camera.main.WorldToScreenPoint(transform.position);

        if ((screenPos.x > duelPlayAreaValue) && (screenPos.x < Screen.width - duelPlayAreaValue) && (screenPos.y > duelPlayAreaValue) && (screenPos.y < Screen.height - duelPlayAreaValue))
        {
            if (IsPlayer)
            {
                Vector3 axis = new Vector3(CnInputManager.GetAxis("Horizontal"), 0, CnInputManager.GetAxis("Vertical"));
                if (axis != Vector3.zero)
                {
                    targetEulerY = Constants.GetPlusAngle(Quaternion.LookRotation(axis).eulerAngles.y);
                }
                Rotate();
            }
            else
            {
                if (aiHeadRotateWaiter % 1 == 0)
                {
                    IaRotate();
                }

                    aiHeadRotateWaiter++;
            }
            
        }
        else
        {
            RotateTowardsCenter();
        }
        
    }

  public void HeadUpdate()
  {
    if (snakeParameters.dieing) return;


    Move();
        
        //To make head bigger with the amount of food consumed
        transform.localScale = new Vector3(snakeParameters.referenceScale, snakeParameters.referenceScale,snakeParameters.referenceScale);
        for (int i = 0; i < snakeParameters.snakePieces.Count; i++)
        {
            snakeParameters.snakePieces[i].transform.localScale = new Vector3(snakeParameters.referenceScale, snakeParameters.referenceScale, snakeParameters.referenceScale);
        }

        //if (Vector3.Distance(transform.position, Vector3.zero) <= playerCircleLimit)// Population.instance.spawnCircleLenght)
        //{
        //    if (IsPlayer)
        //    {
        //        Vector3 axis = new Vector3(CnInputManager.GetAxis("Horizontal"), 0, CnInputManager.GetAxis("Vertical"));

        //        if (axis != Vector3.zero)
        //        {
        //            targetEulerY = Constants.GetPlusAngle(Quaternion.LookRotation(axis).eulerAngles.y);
        //        }

        //        Rotate();
        //    }
        //    else
        //    {
        //        if (aiHeadRotateWaiter % 1 == 0)
        //        {
        //            IaRotate();
        //        }

        //        aiHeadRotateWaiter++;
        //    }
        //}
        //else
        //{
        //    RotateTowardsCenter();
        //}

        if (IsPlayer)
        {
            if (Vector3.Distance(transform.position, Vector3.zero) <= Population.instance.spawnCircleLenght + playerCircleOffset)
            {
                Vector3 axis = new Vector3(CnInputManager.GetAxis("Horizontal"), 0, CnInputManager.GetAxis("Vertical"));

                if (axis != Vector3.zero)
                {
                    targetEulerY = Constants.GetPlusAngle(Quaternion.LookRotation(axis).eulerAngles.y);
                }

                Rotate();
            }
            else
            {
                RotateTowardsCenter();
            }
        }
        else
        {


            if (Vector3.Distance(transform.position, Vector3.zero) <= Population.instance.spawnCircleLenght)
            {
                  if (snakeParameters.FollowPlayer && snakeParameters.PlayerToFollow != null)
                  {
                    /*  if (GoingTowardPlayer)
                      {
                          if (Vector3.Distance(transform.position, snakeParameters.PlayerToFollow.transform.position) < 10)
                          {
                              GoingTowardPlayer = false;
                              SpeedyFriend = 1;
                          }
                          else
                          {
                              Quaternion targetLook = Quaternion.LookRotation(snakeParameters.PlayerToFollow.transform.position - transform.position);
                              transform.rotation = Quaternion.Slerp(transform.rotation, targetLook, snakeParameters.rotatingSpeed * 4 * Time.deltaTime);
                              return;
                          }
                      }*/
                if (Vector3.Distance(transform.position, snakeParameters.PlayerToFollow.snakeHead.transform.position) > 50)
                    {
                        //distance is farther from player
                        Quaternion targetLook = Quaternion.LookRotation(snakeParameters.PlayerToFollow.snakeHead.transform.position - transform.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, targetLook, snakeParameters.rotatingSpeed * Time.deltaTime);
                    }
                    else if (aiHeadRotateWaiter % 1 == 0)
                    {
                        IaRotate();
                    }
                }
                else if (aiHeadRotateWaiter % 1 == 0)
                {
                    IaRotate();
                }

                aiHeadRotateWaiter++;
            }
            else
            {
                RotateTowardsCenter();
            }
        }

  }
    float SpeedyFriend = 1;
    bool GoingTowardPlayer = false;

    public void GoToPlayer()
    {
        GoingTowardPlayer = true;
        SpeedyFriend = 5;
        Quaternion targetLook = Quaternion.LookRotation(snakeParameters.PlayerToFollow.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetLook, snakeParameters.rotatingSpeed * 4 * Time.deltaTime);
    }

    void RotateTowardsCenter()
  {
    Quaternion targetLook = Quaternion.LookRotation(Vector3.zero - transform.position);
    transform.rotation =
      Quaternion.Slerp(transform.rotation, targetLook, snakeParameters.rotatingSpeed * Time.deltaTime);
    targetEulerY = transform.eulerAngles.y;
  }


  void IaRotate()
  {
    if (aimodule.direction != Vector3.zero)
    {
      Quaternion targetLook = Quaternion.LookRotation(aimodule.direction);

      transform.rotation =
        Quaternion.Slerp(transform.rotation, targetLook, snakeParameters.rotatingSpeed * Time.deltaTime);
    }
  }

  float targetEulerY = 0;
  private object reference;

  void Rotate()
  {
    
    float curEulerY = Constants.GetPlusAngle(transform.eulerAngles.y);
    float startAngle = curEulerY;
    float rightAngle = Constants.GetPlusAngle(targetEulerY - curEulerY);
    float leftAngle = Constants.GetPlusAngle(curEulerY - targetEulerY);
    if (rightAngle < leftAngle)
    {
      if (rightAngle < 1f)
        curEulerY = targetEulerY;
      else
      {
        curEulerY = Constants.GetPlusAngle(curEulerY + snakeParameters.rotatingSpeed * 50f * Time.deltaTime);
        if (Constants.IsBig(curEulerY, targetEulerY, startAngle))
        {
          curEulerY = targetEulerY;
        }
      }
    }
    else
    {
      if (leftAngle < 1f)
        curEulerY = targetEulerY;
      else
      {
        curEulerY = Constants.GetPlusAngle(curEulerY - snakeParameters.rotatingSpeed * 50f * Time.deltaTime);
        if (Constants.IsSmall(curEulerY, targetEulerY, startAngle))
        {
          curEulerY = targetEulerY;
        }
      }
    }

    Vector3 euler = Vector3.zero;
    euler.y = curEulerY;
    transform.eulerAngles = euler;
  }

  void Move()
  {
    transform.position +=
      transform.forward * snakeParameters.speed * (Time.deltaTime) * snakeParameters.speedMultiplier*SpeedyFriend;
  }

    //public void ResetPosition()
    //{
    //    transform.position = new Vector3(0f, 0f, 0f);
    //    Debug.Log("Player Snake head reset:- " + transform.position);
        
    //}
}