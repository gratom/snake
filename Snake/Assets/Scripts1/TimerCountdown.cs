﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerCountdown : MonoBehaviour
{
    public Text timerText;
    public float timerCount;
    private float count;
    public bool isExtendTimer = false;

    private void OnEnable()
    {
      //  Debug.Log("timer extended "+isExtendTimer);
        if (!isExtendTimer)
            count = timerCount;
        else
        {
            count = 20;
            timerText.text = "20";
        }

        isExtendTimer = false;
    }

    private void Update()
    {
        timerText.text = count.ToString("0");
        count -= Time.deltaTime;
        if(count < 1)
        {
            //If two or more snakes still alive, the longest will win.
            //Debug.Log("Time over");
            GameManager.instance.isDuelTimerOver = true;
            gameObject.SetActive(false);
        }
    }

    public void ExtendTime()
    {
        GameManager.instance.isDuelTimerOver = false;
        //isExtendTimer = true;
        this.gameObject.SetActive(true);
       
    //    timerText.text = "20";
     //   count =20;
       
    }
}
