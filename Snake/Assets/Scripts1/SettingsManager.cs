﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CnControls;

public class SettingsManager : MonoBehaviour {

	// Use this for initialization
	public GameObject soundsElement;
	public GameObject controlsElement;
	public GameObject moreElement;

	public Button soundsSettingsButton;
	public GameObject soundsUnSelectedHighlight;
	//public Text soundText;

	public Button controlsSettingsButton;
	public GameObject controlsUnSelectedHighlight;
	//public Text controlText;

	public Button moreSettingsButton;
	public GameObject moreUnSelectedHighlight;
	//public Text moreText;

	public AudioSource bgMusic;

	public Button musicToggleButton;
    public Button musicOnButton;
    public Button musicOffButton;
	public bool ifMusicOff;
	public float bgMusicVolume;
	public Sprite OnButtonSprite;
	public Sprite OffButtonSprite;
	public Slider soundVolumeSlider;
	public Text soundPercent;

	public bool ifFreeJoyStickControlMode;

	public Button freeJoystickButton;
	public Button fixedJoystickButton;
	//public Text freeJoyStickText; //recent
	//public Text fixedJoyStickText;

	public bool isLeftHand;
	public GameObject leftHandSelected;
	public GameObject rightHandSelected;


	void OnEnable()
	{
		SoundSettingsButtonClicked ();
		CheckForSavedSettings ();
	}

	void Start () {
		//Adds a listener to the main slider and invokes a method when the value changes.
		soundVolumeSlider.onValueChanged.AddListener(delegate {SliderValueChangeCheck(); });
	}

	public void SoundSettingsButtonClicked()
	{
		DisableAll ();
		soundsUnSelectedHighlight.SetActive (false);
		//SetButtonAlpha (soundsSettingsButton, true, soundText);
		soundsElement.SetActive (true);
	}

	public void ControlSettingsButtonClicked()
	{
		DisableAll ();
		controlsUnSelectedHighlight.SetActive (false);
		//SetButtonAlpha (controlsSettingsButton, true, controlText);
		controlsElement.SetActive (true);
	}

	public void MoreSettingsButtonClicked()
	{
		DisableAll ();
		moreUnSelectedHighlight.SetActive (false);
		//SetButtonAlpha (moreSettingsButton, true, moreText);
		moreElement.SetActive (true);
	}

	public void SoundTogglButtonClicked()
	{
		ifMusicOff = !ifMusicOff;
		ToggleSoundOnOff ();
	}

	public void ToggleSoundOnOff()
	{
		if (ifMusicOff) {
			PlayerPrefs.SetInt (GameUtils.PREFS_SOUND_TOGGLE, 0);
			soundVolumeSlider.enabled = false;
		} else {
			PlayerPrefs.SetInt (GameUtils.PREFS_SOUND_TOGGLE, 1);
			soundVolumeSlider.enabled = true;
		}

		if (ifMusicOff) {
            soundVolumeSlider.value = 0;
            musicOffButton.GetComponent<Image>().sprite = OnButtonSprite;
            musicOffButton.GetComponent<Image>().SetNativeSize();

            musicOnButton.GetComponent<Image>().sprite = OffButtonSprite;
            musicOnButton.GetComponent<Image>().SetNativeSize();
        } else {
			//Debug.Log ("Coming to this section");
			SetSound ();
            musicOffButton.GetComponent<Image>().sprite = OffButtonSprite;
            musicOffButton.GetComponent<Image>().SetNativeSize();

            musicOnButton.GetComponent<Image>().sprite = OnButtonSprite;
            musicOnButton.GetComponent<Image>().SetNativeSize();
        }
		
	}

    private void Update()
    {
        bgMusic.volume = soundVolumeSlider.value;
    }
    public void SliderValueChangeCheck()
	{
		bgMusicVolume = soundVolumeSlider.value;
		//soundPercent.text = Mathf.RoundToInt(bgMusicVolume * 100) + "%";
		SetSound ();
	}

	public void SetSound()
	{
		if(!ifMusicOff)
		bgMusic.volume = bgMusicVolume;

        PlayerPrefs.SetFloat (GameUtils.PREFS_SOUND_VOLUME, bgMusicVolume);
		PlayerPrefs.Save ();
	}

	public void FixedJoyStickSelectedClicked()
	{
		ifFreeJoyStickControlMode = false;
		PlayerPrefs.SetInt (GameUtils.PREFS_JOYSTICK_CONTROL_MODE, 1);
		PlayerPrefs.Save ();
        fixedJoystickButton.GetComponent<Image>().sprite = OnButtonSprite;
        freeJoystickButton.GetComponent<Image>().sprite = OffButtonSprite;
        //SetJoyStickControlMode ();
    }

	public void FreeJoyStickSelectedClicked()
	{
		ifFreeJoyStickControlMode = true;
		PlayerPrefs.SetInt (GameUtils.PREFS_JOYSTICK_CONTROL_MODE, 0);
		PlayerPrefs.Save ();
        fixedJoystickButton.GetComponent<Image>().sprite = OffButtonSprite;
        freeJoystickButton.GetComponent<Image>().sprite = OnButtonSprite;
        //SetJoyStickControlMode ();
    }

	public void SetJoyStickControlMode()
	{
		if (ifFreeJoyStickControlMode)
        {  //free base
           // Debug.Log("Arrow");
            FreeJoyStickSelectedClicked();
            //SetButtonAlpha (freeJoystickButton, true);    //recent
            //SetButtonAlpha (fixedJoystickButton, false);

            //SetButtonAlpha(freeJoystickButton, true, freeJoyStickText);
            //SetButtonAlpha(fixedJoystickButton, false, fixedJoyStickText);

        }
        else
        { //Fixed base
            //Debug.Log("Joystick");
            FixedJoyStickSelectedClicked();
            //SetButtonAlpha (fixedJoystickButton, true);
            //SetButtonAlpha (freeJoystickButton, false);


            //SetButtonAlpha(fixedJoystickButton, true, fixedJoyStickText);
            //SetButtonAlpha(freeJoystickButton, false, freeJoyStickText);

        }
	}

	public void LeftHandLayoutSelectedClicked()
	{
		isLeftHand = true;
		SetControlLayoutMode ();
	}

	public void RightHandLayoutSelectedClicked()
	{
		isLeftHand = false; 
		SetControlLayoutMode ();
	}

	public void SetControlLayoutMode()
	{
		// Debug.Log("Inside setcontrolLayout");
		Debug.Log("left hand : " + isLeftHand);
		if (isLeftHand)
        {
            Debug.Log("Left Hand");
			leftHandSelected.SetActive (true);
			rightHandSelected.SetActive (false);
			PlayerPrefs.SetInt (GameUtils.PREFS_JOYSTICK_CONTROL_HAND, 1);  //0
			PlayerPrefs.Save ();
		}
        else
        {
            Debug.Log("Right Hand");
			leftHandSelected.SetActive (false);
			rightHandSelected.SetActive (true);
			PlayerPrefs.SetInt (GameUtils.PREFS_JOYSTICK_CONTROL_HAND, 0);  //1
			PlayerPrefs.Save ();
		}
	}

	public void CheckForSavedSettings()
	{
		int musicToggleValue = 0;
        Debug.Log("Joystick mode:- " + PlayerPrefs.GetInt(GameUtils.PREFS_JOYSTICK_CONTROL_MODE));
        Debug.Log("Joystick hand:- " + PlayerPrefs.GetInt(GameUtils.PREFS_JOYSTICK_CONTROL_HAND));

		if (PlayerPrefs.HasKey (GameUtils.PREFS_SOUND_TOGGLE))
        {
            Debug.Log("Inside if saved settings");
			musicToggleValue = PlayerPrefs.GetInt (GameUtils.PREFS_SOUND_TOGGLE);

			bgMusicVolume = PlayerPrefs.GetFloat (GameUtils.PREFS_SOUND_VOLUME);
			soundVolumeSlider.value = bgMusicVolume;
			//soundPercent.text = Mathf.RoundToInt(bgMusicVolume * 100) + "%";


			if (PlayerPrefs.GetInt (GameUtils.PREFS_JOYSTICK_CONTROL_MODE,0) == 1)
				ifFreeJoyStickControlMode = false;
			else
				ifFreeJoyStickControlMode = true;

            if (PlayerPrefs.GetInt(GameUtils.PREFS_JOYSTICK_CONTROL_HAND) == 1) //0
            {
                isLeftHand = true;
                
            }
            else
            {
                isLeftHand = false;
            }
            
		}
        else
        {
            Debug.Log("Inside else saved settings");
			PlayerPrefs.SetInt (GameUtils.PREFS_SOUND_TOGGLE, 1);
			PlayerPrefs.SetFloat (GameUtils.PREFS_SOUND_VOLUME, 1.0f);
			PlayerPrefs.SetInt (GameUtils.PREFS_JOYSTICK_CONTROL_MODE, 0);  
			PlayerPrefs.SetInt (GameUtils.PREFS_JOYSTICK_CONTROL_HAND, 1);
			PlayerPrefs.Save ();
			musicToggleValue = 1;
			bgMusicVolume = 1;
			soundVolumeSlider.value = 1;
			ifFreeJoyStickControlMode = true;
			isLeftHand = true;
		}

		if (musicToggleValue == 0)
			ifMusicOff = true;
		else
			ifMusicOff = false;



		ToggleSoundOnOff ();
		SetSound ();
		SetJoyStickControlMode ();
		SetControlLayoutMode ();
	}



	void DisableAll()
	{
		soundsElement.SetActive (false);
		soundsUnSelectedHighlight.SetActive (true);
		//SetButtonAlpha (soundsSettingsButton, false, soundText);

		controlsElement.SetActive (false);
		controlsUnSelectedHighlight.SetActive (true);
		//SetButtonAlpha (controlsSettingsButton, false, controlText);

		moreElement.SetActive (false);
		moreUnSelectedHighlight.SetActive (true);
		//SetButtonAlpha (moreSettingsButton, false, moreText);
	}

	void SetButtonAlpha(Button button, bool shouldIncrease)
	{
		Color newColor = Color.white;
		if (shouldIncrease)
			newColor.a = 1f;
		else
			newColor.a = 0f;

		button.GetComponent<Image> ().color = newColor;

        //if (text == null)
        //    return;

		//if (shouldIncrease)       //recent
		//	text.color = Color.black;
		//else
		//	text.color = Color.white;
			
	}
}
