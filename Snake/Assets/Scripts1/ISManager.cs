﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if IRONSOURCE
public class ISManager : MonoBehaviour
{
    public static ISManager instance;     //Commented out recently
    public string rewardType = "";
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

    }
    // Use this for initialization
    void Start()
    {
#if UNITY_IOS
        IronSource.Agent.init("99321bfd", IronSourceAdUnits.REWARDED_VIDEO);
        //For Interstitial
        IronSource.Agent.init("99321bfd", IronSourceAdUnits.INTERSTITIAL);
#endif
#if UNITY_ANDROID
        //For Rewarded Video
        IronSource.Agent.init("925bad15", IronSourceAdUnits.REWARDED_VIDEO);
        //For Interstitial
        IronSource.Agent.init("925bad15", IronSourceAdUnits.INTERSTITIAL);
#endif
        IronSource.Agent.validateIntegration();
        IronSource.Agent.loadInterstitial();


        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

    }

    void OnEnable()
    {
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
        IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
        IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
        IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
        IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
    }

    void OnApplicationPause(bool isPaused)
    {
        IronSource.Agent.onApplicationPause(isPaused);
    }
    //Invoked when the initialization process has failed.
    //@param description - string - contains information about the failure.
    void InterstitialAdLoadFailedEvent(IronSourceError error)
    {
        Debug.Log("Interstitial ad failed to load");
    //   GameManager.instance.OnGameOverPlayButton();
    }
    //Invoked right before the Interstitial screen is about to open.
    void InterstitialAdShowSucceededEvent()
    {
    }
    //Invoked when the ad fails to show.
    //@param description - string - contains information about the failure.
    void InterstitialAdShowFailedEvent(IronSourceError error)
    {
    }
    // Invoked when end user clicked on the interstitial ad
    void InterstitialAdClickedEvent()
    {
    }
    //Invoked when the interstitial ad closed and the user goes back to the application screen.
    void InterstitialAdClosedEvent()
    {
        IronSource.Agent.loadInterstitial();
    }
    //Invoked when the Interstitial is Ready to shown after load function is called
    void InterstitialAdReadyEvent()
    {
    }
    //Invoked when the Interstitial Ad Unit has opened
    void InterstitialAdOpenedEvent()
    {
    }



    public void ShowInterstitial()
    {
        if (PlayerPrefs.GetInt("ads", 1) == 0)
            return;

        if (IronSource.Agent.isInterstitialReady())
        {
            IronSource.Agent.showInterstitial();
        }
    }
    
    //Invoked when the RewardedVideo ad view has opened.
    //Your Activity will lose focus. Please avoid performing heavy 
    //tasks till the video ad will be closed.
    void RewardedVideoAdOpenedEvent()
    {
    }
    //Invoked when the RewardedVideo ad view is about to be closed.
    //Your activity will now regain its focus.
    void RewardedVideoAdClosedEvent()
    {
        //        Debug.Log("---------->RewardedVideoAdClosedEvent");
        isAdClosed = true;
        //Debug.Log("RewardedVideoAdClosedEvent");
        //PlayerPrefs.SetInt("WatchedAd", 1);
        ////PlayerPrefs.SetInt("points", Snake.player.points);
        //GameManager.instance.loadingOverlay.SetActive(true);
        //GameManager.instance.KillAllSnakes();
    }
    //Invoked when there is a change in the ad availability status.
    //@param - available - value will change to true when rewarded videos are available. 
    //You can then show the video by calling showRewardedVideo().
    //Value will change to false when no videos are available.
    void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        //Change the in-app 'Traffic Driver' state according to availability.
        bool rewardedVideoAvailability = available;
    }
    //  Note: the events below are not available for all supported rewarded video 
    //   ad networks. Check which events are available per ad network you choose 
    //   to include in your build.
    //   We recommend only using events which register to ALL ad networks you 
    //   include in your build.
    //Invoked when the video ad starts playing.
    void RewardedVideoAdStartedEvent()
    {
    }
    //Invoked when the video ad finishes playing.
    void RewardedVideoAdEndedEvent()
    {
    }
    //Invoked when the user completed the video and should be rewarded. 
    //If using server-to-server callbacks you may ignore this events and wait for the callback from the  ironSource server.
    //
    //@param - placement - placement object which contains the reward data
    //
    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        Debug.Log("RewardedVideoAdRewardedEvent");
        isRewarded = true;
    }

    public void dummmyCall()
    {
        isRewarded = true;
        isAdClosed = true;

    }

    //Invoked when the Rewarded Video failed to show
    //@param description - string - contains information about the failure.
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        Debug.Log("RewardedVideo Ad Failed-------------->>> " + error + "    " + error.getErrorCode() + "    " + error.getDescription());
        GameManager.instance.OnGameOverPlayButton();
    }

    public bool isVideoAvailable()
    {
        return IronSource.Agent.isRewardedVideoAvailable();
    }
    
    public void ShowRewardVideo(string type)
    {
        rewardType = type;
        IronSource.Agent.showRewardedVideo();
        GameManager.instance.gameOverCanvas.SetActive(false);
       // AchievementManager._instance.PlaySessionCount();
    }
    
    bool isAdClosed = false;
    bool isRewarded = false;

    void Update()
    {
        if (isAdClosed)
        {
            if (isRewarded)
            {
                if (rewardType == "")
                {
                    //Debug.Log("is rewarded :- " + isRewarded);
                    PlayerPrefs.SetInt("WatchedAd", 1);
                    //PlayerPrefs.SetInt("points", Snake.player.points);
                    SnakeSpawner.Instance.UseLastSnakePoints = true;
                    GameManager.instance.loadingOverlay.SetActive(true);
                    GameManager.instance.KillAllSnakes();

                }
                else if (rewardType == "egg")
                {
                    int gotEggs = 2;
                    EggsManager.instance.IncreaseEgg(gotEggs);
                    AchievementManager._instance.achivementUnlockText.text = "You got " + gotEggs.ToString() + " Eggs!";
                    GameManager.instance.dialogBox.SetActive(true);
                    rewardType = "";
                }
                else if(rewardType== "duelmode")
                {
                    GameManager.instance.ExtendDuelModeTimer();
                    rewardType = "";
                }
                isRewarded = false;
            }
            else
            {
                // Ad closed but user skipped ads, so no reward 
                // Ad your action here 
            }
            isAdClosed = false;  // to make sure this action will happen only once.
        }
    }

}
#endif