﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum MaskType {
    Normal = 0,
    Pro = 1,
    Egg = 2
}
public class MaskSelector : MonoBehaviour {

	// Use this for initialization
	public int maskID;
	public Image maskImageHolder;
    public GameObject lockObj;
    public GameObject eggBox;
    public Text eggAmount;
    public int price;
    public bool isLocked = false;
    public MaskType maskType = MaskType.Normal;
	void Start () {
		
	}

    public void SetMaskDetails(int _maskID, Sprite _maskImage, bool isAchivementMask = false, bool isEggMask = false)
	{
		maskID = _maskID;
		maskImageHolder.sprite = _maskImage;
        if (isAchivementMask)
        {
            maskType = MaskType.Pro;
            isLocked = true;
            lockObj.SetActive(true);
            maskImageHolder.color = Color.black;

            if (PlayerPrefs.GetInt("pro" + _maskID, 0) == 1)
            {
                isLocked = false;
                lockObj.SetActive(false);
                maskImageHolder.color = Color.white;

            }
            /*  if (Social.localUser.authenticated)
              {

              }*/
            // AchievementManager._instance.lo
        }
        if (isEggMask)
        {
            maskType = MaskType.Egg;
            isLocked = true;
            lockObj.SetActive(false);
            eggBox.SetActive(true);
            price = GetPriceWithId(maskID);
            eggAmount.text =price.ToString();
           
            if (PlayerPrefs.GetInt("egg" + _maskID, 0) == 1)
            {
                isLocked = false;
                lockObj.SetActive(false);
                eggBox.SetActive(false);

            }
        }
	}

    public int GetPriceWithId(int id)
    {
        switch (id)
        {
            case 0:
            case 1:
                return 20;
                
                break;
            case 5:
            case 7:
                return 15;
                break;
            case 9:
                return 50;
                break;
            default:
                return 10;
                break;
        }
    }

    public void UnlockEggMaskAndSelect()
    {
        PlayerPrefs.SetInt("egg" + maskID, 1);
        isLocked = false;
        lockObj.SetActive(false);
        eggBox.SetActive(false);
        MaskGUIScript._instance.OnSelectClick(maskID);
    }

	public void OnMaskSelection()
	{
        if (isLocked)
        {
            if (maskType == MaskType.Pro)
            {
                if (!Social.localUser.authenticated)
                {
                    Social.localUser.Authenticate((bool success) =>
                    {
                        if (success)
                        {
                            AchievementManager._instance.UnlockAllProMasks();

                        }
                        else
                        {
                            AchievementManager._instance.achivementUnlockText.text = "Unlock!\n" + AchievementManager._instance.achivementTexts[maskID] + "\nAchievement";
                            GameManager.instance.dialogBox.SetActive(true);
                        }
                    });
                }
                else
                {
                    AchievementManager._instance.achivementUnlockText.text = "Unlock!\n" + AchievementManager._instance.achivementTexts[maskID] + "\nAchievement";
                    GameManager.instance.dialogBox.SetActive(true);
                }
            }
            else if (maskType == MaskType.Egg)
            {
                EggsManager.instance.selector = this;
                GameManager.instance.eggBuyBox.SetActive(true);

            }
            return;
        }
		MaskGUIScript._instance.OnSelectClick (maskID);
	}
}
