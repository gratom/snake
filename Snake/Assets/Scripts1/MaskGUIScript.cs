﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MaskGUIScript : MonoBehaviour
{
	public static MaskGUIScript _instance;

	public GameObject mainMenuCanvas;
	public GameObject maskCanvas;
	public Image[] showmasks;
    public GameObject[] tabsObject;
	public Sprite nomaskImage;
    public GameObject skinPrefab;
    public List<Skin> skins;
    public Transform skinparent;
    public int currentSkinSelected = 0;
	int currIndex = 0;

	public Button herosButton;
	public GameObject herosUnselectedHighlight;
	public Text herosText;

	public Button animalsButton;
	public GameObject animalUnselectedHighlight;
	public Text animalText;

	public Button charactersButton;
	public GameObject characterUnselectedHighlight;
	public Text creatureText;
	public ScrollRect scrollRect;
    
	public int maskStartingIndex;
	public int maskEndingIndex;
	public GameObject maskSelectorIcon;
	public GameObject maskSelectorIconHolder;
	public List<MaskSelector> allMaskSelector = new List<MaskSelector> ();
    public GameObject childPrefab;

    private GameObject child;

    public GameObject sliderBuyButton;
    public Slider transparencySlider;
    public Text transparencyText;

    void Awake()
	{
		_instance = this;
        SetupSkins();

    }

	void OnEnable()
	{
		maskStartingIndex = 0;
		maskEndingIndex = 21;
		AnimalsMaskSelectionCategoryClicked ();
        SetMaskTab(0);

        if (PlayerPrefs.HasKey("transparency"))
        {
            if (PlayerPrefs.GetInt("transparency") == 0)
            {
                sliderBuyButton.SetActive(false);
                transparencySlider.enabled = true;
                if (PlayerPrefs.HasKey("PlayerTransparency"))
                {
                    transparencySlider.value = PlayerPrefs.GetFloat("PlayerTransparency");
                }
            }
            else
            {
                sliderBuyButton.SetActive(true);
                transparencySlider.enabled = false;
            }
        }
        else
        {
            PlayerPrefs.SetInt("transparency", 1);
            PlayerPrefs.Save();
            sliderBuyButton.SetActive(true);
            transparencySlider.enabled = false;
        }

#if UNITY_WEBGL
        sliderBuyButton.SetActive(false);
        transparencySlider.enabled = true;
        if (PlayerPrefs.HasKey("PlayerTransparency"))
        {
            transparencySlider.value = PlayerPrefs.GetFloat("PlayerTransparency");
        }
#endif
    }

    public void EnableSlider()
    {
        sliderBuyButton.SetActive(false);
        transparencySlider.enabled = true;
    }

    public void SetMaskTab(int index)
    {
        DisableAll();
        MaskManager.instance.SetTab(index);
        for (int x = 0; x < tabsObject.Length; x++)
        {
            if (x == index)
                tabsObject[x].SetActive(false);
            else
                tabsObject[x].SetActive(true);
        }
        //animalUnselectedHighlight.SetActive(false);
        maskStartingIndex = 0;
        maskEndingIndex = MaskManager.instance.masks.Length - 1; ;
        AssignMasksToCategory();
       
    }

    public void SetupSkins()
    {
        skins = new List<Skin>();
        for (int x = 0; x < SkinManager._instance.availableColorTemplate.Length-1; x++)
        {
            GameObject nSkin = Instantiate(skinPrefab, skinparent) as GameObject;
            Skin skin = nSkin.GetComponent<Skin>();
            skin.SetupSkin(SkinManager._instance.availableColorTemplate[x]);
            skin.maskGui = this;
            skin.index = x;
            skins.Add(skin);
            
        }
        transparencyText.text = ((int)(transparencySlider.value * 100f)).ToString() + "%";
    }
    
    public void AnimalsMaskSelectionCategoryClicked()
    {
        DisableAll();
        animalUnselectedHighlight.SetActive(false);
        maskStartingIndex = 0;
        maskEndingIndex = 16;
        AssignMasksToCategory();
    }

    //   public void HeroesMaskSelectionCategoryClicked()		//Disabled as client wanted only animals
    //{
    //	DisableAll ();
    //	herosUnselectedHighlight.SetActive (false);
    //       maskStartingIndex = 12;
    //	maskEndingIndex = 25;
    //       AssignMasksToCategory ();
    //}

    //public void CharactersMaskSelectionCategoryClicked()
    //{
    //	DisableAll ();
    //       characterUnselectedHighlight.SetActive (false);
    //       //SetButtonAlpha (creaturesButton, true, creatureText);
    //       maskStartingIndex = 26;
    //	maskEndingIndex = 39;
    //       AssignMasksToCategory ();
    //}

    public void SetSkin(Skin skin)
    {
        skins[currentSkinSelected].Deselect();
        currentSkinSelected = skin.index;
        LooknFeelDisplay._instance.SetSnakeColorInDisplay(skins[currentSkinSelected].colorTemp);
        LooknFeelDisplay._instance.SetTransparency(1f - transparencySlider.value);
        PlayerPrefs.SetInt("skin_index", currentSkinSelected);
    }

    public void AssignMasksToCategory()
	{
        int count = 0;
        for (int i = 0; i < allMaskSelector.Count; i++)
        {
			Destroy (allMaskSelector [i].gameObject);
		}
        
        for (int i = 0; i < maskSelectorIconHolder.transform.childCount; i++)
        {
            Destroy(maskSelectorIconHolder.transform.GetChild(i).gameObject);
        }
        
        allMaskSelector.Clear ();
        child = Instantiate(childPrefab, transform.position, Quaternion.identity) as GameObject;
        child.transform.SetParent(maskSelectorIconHolder.transform, false);

        for (int j = maskStartingIndex; j <= maskEndingIndex; j++)
        {
			GameObject newMaskSelector = Instantiate (maskSelectorIcon, transform.position, Quaternion.identity) as GameObject;
            count++;

            if(count > 4)
            {
                CreateGameobject();
                count = 1;
            }
            
            newMaskSelector.transform.SetParent(child.transform, false);

            if(MaskManager.instance.tabIndex==4)
                newMaskSelector.GetComponent<MaskSelector>().SetMaskDetails(j, MaskManager.instance.masks[j],true);
            if (MaskManager.instance.tabIndex == 5)
                newMaskSelector.GetComponent<MaskSelector>().SetMaskDetails(j, MaskManager.instance.masks[j], false,true);
            else
              newMaskSelector.GetComponent<MaskSelector>().SetMaskDetails(j, MaskManager.instance.masks[j]);

            allMaskSelector.Add(newMaskSelector.GetComponent<MaskSelector>());
		}
        count = 0;
        scrollRect.GetComponent<UnityEngine.UI.Extensions.HorizontalScrollSnap>().enabled = false;
        scrollRect.horizontalNormalizedPosition = 0f;
        scrollRect.GetComponent<UnityEngine.UI.Extensions.HorizontalScrollSnap>()._currentPage = 0;
        Invoke("EnableScrollSnap", 0.01f);
       
     //   scrollRect.normalizedPosition = new Vector2(0,0);

    }

    private void EnableScrollSnap()
    {
        scrollRect.GetComponent<UnityEngine.UI.Extensions.HorizontalScrollSnap>().enabled = true;
    }
    
    private void CreateGameobject()
    {
        child = Instantiate(childPrefab, transform.position, Quaternion.identity) as GameObject;
        child.transform.SetParent(maskSelectorIconHolder.transform, false);
    }
    
	void DisableAll()
	{
		herosUnselectedHighlight.SetActive (true);
		//SetButtonAlpha (herosButton, false, herosText);


		animalUnselectedHighlight.SetActive (true);
        //SetButtonAlpha (animalsButton, false, animalText);


        characterUnselectedHighlight.SetActive (true);
		//SetButtonAlpha (creaturesButton, false, creatureText);
	}

    public void ChangeSlider(float slide)
    {
        transparencyText.text = ((int)(transparencySlider.value * 100f)).ToString() + "%";
        LooknFeelDisplay._instance.SetTransparency(1f-transparencySlider.value);
        PlayerPrefs.SetFloat("PlayerTransparency", transparencySlider.value);

    }

	//void SetButtonAlpha(Button button, bool shouldIncrease, Text text)
	//{
	//	Color newColor = Color.white;
	//	if (shouldIncrease)
	//		newColor.a = 1f;
	//	else
	//		newColor.a = 0f;

	//	button.GetComponent<Image> ().color = newColor;

	//	if (shouldIncrease)
	//		text.color = Color.black;
	//	else
	//		text.color = Color.white;

	//}

	public void OnSelectClick(int _maskNumber)
	{
		GameManager.instance.playerMaskNo = _maskNumber;
		PlayerPrefs.SetInt ("PlayerMask", GameManager.instance.playerMaskNo);
        PlayerPrefs.SetInt("TabIndex", MaskManager.instance.tabIndex);
		LooknFeelDisplay._instance.eyes.SetActive (false);
		LooknFeelDisplay._instance.SetSnakeMaskOnDisplay (MaskManager.instance.masks [GameManager.instance.playerMaskNo],true,false);

		//mainMenuCanvas.SetActive(true);
		//maskCanvas.SetActive(false);
	}

	public void OnSelectNoMask()
	{
		GameManager.instance.playerMaskNo = -1;
		PlayerPrefs.SetInt ("PlayerMask", GameManager.instance.playerMaskNo);

		LooknFeelDisplay._instance.SetSnakeMaskOnDisplay (nomaskImage,true,true);
		LooknFeelDisplay._instance.eyes.SetActive (true);
		LooknFeelDisplay._instance.ChangeMaskColorAsFirstColor ();
	}

	public void onLeftClick()
	{
		currIndex -= 4;
		if (currIndex < 0) {
			currIndex = 0;
		}
		for (int i = 0; i < 4; i++) {
			if (currIndex < MaskManager.instance.masks.Length) {
				showmasks [i].color = Color.white;
				showmasks [i].sprite = MaskManager.instance.masks [currIndex + i];
			} else {
				showmasks[i].color = Color.clear;
			}
		}
	}

	public void onRightClick()
	{
		currIndex += 4;
		if (currIndex > MaskManager.instance.masks.Length - 1) {
			currIndex = currIndex - 4;
			return;
		}
		for (int i = 0;i < 4;i++) {
			if (currIndex + i < MaskManager.instance.masks.Length) {
				showmasks [i].color = Color.white;
				showmasks [i].sprite = MaskManager.instance.masks [currIndex + i];
			} else {
				showmasks [i].color = Color.clear;
			}
		}
	}

	public void onBackClick()
	{
		mainMenuCanvas.SetActive(true);
		maskCanvas.SetActive(false);
	}
}
