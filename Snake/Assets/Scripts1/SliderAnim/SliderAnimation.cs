﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderAnimation : MonoBehaviour {

	private bool isOpen, isMoving;
	[SerializeField]
	private RectTransform contentPanel;

	[SerializeField]
	private float lowerPosition, upperPosition;

	void OnEnable () {
		isOpen = false;
		contentPanel.anchoredPosition = new Vector2 (contentPanel.anchoredPosition.x, lowerPosition);
	}
	
	public void GearButtonClicked()
	{
		if (isOpen) {
			StartCoroutine (PlayAnimation (lowerPosition));
		} else {
			StartCoroutine (PlayAnimation (upperPosition));
		}
	}

	IEnumerator PlayAnimation(float position)
	{
		isOpen = !isOpen;
		float t = 0;
		Vector2 nextPosition = new Vector2 (contentPanel.anchoredPosition.x, position);
		while (t < 1) {
			t += Time.unscaledDeltaTime;
			contentPanel.anchoredPosition = Vector2.Lerp (contentPanel.anchoredPosition, nextPosition, t);
			yield return null;
		}
	}
}
