﻿using System.Collections;
using UnityEngine;
//using UnityEngine.Experimental.PlayerLoop;

public class Piece : MonoBehaviour {
    public Transform reference;
	public SpriteRenderer spriteRenderer;
   	public SpriteRenderer glow;
    public SphereCollider sc;
    
    public void InitializePiece(int index, Snake parameters)
    {
        sc.enabled = true;
        transform.parent = parameters.transform;
        if (index > 0)
        {
            //Debug.Log("Snake pieces Index:- " + parameters.snakePieces[index - 1].transform);
                reference = parameters.snakePieces[index - 1].transform;
        }
        else
        {
            reference = parameters.snakeHead.transform;
        }

        transform.position = reference.transform.position;
        
        UpdateGlowAlpha(parameters.GlowAlpha);
    }

    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1);
    }
    
    public void Move(float movLerpTime)
    {
        //Debug.LogFormat("Gamobject:- {0}  Reference:- {1} moveLerpTime:- {2} " , gameObject, reference, movLerpTime);

        //if (gameObject == null)
        //    return;
        //Debug.LogFormat("Transform:- {0} Reference:- {1}", gameObject, reference.gameObject);

        if(reference.transform != null)
        {
            transform.position = Vector3LerpXZ(transform.position, reference.transform.position, movLerpTime);      
        }
    }

    private Vector3 Vector3LerpXZ(Vector3 a, Vector3 b, float t)
    {
        return new Vector3(a.x + (b.x - a.x) * t, 0f, a.z + (b.z - a.z) * t);
    }

    public void UpdateGlowAlpha(float value)
    {
        var glowEffectColor = glow.color;
        glowEffectColor.a = value;
        glow.color = glowEffectColor;

        if (value <= 0 && glow.gameObject.activeSelf )
        {
            glow.gameObject.SetActive(false);
        } 
        else if (!glow.gameObject.activeSelf && value > 0f)
        {
            glow.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

}
