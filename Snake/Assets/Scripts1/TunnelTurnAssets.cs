﻿//using UnityEngine;
//using System.Collections;
//using Soomla.Store;
//using System.Collections.Generic;
//
//public class TunnelTurnAssets : IStoreAssets
//{
//    public int GetVersion() {
//        return 0;
//    }
//
//    public VirtualCurrency[] GetCurrencies() {
//		return new VirtualCurrency[]{};//ORBS_CURRENCY};
//    }
//
//    public VirtualGood[] GetGoods() {
//		return new VirtualGood[] {FIMO_LTVG};
//    }
//
//    public VirtualCurrencyPack[] GetCurrencyPacks() {
//		return new VirtualCurrencyPack[] {};//ORBS_PACK1, ORBS_PACK2, ORBS_PACK3, ORBS_PACK4};
//    }
//
//    public VirtualCategory[] GetCategories() {
//		return new VirtualCategory[]{};//GENERAL_CATEGORY};
//    }
//
////    public const string ORBS_PACK1_PRODUCT_ID = "android.test.refunded";
//
////    public static VirtualCurrency ORBS_CURRENCY = new VirtualCurrency(
////        "Orbs",                                      // name
////        "",                                             // description
////        ORBS_CURRENCY_ITEM_ID                         // item id
////    );
//
//
//    /** Virtual Currency Packs **/
//
////    public static VirtualCurrencyPack ORBS_PACK1 = new VirtualCurrencyPack(
////        "1 Zap",                                       // name
////		"Buy 1 Zap",                                 // description
////        ORBS1_ITEM_ID,                                   // item id
////        1,                                             // number of currencies in the pack
////        "zaps",                        // the currency associated with this pack
////		new PurchaseWithMarket(ORBS_CURRENCY_ITEM_ID, 0.99)
////    );
////
////    public static VirtualCurrencyPack ORBS_PACK2 = new VirtualCurrencyPack(
////		"3 Zap",                                   // name
////        "Buy 3 orbs",                 // description
////        ORBS2_ITEM_ID,                                   // item id
////        3,                                             // number of currencies in the pack
////		"zaps",                        // the currency associated with this pack
////		new PurchaseWithMarket(ORBS_PACK1_PRODUCT_ID, 1.99)
////    );
////
////	public static VirtualCurrencyPack ORBS_PACK3 = new VirtualCurrencyPack(
////		"5 Zap",                                   // name
////		"Buy 5 orbs",                 // description
////		ORBS3_ITEM_ID,                                   // item id
////		5,                                             // number of currencies in the pack
////		"zaps",                        // the currency associated with this pack
////		new PurchaseWithMarket(ORBS_PACK2_PRODUCT_ID, 3.99)
////	);
////
////	public static VirtualCurrencyPack ORBS_PACK4 = new VirtualCurrencyPack(
////		"10 Zap",                                   // name
////		"Buy 10 orbs",                 // description
////		ORBS4_ITEM_ID,                                   // item id
////		10,                                             // number of currencies in the pack
////		"zaps",                        // the currency associated with this pack
////		new PurchaseWithMarket(NO_ADS_LIFETIME_PRODUCT_ID, 6.99)
////	);
//	
//
////    public static VirtualGood ORBS_GOOD1 = new SingleUseVG(
////        "1 Zap",                                               // name
////        "Buy 1 Zap", // description
////		ORBS1_ITEM_ID,                                               // item id
////		new PurchaseWithMarket(ORBS_CURRENCY_ITEM_ID, 0.99)); // the way this virtual good is purchased
////
////    public static VirtualGood ORBS_GOOD2 = new SingleUseVG(
////		"3 Zaps",                                               // name
////		"Buy 3 Zaps", // description
////		ORBS2_ITEM_ID,                                               // item id
////		new PurchaseWithMarket(ORBS_PACK1_PRODUCT_ID, 1.99)); // the way this virtual good is purchased
////
////	public static VirtualGood ORBS_GOOD3 = new SingleUseVG(
////		"5 Zaps",                                               // name
////		"Buy 5 Zaps", // description
////		ORBS3_ITEM_ID,                                               // item id
////		new PurchaseWithMarket(ORBS_PACK2_PRODUCT_ID, 3.99)); // the way this virtual good is purchased
////
////	public static VirtualGood ORBS_GOOD4 = new SingleUseVG(
////		"10 Zaps",                                               // name
////		"Buy 10 Zaps", // description
////		ORBS4_ITEM_ID,                                               // item id
////		new PurchaseWithMarket(NO_ADS_LIFETIME_PRODUCT_ID, 6.99)); // the way this virtual good is purchased
//
//    /** Virtual Categories **/
//    // The muffin rush theme doesn't support categories, so we just put everything under a general category.
////    public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory(
////        "General", new List<string>(new string[] { "1", "2" })
////    );
//
//	public const string FIMO_PRODUCT_ID      		= "com.camc.slither.adremover";//"android.test.purchased";
//
//	public const string ORBS1_ITEM_ID   = "ad_remove";
//
//    /** LifeTimeVGs **/
//    // Note: create non-consumable items using LifeTimeVG with PuchaseType of PurchaseWithMarket
//    public static VirtualGood FIMO_LTVG = new LifetimeVG(
//        "Remove Ads",                                             // name
//        "Remove all ads",                                         // description
//		ORBS1_ITEM_ID,                                              // item id
//		new PurchaseWithMarket(FIMO_PRODUCT_ID, 0.99));  			// the way this virtual good is purchased
//}
