﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {
	public static string prefstr_Username = "PlayerName";
	public static int MaxPopulation = 20;
	public static int ReliableCount = 5;
	public static float GetPlusAngle(float ang)
	{
		if (ang < 0)
			return ang + 360f;
		return ang;
	}
	public static bool IsBig(float a1,float a2,float astart)
	{
		float ar1 = GetPlusAngle (a1 - astart);
		float ar2 = GetPlusAngle (a2 - astart);
		if (ar1 >= ar2)
			return true;
		return false;
	}
	public static bool IsSmall(float a1, float a2, float astart)
	{
		float ar1 = GetPlusAngle (astart - a1);
		float ar2 = GetPlusAngle (astart - a2);
		if (ar1 >= ar2)
			return true;
		return false;
	}
}

