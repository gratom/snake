﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reserved {
	public static string reqUpdateMe = "updateme";
	public static string rspServerUpdated = "serverupdated";
	public static string rspUserUpdated = "userupdated";
	public static string cmdKickUser = "cmdkickuser";
	public static string cmdSendAIData = "cmdsendaidata";
	//player data key
	public static string playerid = "playerid";
	public static string posx = "posx";
	public static string posz = "posz";
	public static string eulery = "eulery";
	public static string teulery = "teulery";
	public static string speedmul = "speedmul";
	public static string score = "score";
	public static string username = "username";
	public static string maskno = "maskno";
	public static string colno = "colno";
	public static string state = "state";
	public static string kickname = "kickname";


	//heapname
	public static string aiheap = "aiheap";
	public static string playerdataheap = "pldataheap";

	public static string aireportheap = "aireportheap";
	public static string arstate = "arstate";
	public static string archaseeuler = "arceuler";
	public static string arescapeeuler = "areeuler";
	public static string archasefood = "arcfood";
	public static string arscore = "arscore";

	//prefix
	public static string idprefix = "user-";
	public static string autoprefix = "player-";
}
