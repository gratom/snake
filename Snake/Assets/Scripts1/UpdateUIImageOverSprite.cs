﻿using UnityEngine;

public class UpdateUIImageOverSprite : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] private Transform uiTransform;
    [SerializeField] private float offsetAmount = 10.0f;
    [SerializeField] private Canvas canvas;

    private bool isDuelModeOn = false;
    private void OnEnable()
    {
        isDuelModeOn = false;

        if (GameManager.instance.IsDuelMode())
        {
            isDuelModeOn = true;
        }
    }

    void Update()
    {
        if(isDuelModeOn)
        {
            uiTransform.position = ScreenToUISpace(canvas, targetTransform.position);
            //uiTransform.position = (uiTransform.position - targetTransform.position).normalized * offsetAmount + uiTransform.position;
        }
        
    }

    public Vector3 ScreenToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        //Convert the world for screen point so that it can be used with ScreenPointToLocalPointInRectangle function
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;

        //Convert the screenpoint to ui rectangle local point
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        //Convert the local point to world point
        //Debug.Log(">>"+parentCanvas.transform.TransformPoint(movePos));
        return parentCanvas.transform.TransformPoint(movePos);
    }
}
