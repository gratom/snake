﻿using UnityEngine;
using System.Collections;
//using GooglePlayGames;
#if GPG
using GooglePlayGames.BasicApi;   //Commented out recently
#endif
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
public class AudioScript : MonoBehaviour
{
	public static AudioScript instance = null;

//	public int adCount = 1;
	public int reviewCount = 0;
	public AudioSource bgMusic;

	void Awake()
	{
		if(AudioScript.instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}
    
    public void SetSavedVolume()
	{
		bgMusic.volume = 1.0f;
		if (PlayerPrefs.HasKey (GameUtils.PREFS_SOUND_TOGGLE)) {
			if (PlayerPrefs.GetInt (GameUtils.PREFS_SOUND_TOGGLE) == 1) {
		
				if (PlayerPrefs.HasKey (GameUtils.PREFS_SOUND_VOLUME)) {
					bgMusic.volume = PlayerPrefs.GetFloat (GameUtils.PREFS_SOUND_VOLUME);
				} else {
					bgMusic.volume = 1.0f;
				}
			} else {
				bgMusic.volume = 0.0f;
			}
		}
	}

	void OnApplicationQuit()
	{
#if UNITY_ANDROID
//		LocalNotification.CancelNotification(1); //82800
		LocalNotification.SendNotification(1, 82800, "Snake Eater Slither Mask", "Hi there, come back and beat your record",
			new Color32(0xff, 0x44, 0x44, 255), true, true, true, "app_icon");
#endif
	}

	void Start()
	{
		SetSavedVolume ();

#if UNITY_ANDROID
        //Commented out recently
        //// recommended for debugging:
        //PlayGamesPlatform.DebugLogEnabled = true;
        //PlayGamesPlatform.Activate();
#endif

        Social.localUser.Authenticate(success => {
            if (success)
            {
                //				Debug.Log("Authentication successful");
                string userInfo = "Username: " + Social.localUser.userName +
                    "\nUser ID: " + Social.localUser.id +
                    "\nIsUnderage: " + Social.localUser.underage;
                //				Debug.Log(userInfo);

               // AchievementManager._instance.PlaySessionCount();
            }
            else
            {
                Debugger.instance.DebugLog("Authentication failed");
               // Debug.Log("Authentication failed");
            }
		});
	}
}