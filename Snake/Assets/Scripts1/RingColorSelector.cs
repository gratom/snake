﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RingColorSelector : MonoBehaviour, IPointerClickHandler {

	// Use this for initialization
	public int ringNumber;
	public GameObject highLighter;

	void Start () {
		highLighter.SetActive (false);
	}

	public void OnPointerClick(PointerEventData eventData)
	{

		if (ringNumber == 1) {
			if (!ColorSelectionManager._instance.canUseColorOfFirstRing)
				return;
		}
		else if (ringNumber == 2) {
			if (!ColorSelectionManager._instance.canUseColorOfSecondRing)
				return;
		}

		else if (ringNumber == 3) {
			if (!ColorSelectionManager._instance.canUseColorOfThirdRing)
				return;
		}

		Debug.Log ("You have clicked the ring color selector $$$$$$$$$$$$$$$$");
		ColorSelectionManager._instance.SetSelectedColorRing (ringNumber);

	}

	public void DeactivateHighlighter()
	{
		highLighter.SetActive (false);
	}

	public void ActivateHighlighter()
	{
		highLighter.SetActive (true);
	}
}
