﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskManager : MonoBehaviour {
	public static MaskManager instance;
	public Sprite[] masks;
    public Sprite[] animals;
    public Sprite[] characters;
    public Sprite[] faces;
    public Sprite[] flags;
    public Sprite[] pros;
    public Sprite[] eggs;
    public Sprite[] hats;

    public int tabIndex = 0;
    public int hatCount = 2;
	// Use this for initialization
	void Awake()
	{
		instance = this;
        SetTab(0);

    }

    public void SetTab(int tab)
    {
        tabIndex = tab;
        switch (tabIndex)
        {
            case 0:
                masks = animals;
                break;
            case 1:
                masks = characters;
                break;
            case 2:
                masks = faces;
                break;
            case 3:
                masks = flags;
                break;
            case 4:
                masks = pros;
                break;
            case 5:
                masks = eggs;
                break;
            default:
                masks = animals;
                break;
        }
        
    }

    public int GetRandomTab()
    {
        return Random.Range(0, 6);
    }

	public int GetRandomMask(int maskTab)
	{
        //return -1;      //To disable masks temporarily
        int randomMask = -1;// Random.Range(0,MaskManager.instance)
        switch (maskTab)
        {
            case 0:
                randomMask = Random.Range(0, MaskManager.instance.animals.Length);
                break;
            case 1:
                randomMask = Random.Range(0, MaskManager.instance.characters.Length);
                break;
            case 2:
                randomMask = Random.Range(0, MaskManager.instance.faces.Length);
                break;
            case 3:
                randomMask = Random.Range(0, MaskManager.instance.flags.Length);
                break;
            case 6:
                randomMask = Random.Range(0, MaskManager.instance.hats.Length);
                break;
            case 4:
                randomMask = Random.Range(0, MaskManager.instance.pros.Length);
                break;
            case 5:
                randomMask = Random.Range(0, MaskManager.instance.eggs.Length);
                break;
        }

        int randomtype = Random.Range(0, 2);
		int maskno = -1;
		if (randomtype == 0)        
        {
            maskno = randomMask;

        }
       

        return maskno;
	}
	public bool IsHat(int maskNo)
	{
		if (maskNo < masks.Length - hatCount)
			return false;
		return true;
	}

	public bool IsSmiley(int maskNo)
	{
		if (maskNo >= 64 && maskNo <= 70) {
			return true;
		} else {
			return false;
		}
	}

    public Sprite GetRandomMask()
    {
        int rndTab = Random.Range(0, 6);
        switch (rndTab)
        {
            case 0:
                return animals[Random.Range(0,animals.Length)];
            case 1:
                return characters[Random.Range(0, characters.Length)];
            case 2:
                return faces[Random.Range(0, faces.Length)];
            case 3:
                return flags[Random.Range(0, flags.Length)];
            case 4:
                return pros[Random.Range(0, pros.Length)];
            case 5:
                return eggs[Random.Range(0, eggs.Length)];
            default:
                return null;
        }
    }

    public Sprite GetMask(int maskNum,int maskTab)
    {
        switch (maskTab)
        {
            case 0:
                return animals[maskNum];
            case 1:
                return characters[maskNum];
            case 2:
                return faces[maskNum];
            case 3:
                return flags[maskNum];
            case 4:
                return pros[maskNum];
            case 5:
                return eggs[maskNum];
            default:
                return null;
        }

        return null;
    }
}
