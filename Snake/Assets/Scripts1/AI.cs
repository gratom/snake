﻿using UnityEngine;
using System.Collections;

public enum AIType {
    Normal = 0,
    Team = 1,
    Baby = 2

}

public class AI : MonoBehaviour {

    public Vector3 direction;
	private Snake snake;
   	public bool sprint;
	public int counter;
    private bool isDuelOn = false;
    public AIType aiType = AIType.Normal;
   

    void Start()
    {
		snake = transform.parent.parent.GetComponent<Snake>();
		StartCoroutine(changeDirectionRandomlyForRoaming());
        StartCoroutine(sprintRandomly());

    }
    private void OnEnable()
    {
        isDuelOn = GameManager.instance.IsDuelMode();
        if(isDuelOn)
        {
            gameObject.transform.localPosition = new Vector3(-0.5f, 0f, -9f);
            gameObject.GetComponent<SphereCollider>().radius = 20;
        }
        else
        {
            gameObject.transform.localPosition = new Vector3(0f, 0f, 0f);
            gameObject.GetComponent<SphereCollider>().radius = 12.52f;
        }
    }

    IEnumerator sprintRandomly()
    {
        float waitTime = 3f;
        while (true)
        {
            int random = Random.Range(0, 4);
            if (random == 0)
            {
                sprint = true;
            }
            else
            {
                sprint = false;
            }
            yield return new WaitForSeconds(waitTime);

        }
    }

    IEnumerator changeDirectionRandomlyForRoaming()
    {
        float waitTime = 5;
        Vector3 startingPoint = transform.position;
      
        while (true) {
            Vector3 circle = Random.insideUnitSphere * 350; //250
            circle.y = startingPoint.y;
            direction = circle - transform.position;
            direction *= 1.2f;
            yield return new WaitForSeconds(waitTime);
        }
    }
    
    /*void OnTriggerStay(Collider obj) {
		if (obj.tag == "Snake" && obj.transform.root != transform.root)
        {
			//Debug.LogError ("Escape from Front : "+obj.transform.root.name + " : "+transform.root.name);
            Escape(obj);
        }
    }*/

    void OnTriggerEnter(Collider obj)
    {
		if (obj.CompareTag("Food"))
        {
            Chase(obj);
        }

        if (aiType == AIType.Normal)
        {
            if (obj.CompareTag("Snake") && obj.transform.root != transform.root)
            {
                if (isDuelOn)
                {
                    //    if (counter % 5 == 0)
                    //    {
                    //        Chase(obj);
                    //    }
                    //    else
                    //    {
                    Escape(obj);
                    //}
                }
                else
                {
                    if (counter % 8 == 0)
                    {
                        Escape(obj);
                        //Debug.Log("Snake escaped");
                    }
                    else if (counter % 8 == 6)
                    {
                        Chase(obj);
                        //Debug.Log("Snake chased");
                    }
                    counter++;
                }



                //if (counter % 8 == 0)
                //{
                //    Escape(obj);
                //    //Debug.Log("Snake escaped");
                //}
                //else if (counter % 8 == 6)
                //{
                //    Chase(obj);
                //    //Debug.Log("Snake chased");
                //}
                //counter++;

            }
        }
        else
        {
            if (obj.CompareTag("Snake") && obj.transform.root != transform.root)
            {
                
                Snake objectSnake = obj.transform.root.GetComponent<Snake>();
                if (snake != null&& objectSnake!=null)
                {
                    if (objectSnake.snakeTeam == snake.snakeTeam)
                        return;
                }
                else
                    return;
            }
            else
                return;
      

                if (counter % 8 == 0)
                {
                    Escape(obj);
                 //   Debug.Log(snake.snakeName + " Snake escaped");
                }
                else if (counter % 8 == 6)
                {
                    Chase(obj);
                //    Debug.Log(snake.snakeName + " Snake chased");
                //Debug.Log("Snake chased");
                }
                counter++;

            
        }
    }

    void Escape(Collider obj) {
        direction = transform.position - obj.transform.position;
        direction.y = transform.position.y;
        direction *= 1.2f;
        int random = Random.Range(0, 3);
        if (random == 0)
        {
            sprint = true;
        }
        else
        {
            sprint = false;
        }

        //Debug.Log("Escaping--");

    /*    direction = transform.position - obj.transform.position;
        direction.y = transform.position.y;
        int random = Random.Range(0, 6 + 1);
        if (random == 0)
        {
            sprint = true;
        }
        else
        {
            sprint = false;
        }
		float eulery = Quaternion.LookRotation (direction).eulerAngles.y;
		if(eulery < 0)
			eulery += 360f;
		if (snake != null) {
			if (snake.netPlayerData != null) {
				if (snake.netPlayerData.aiReport != null) {
					snake.netPlayerData.aiReport.escapeEuler = eulery;
					snake.netPlayerData.aiReport.escapesent = Constants.ReliableCount;
				}
			}
		}*/
    }

    void FollowPlayer()
    {

    }

    void Chase(Collider obj)
    {
        direction = obj.transform.position - transform.position;
        direction.y = transform.position.y;
        direction *= 1.2f;
        //Debug.Log("--Chasing");

        //		if (Vector3.Distance (transform.position, Vector3.zero) > Population.instance.spawnCircleLenght * 0.8)
        //			return;
        /*    direction =  obj.transform.position-transform.position;
            direction.y = transform.position.y;
            float eulery = Quaternion.LookRotation (direction).eulerAngles.y;
            if(eulery < 0)
                eulery += 360f;
            if (snake != null) {
                if (snake.netPlayerData != null) {
                    if (snake.netPlayerData.aiReport != null) {
                        snake.netPlayerData.aiReport.chaseEuler = eulery;
                        snake.netPlayerData.aiReport.chasesent = Constants.ReliableCount;
                    }
                }
            }*/
    }
}





