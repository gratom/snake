﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MaskScript : MonoBehaviour
{

	public GameObject Eyes;
	public GameObject Mask;
	public GameObject Hat;
	public GameObject head;
	public GameObject smiley;
	public SpriteRenderer smileyRenderer;
	public bool isPlayer;

	// Use this for initialization

	void OnEnable()
	{
		/*
			Color maskAlphaColor = Mask.GetComponent<SpriteRenderer> ().color;
			Color headAlphaColor = head.GetComponent<SpriteRenderer> ().color;

			if (PlayerPrefs.HasKey ("PlayerTransparency")) {

			} else {
				PlayerPrefs.SetFloat ("PlayerTransparency", 1);
				PlayerPrefs.Save ();
			}

			Debug.Log ("Current alpha valus is:  --------------------------------:" + PlayerPrefs.GetFloat ("PlayerTransparency"));

		if (isPlayer) {
			maskAlphaColor.a = (1 - PlayerPrefs.GetFloat ("PlayerTransparency"));
			headAlphaColor.a = (1 - PlayerPrefs.GetFloat ("PlayerTransparency"));
			Debug.Log ("%%%%%%%%%%%%%%%%%%: " + headAlphaColor.a);
		} else {
			maskAlphaColor.a = 0.75f;
			headAlphaColor.a = 0.75f;
		}
			Mask.GetComponent<SpriteRenderer> ().color = maskAlphaColor;
			head.GetComponent<SpriteRenderer> ().color = headAlphaColor;
			*/
		
	}
    
	public void SetMask(int maskNo,int tabIndex,Color maskColor)
	{
        Debug.Log(tabIndex);
		//Debug.Log ("Coming again and again ===============================");
		if (maskNo >= 0)
        {
            if (tabIndex == 6)
            {
                Eyes.SetActive(true);
                Hat.SetActive(true);
                Mask.SetActive(false);
                smiley.SetActive(false);
                Hat.GetComponent<SpriteRenderer>().sprite = MaskManager.instance.hats[maskNo];
            }
            else if (tabIndex == 2)
            {
                Eyes.SetActive(false);
                Hat.SetActive(false);
                Mask.SetActive(false);
                smiley.SetActive(true);
              //  head.SetActive(false);
                if (LooknFeelDisplay._instance != null)
                    smiley.GetComponent<SpriteRenderer>().color = LooknFeelDisplay._instance.color1;
                smileyRenderer.sprite = MaskManager.instance.faces[maskNo];
              //  smiley.GetComponent<Image>().color = maskColor;
            }
            else
            {
                Eyes.SetActive(false);
                Hat.SetActive(false);
                Mask.SetActive(true);
                smiley.SetActive(false);
                if (tabIndex == 0)
                {
                    Mask.GetComponent<SpriteRenderer>().sprite = MaskManager.instance.animals[maskNo];
                }
                else if (tabIndex == 1)
                {
                    Mask.GetComponent<SpriteRenderer>().sprite = MaskManager.instance.characters[maskNo];
                }
                else if (tabIndex == 3)
                {
                    Mask.GetComponent<SpriteRenderer>().sprite = MaskManager.instance.flags[maskNo];
                }
                else if (tabIndex == 4)
                {
                    Mask.GetComponent<SpriteRenderer>().sprite = MaskManager.instance.pros[maskNo];
                }
                else if (tabIndex == 5)
                {
                    Mask.GetComponent<SpriteRenderer>().sprite = MaskManager.instance.eggs[maskNo];
                }
            }
			
        }
        else
        {
			Eyes.SetActive (true);
			Hat.SetActive (false);
			Mask.SetActive (false);

            Debug.Log("MaskScript-Else called");
		}
	}
}
