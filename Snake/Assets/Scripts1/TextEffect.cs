﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TextEffect : MonoBehaviour {

	public static TextEffect _instance; 


	public GameObject[] textEffects;

	void Awake()
	{
		_instance = this;
	}
	public void ShowAnEffect()
	{
		int rand = Random.Range (0,3);
		foreach (GameObject g in textEffects) {
			if (textEffects [rand] == g) {
				g.SetActive (true);
				g.transform.DOShakeRotation (2, 45, 5, 20, false);
			} else {
				g.SetActive (false);
			}
		}
		Invoke ("DisableALlEffects", 3);
	}


	
	void DisableALlEffects()
	{
		foreach (GameObject g in textEffects) {
			g.transform.rotation = Quaternion.identity;
			g.SetActive (false);
		}
	}
}
