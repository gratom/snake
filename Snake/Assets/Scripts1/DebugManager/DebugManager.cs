﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DebugManager : MonoBehaviour {
	public static DebugManager instance;
	// Use this for initialization
	public int debugLines = 30;
	public bool DebugMode;
	private bool bStopped = false;
	private int curStartNo = 0;
	Text debugText;
	List<string> debugList = new List<string> ();
	void Awake()
	{
		debugText = transform.GetChild (0).GetComponent<Text> ();
		instance = this;
	}
	void Start () {
	}

	void OnEnable () {
		Application.RegisterLogCallback(HandleLog);
	}
	void HandleLog (string logString, string stackTrace, LogType type) {
		if (!DebugMode)
			return;
		
		//output = logString;
		//stack = stackTrace;
		//report ("Log:>>>" +logString);
		//report ("Trace:>>>" + stackTrace);
	}

	void OnDisable () {
		// Remove callback when object goes out of scope
		Application.RegisterLogCallback(null);
	}
	// Update is called once per frame
	void Update () {
		if (DebugMode) {
			if (!gameObject.activeSelf) {
				gameObject.SetActive (true);
			}
		} else {
			if (gameObject.activeSelf) {
				gameObject.SetActive (false);
			}
		}
	}
	public void report(string content)
	{
		if (!DebugMode)
			return;
		
		if (bStopped)
			return;
		string[] contentlines = content.Split ("\n"[0]);
		foreach(string str in contentlines)
		{
			debugList.Add (str);
		}
		debugText.text = "";
		int startno = debugList.Count - debugLines;
		if (startno < 0)
			startno = 0;
		curStartNo = startno;
		for (int i = startno; i < debugList.Count; i++) {
			debugText.text += debugList[i] + "\n";
		}
	}
	public void OnBtxPrevClick()
	{
		debugText.text = "";
		int startno = curStartNo - debugLines;
		if (startno < 0)
			startno = 0;
		curStartNo = startno;
		int endno = startno + debugLines;
		if (endno > debugList.Count - 1)
			endno = debugList.Count - 1;
		for (int i = startno; i <= endno; i++) {
			debugText.text += debugList[i] + "\n";
		}
	}
	public void OnBtnNextClick()
	{
		debugText.text = "";
		int startno = curStartNo + debugLines;
		if (startno < 0)
			startno = 0;
		if (startno > debugList.Count - 1)
			startno = debugList.Count - 1;
		curStartNo = startno;
		int endno = startno + debugLines;
		if (endno > debugList.Count - 1)
			endno = debugList.Count - 1;
		for (int i = startno; i <= endno; i++) {
			debugText.text += debugList[i] + "\n";
		}
	}
	public void OnBtnStopClick()
	{
		bStopped = !bStopped;
	}
}
