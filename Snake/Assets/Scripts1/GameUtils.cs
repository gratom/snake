﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUtils {

	public const string PREFS_FIRST_KILL = "PREFS_FIRST_KILL";
	public const string PREFS_FIVE_KILL = "PREFS_FIVE_KILL";
	public const string PREFS_TEN_KILL = "PREFS_TEN_KILL";
	public const string PREFS_HUNDRED_KILL = "PREFS_HUNDRED_KILL";
	public const string PREFS_EDIT_SNAKE = "PREFS_EDIT_SNAKE";
	public const string PREFS_SOCIAL_MEDIA = "PREFS_SOCIAL_MEDIA";
	public const string PREFS_TWO_HUNDRED_POINTS = "PREFS_TWO_HUNDRED_POINTS";
	public const string PREFS_ONE_LAKH_POINTS = "PREFS_ONE_LAKH_POINTS";
	public const string PREFS_SECOND_PLAY = "PREFS_SECOND_PLAY";
	public const string PREFS_BUY_TRANSPARENCY = "PREFS_BUY_TRANSPARENCY";
	public const string PREFS_KILL_COUNTER = "PREFS_KILL_COUNTER";
	public const string PREFS_COLOUR_CHANGED = "PREFS_COLOUR_CHANGED";
	public const string PREFS_MASK_CHANGED = "PREFS_MASK_CHANGED";
	public const string PREFS_PLAY_SECOND_TIME = "PREFS_PLAY_SECOND_TIME";
	public const string PREFS_INITIAL_SNAKE_COLOUR = "PREFS_INITIAL_SNAKE_COLOUR";
	public const string PREFS_CURRENT_SNAKE_COLOUR = "PREFS_CURRENT_SNAKE_COLOUR";
	public const string PREFS_SOUND_TOGGLE = "PREFS_SOUND_TOGGLE";
	public const string PREFS_SOUND_VOLUME = "PREFS_SOUND_VOLUME";
	public const string PREFS_JOYSTICK_CONTROL_MODE = "PREFS_JOYSTICK_CONTROL_MODE";
	public const string PREFS_JOYSTICK_CONTROL_HAND = "PREFS_JOYSTICK_CONTROL_HAND";
	public const string PREFS_MSPORTS_UPDATE = "PREFS_MSPORTS_UPDATE";

}
