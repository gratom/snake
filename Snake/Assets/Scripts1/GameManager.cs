﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

#if UNITY_ANDROID

using Unity.Notifications.Android;

#endif

#if UNITY_IPHONE
using Unity.Notifications.iOS;
#endif
//using UnityEngine.Purchasing;     //Commented out recently
//using Sfs2X.Entities; //Commented out recently

public enum GameMode
{
    Normal = 0,
    DuelMode = 1,
    TeamMode = 2
}

public enum TeamMode
{
    _2x2 = 0,
    _3x3 = 1
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameManager Instance => instance;

    public bool ifAdRemoveClicked;
    public bool dev_SpeedUp = false;

    //  public List<Snake> snakeDiedList = new List<Snake>();
    // public List<Transform> pieceDiedList = new List<Transform>();

    public ECSSnake playerSnake;
    public Snake babySnake;
    public GameObject mainMenuCanvas;
    public GameObject teamModeCanvas;
    public GameObject settingsCanvas;
    public GameObject InGameCanvas;
    public GameObject InGameJoystickCanvas;
    public GameObject inGameJoystickHomeButton;
    public GameObject gameOverCanvas;
    public GameObject gameOverDuelCanvas;
    public GameObject duelWonImage;
    public GameObject gameOverImage;
    public GameObject maskCanvas;
    public GameObject skinsCanvas;
    public GameObject scoreboardCanvas;
    public GameObject scoreboardCanvas_Duel;
    public GameObject scoreboardCanvas_Team;
    public GameObject snakeLooknFellBoard;
    public GameObject loadingGUI;
    public GameObject duelModeObject;
    public GameObject EggInAppPage;
    public GameObject BundleOfferPage;
    public GameObject bundleBtn;
    public Text loadingGuiText;
    public GameObject rewardBtn;
    public Text InGameLenghtText;
    public Text GameOverLenghtText;
    private string inGameLTString, GameOverLTstring;
    private string leaderboard;
    private string duelleaderboard;
    public string iOSLeaderboard;
    public GameObject continueBtn;
    public GameObject continueBtnDuelMode;
    public GameObject get2EggsBtn;
    public GameObject AdRemoveBtn;
    public GameObject babySnakeBtn;
    public GameObject Speed;
    public int playerMaskNo = -1;
    public GameObject DB1, DB2, DB3, DB4;
    public GameObject loadingOverlay;
    public GameObject duelTimer;
    public GameObject duelReadyCount;
    public Camera cam;
    public Vector3 hidePos = Vector3.up * 1000f;
    public Vector3 snakeLooknFeel_DefaultPos;
    public Vector3 snakeLooknFeel_SkinsPos;
    public Vector3 snakeLooknFeel_DefaultScale;
    public Vector3 snakeLooknFeel_SkinsScale;
    private bool speedCheck = false;
    public int lastSnakePoints = 50;
    public GameMode gameMode = GameMode.Normal;
    public TeamMode teamMode = TeamMode._2x2;
    public bool isDuelTimerOver = false;
    public Vector3 duelScreenOffsetPosition;
    public TimerCountdown duelModeTimer;
    public Text duelModeWinText;
    public Text duelModeLooseText;
    public Transform chosenPlayerSpawnPoint;

    public Text debug2Text;

    //public bool isHomeButtonPressed = false;
    public GameObject dialogBox;

    public GameObject babyBox;
    public GameObject eggBuyBox;
    private bool isPlayerActive;

    public MaskGUIScript maskGUI;

    public bool IsRewardedVideo = false;

    public bool IsPlayerActive
    {
        get => isPlayerActive;
        set => isPlayerActive = value;
    }

    private int userRoomID;

    public int UserRoomID
    {
        get => userRoomID;
        set => userRoomID = value;
    }

    public ECSSnake playerSnakeParam;

    // Use this for initialization

    public int MaxWormPieceLength = 100;
    public float MaxSnakeScale = 2.5f;

    [Header("Stats")]
    [SerializeField] private PlayerStatsManager _statsManager;

    public int PlayerKills { get; set; }
    public long SurvivalTimeTicks { get; set; }
    public long TopOneTimeTicks { get; set; }
    private bool _isTopOneTrackingEnabled;
    public DateTime TopOneStartTime { get; set; }

    private void Awake()
    {
        instance = this;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Debug.unityLogger.logEnabled = false;
        inGameLTString = InGameLenghtText.text;
        GameOverLTstring = GameOverLenghtText.text;

        UpdateDuelModeStates();
    }

    public ECSSnake GetPlayerSnakeParams()
    {
        if (playerSnakeParam != null)
        {
            return playerSnakeParam;
        }
        else
        {
            /*  if (playerSnake.activeInHierarchy)
                  playerSnakeParam = playerSnake.GetComponent<Snake>();
              else*/
            playerSnakeParam = null;

            return playerSnakeParam;
        }
    }

    public void UpdateDuelModeStates()
    {
        duelModeWinText.text = PlayerPrefs.GetInt("DuelModeWins", 0).ToString();
        duelModeLooseText.text = PlayerPrefs.GetInt("DuelModeLoose", 0).ToString();
    }

    public void Init()
    {
#if UNITY_ANDROID
        AndroidNotificationCenter.CancelAllScheduledNotifications();
        AndroidNotificationChannel c = new AndroidNotificationChannel()
        {
            Id = "channel_id",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
#endif
#if UNITY_IPHONE
        StartCoroutine("RequestAuthorization");
#endif
    }

#if UNITY_IPHONE
    IEnumerator RequestAuthorization()
    {
        using (var req = new AuthorizationRequest(AuthorizationOption.Alert | AuthorizationOption.Badge, true))
        {
            while (!req.IsFinished)
            {
                yield return null;
            };

            string res = "\n RequestAuthorization: \n";
            res += "\n finished: " + req.IsFinished;
            res += "\n granted :  " + req.Granted;
            res += "\n error:  " + req.Error;
            res += "\n deviceToken:  " + req.DeviceToken;
            Debug.Log(res);
        }
    }
#endif

    [Header("Debug Mode")]
    public bool isMSportsMode = false;

    private void Start()
    {
        PlayerPrefs.SetInt("WatchedAd", 0);
        //Debug.Log("Loading over");
        loadingOverlay.SetActive(false);
        Initialize();
        Init();
        SetupNotifications();
        Invoke("RevokeEggCheck", 1f);
    }

    public void RevokeEggCheck()
    {
        if ((PlayerPrefs.GetInt("ads", 1) == 0) && (PlayerPrefs.GetInt("babysnake", 0) == 1) && (PlayerPrefs.GetInt("transparency", 1) == 0) && (PlayerPrefs.GetInt("bundle", 0) == 0))
        {
            if (PlayerPrefs.GetInt("goteggs", 0) == 0)
            {
                PlayerPrefs.SetInt("goteggs", 1);
                PlayerPrefs.SetInt("bundle", 1);
                Debug.Log("egg increased");
                EggsManager.instance.IncreaseEgg(30);
            }
        }

        if (PlayerPrefs.GetInt("bundle", 0) == 1)
        {
            bundleBtn.SetActive(false);
        }
    }

    public bool IsDuelMode()
    {
        return (gameMode == GameMode.DuelMode);
    }

    public bool IsTeamMode()
    {
        return (gameMode == GameMode.TeamMode);
    }

    public void SetNormalMode()
    {
        gameMode = GameMode.Normal;
    }

    public void SetDuelMode()
    {
        gameMode = GameMode.DuelMode;
#if EVENTS
        Dictionary<string, object> vals = new Dictionary<string, object>
        {
            { "level", "Duel" }
        };
        AppMetrica.Instance.ReportEvent("level_start", vals);
        AppMetrica.Instance.SendEventsBuffer();
#endif
    }

    public bool watchedAd = false;

    public void Initialize()
    {
        if (watchedAd)
        {
            watchedAd = false;
            OnPlayWithAI();
        }
        else
        {
            if (PlayerPrefs.GetInt("ads", 1) == 0) //1=show-ad 0=dont-show-ad
            {
                AdRemoveBtn.SetActive(false);
            }
        }

        if (PlayerPrefs.GetInt("babysnake", 0) == 1) //1=show-ad 0=dont-show-ad
        {
            babySnakeBtn.SetActive(false);
        }

#if UNITY_ANDROID
        //leaderboard = SnakeMask.leaderboard_high_score;
        leaderboard = GPGSID.leaderboard_slither_high_score;
        duelleaderboard = GPGSID.leaderboard_duel_mode_leaderboard;
#elif UNITY_IOS

        leaderboard = iOSLeaderboard;
        duelleaderboard = "com.camc.WormSlitherTeams.duelleaderboard";
#endif

        if (PlayerPrefs.GetInt("PlayerdFirstTime") == 1)
        {
            if (PlayerPrefs.GetInt("AutomaticPlay") == 1)
            {
                MSportsPlayCall();
            }
            else
            {
                PlayerPrefs.SetInt("AutomaticPlay", 1);
                PlayerPrefs.Save();
            }
        }
    }

    public void OpenBundleOffer()
    {
        BundleOfferPage.SetActive(true);
    }

    public void OpenEggInAppPage()
    {
        EggInAppPage.SetActive(true);
    }

    //DEV_EDIT || SNAKE SPEED CHANGE 2
    public void SnakeSpeedManager(bool anyValue)
    {
        dev_SpeedUp = anyValue;
        if (SnakeSpawner.Instance.playerSnake != null)
        {
            if (anyValue)
            {
                SnakeSpawner.Instance.StartSprint(SnakeSpawner.Instance.playerSnake);
            }
            else
            {
                SnakeSpawner.Instance.playerSnake.sprinting = false;
            }
        }
    }

    private void Update()
    {
        //DEV_EDIT || SNAKE SPEED CHANGE 2
        if (dev_SpeedUp)
        {
            GameManager.instance.SpeedUp();
        }
        else
        {
            GameManager.instance.SpeedDown();
        }

        if (SnakeSpawner.Instance.playerSnake != null)
        {
            if (watchedAd)
            {
                dev_SpeedUp = false;
                // SnakeSpawner.Instance.playerSnake.points = PlayerPrefs.GetInt("points");
                // Debug.Log("GM update:- points updated after ad");
                watchedAd = false;
            }
            InGameLenghtText.text = SnakeSpawner.Instance.playerSnake.points.ToString("0");   //inGameLTString + Snake.player.points;
            GameOverLenghtText.text = SnakeSpawner.Instance.playerSnake.points.ToString("0"); //GameOverLTstring + Snake.player.points;
        }

        if (IsDuelMode())
        {
            if (isDuelTimerOver)
            {
                SnakeManager.instance.DuelTimerOverDecision();
                isDuelTimerOver = false;
            }
        }
    }

    public void DestroySnake(Snake snake)
    {
        if (snake == null)
        {
            return;
        }

        if (IsDuelMode())
        {
            snake.points = 0;
        }
        //  else
        //   Population.instance.RemoveSnake(snake);

        SnakeManager.instance.DestroyCompleteSnake(snake);
        /*    for (int i = 1; i < snake.snakePieces.Count; i++)
            {
             //   snake.snakePieces[i].SetParent(null);
              //  snake.snakePieces[i].sc.enabled = false;
                SnakeManager.instance.DestroyPiece(snake.snakePieces[i].gameObject);
              /*  snake.snakePieces[i].SetParent(null);
                snake.snakePieces[i].position = hidePos;
                snake.snakePieces[i].GetComponent<SphereCollider>().enabled = false;
                //snake.snakePieces[i].GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color = glowEffectColor;
                Color glowEffectColor = snake.snakePieces[i].GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color;
                glowEffectColor.a = 0;
                snake.snakePieces[i].GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color = glowEffectColor;
                pieceDiedList.Add(snake.snakePieces[i]);*/
        //   }

        //  snake.gameObject.SetActive(false);
        //       if (!snake.isPlayer)
        //            snakeDiedList.Add(snake);
    }

    public void ExtendDuelModeTimer()
    {
        ResumeDuel();
    }

    public void ExtendDuelMode()
    {
        SnakeSpawner.Instance.playerSnake.isDuelModeDestroyed = false;
        SnakeSpawner.Instance.isGameStarted = true;
        SnakeSpawner.Instance.UnPauseAllSnakes();
        // SnakeManager.instance.UnpauseDuelModeSnakes();
        duelModeTimer.ExtendTime();
    }

    public void PlayerLoseDuels()
    {
        if (PlayerPrefs.GetInt("PlayerWon", 0) == 1)
        {
            return;
        }

        int duelModeLoose = PlayerPrefs.GetInt("DuelModeLoose", 0);
        duelModeLoose++;
        PlayerPrefs.SetInt("DuelModeLoose", duelModeLoose);
        UpdateDuelModeStates();
    }

    public void OnGameOver_Event()
    {
        SnakeSpawner.Instance.isGameStarted = false;
        // Post score MSports
        //MSportsManager._instance.PostMSportsTournamentScore();
        // Debug.Log("GameOverEvent Called");
        IncreaseSurvivingTime();
        StopTopOneTracking();

        dev_SpeedUp = false;
        IsPlayerActive = false;

        //if (!isHomeButtonPressed)         //recent
        //    gameOverCanvas.SetActive(true);

        //isHomeButtonPressed = false;

        //Debug.Log("Game over canvas active!");
        gameOverDuelCanvas.SetActive(false);
        mainMenuCanvas.SetActive(false);
        maskCanvas.SetActive(false);
        snakeLooknFellBoard.SetActive(false);
        skinsCanvas.SetActive(false);
        InGameJoystickCanvas.SetActive(false);
        InGameCanvas.SetActive(false);
        scoreboardCanvas.SetActive(false);
        scoreboardCanvas_Team.SetActive(false);
#if IRONSOURCE
        // Ads
        if (ISManager.instance.isVideoAvailable())    //All plugins disabled because of game crashing situation
        {
            continueBtn.SetActive(true);
        }
        else
        {
            continueBtn.SetActive(false);
        }

#endif

#if APPLOVIN
        // Ads
        if (AppsLovinManager.instance.isVideoAvailable())    //All plugins disabled because of game crashing situation
        {
            continueBtn.SetActive(true);
        }
        else
        {
            continueBtn.SetActive(false);
        }

#endif

#if YODO
        if(YodoManager.instance.IsRewardAdReady())
        {
            continueBtn.SetActive(true);
        }
        else
        {
            continueBtn.SetActive(false);
        }
#endif
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(SnakeSpawner.Instance.lastPoints, leaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");
                }
                else
                {
                    Debug.Log("Update Score Fail");
                }
            });
            AchievementManager._instance.CheckPointsAchievements(SnakeSpawner.Instance.lastPoints);        //Commented out recently
        }
        else
        {
            /* Social.localUser.Authenticate((bool success) =>
             {
                 if (success)
                 {
                     Debug.Log("Login Sucess");
                 }
                 else
                 {
                     Debug.Log("Login failed");
                 }
             });*/
        }

        if (isMSportsMode)
        {
            // Debug.Log("Start Score Posting From Here");
            // Post score MSports
        }

        int gameOverTimes = PlayerPrefs.GetInt("gameOver", 0);
        gameOverTimes++;
        switch (gameOverTimes)
        {
            case 2:
            case 4:
            case 6:
                GameManager.instance.BundleOfferPage.SetActive(true);
                break;
        }
        PlayerPrefs.SetInt("gameOver", gameOverTimes);
#if EVENTS
        Dictionary<string, object> vals = new Dictionary<string, object>();

        if (gameMode == GameMode.Normal)
        {
            vals.Add("level", "Normal");
        }
        else
        {
            vals.Add("level", "Team");
        }
        vals.Add("result", "Lose");
        vals.Add("time", (int)(TimeSpan.FromTicks(SurvivalTimeTicks).TotalSeconds));
        vals.Add("skin_name", "");
        vals.Add("progress", 0);
        AppMetrica.Instance.ReportEvent("level_finish", vals);
        AppMetrica.Instance.SendEventsBuffer();
#endif
    }

    public void SetNotification(string title, string desc, System.DateTime time)
    {
#if UNITY_ANDROID

        AndroidNotification notification = new AndroidNotification
        {
            Title = title,
            Text = desc,

            FireTime = time
        };

        AndroidNotificationCenter.SendNotification(notification, "channel_id");

#endif

#if UNITY_IPHONE

            var timeTrigger = new iOSNotificationTimeIntervalTrigger()
            {
                TimeInterval = new TimeSpan((24), 0, 0),
                Repeats = false
            };

            var notification = new iOSNotification()
            {
                // You can optionally specify a custom identifier which can later be
                // used to cancel the notification, if you don't set one, a unique
                // string will be generated automatically.
                Identifier = "_notification_0"+10,
                Title = title,
                Body = desc,
                Subtitle = "",
                ShowInForeground = true,
                ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
                CategoryIdentifier = "category_a",
                ThreadIdentifier = "thread1",
                Trigger = timeTrigger,
            };

            iOSNotificationCenter.ScheduleNotification(notification);

#endif
    }

    public void SetupNotifications()
    {
#if UNITY_ANDROID
        for (int x = 1; x < 8; x++)
        {
            AndroidNotification notification = new AndroidNotification
            {
                Title = "You friends are waiting in Duels!",
                Text = "Don't miss out our new Snake Masks!",
                //  notification.RepeatInterval = TimeSpan.FromMinutes(1f);

                // notification.SmallIcon = "icon_0";
                //  notification.LargeIcon = "icon_1";
                FireTime = System.DateTime.Now.AddHours(24 * x)
            };

            AndroidNotificationCenter.SendNotification(notification, "channel_id");
        }
#endif

#if UNITY_IPHONE
        for (int x = 1; x < 8; x++)
        {
            var timeTrigger = new iOSNotificationTimeIntervalTrigger()
            {
                TimeInterval = new TimeSpan((24*x), 0, 0),
                Repeats = false
            };

            var notification = new iOSNotification()
            {
                // You can optionally specify a custom identifier which can later be
                // used to cancel the notification, if you don't set one, a unique
                // string will be generated automatically.
                Identifier = "_notification_0"+x,
                Title = "You friends are waiting in Duels!",
                Body = "Don't miss out our new Snake Masks!",
                Subtitle = "",
                ShowInForeground = true,
                ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
                CategoryIdentifier = "category_a",
                ThreadIdentifier = "thread1",
                Trigger = timeTrigger,
            };

            iOSNotificationCenter.ScheduleNotification(notification);
        }
#endif
        /*     Notifications.CancelAllPendingLocalNotifications();

             var notif = new NotificationContent();
             notif.title = "You friends are waiting in Duels!";
             notif.body = "Don't miss out our new Snake Masks!";
             // notif.categoryId = repeatCategoryId;
           //  DateTime triggerDate = DateTime.Now + new TimeSpan(0, 2, 0);
           //  Notifications.ScheduleLocalNotification(triggerDate, notif);
             Notifications.ScheduleLocalNotification(new TimeSpan(24, 0, 0), notif, NotificationRepeat.EveryDay);*/
    }

    public void OnPlayButton()
    {
        OnGameStarted();
    }

    public void PlayWithMSportsCalled()
    {
        PlayerPrefs.SetInt("MsportsModeSelected", 1);
        PlayerPrefs.Save();
    }

    public void NormalPlayButtonTrigger()
    {
        PlayerPrefs.SetInt("MsportsModeSelected", 0);
        PlayerPrefs.Save();
        OnPlayWithAI();
    }

    public void DummyMsportsButtonTrigger()
    {
        PlayerPrefs.SetInt("MsportsModeSelected", 1);
        PlayerPrefs.Save();
        //MSportsUnity.GameStarted ();
        OnPlayWithAI();
    }

    public void OnPlayWithAI()
    {
        PlayerPrefs.SetInt("points", 50);
        OnGameStarted();
    }

    public void OnPlayWithRevardedVideo()
    {
        WatchRewardedVideoAdditionSnakeLenght();
        //RewardedVideoComponent.Reward delegateInstance = //null;
        //delegateInstance =
        //new RewardedVideoComponent.Reward(x =>
        //{
        //    debug2Text.text += "RewardCallBack\n";

        //    IsRewardedVideo = true;
        //    //rewardedVideoComponent.onRewardOK -= delegateInstance;
        //    OnPlayWithAI();
        //});

        //rewardedVideoComponent.onRewardOK += delegateInstance;
        //rewardedVideoComponent.ShowReward();
    }

    public void OpenTeamModeWindow()
    {
        teamModeCanvas.SetActive(true);
    }

    public void Play2x2TeamGame()
    {
        teamModeCanvas.SetActive(false);
        OnPlayWithAITeam(TeamMode._2x2);
    }

    public void Play3x3TeamGame()
    {
        teamModeCanvas.SetActive(false);
        OnPlayWithAITeam(TeamMode._3x3);
    }

    public void OnPlayWithAITeam(TeamMode mode)
    {
        loadingGUI.SetActive(true);
        SkinManager._instance.ShuffleTeamColors();
        teamMode = mode;
        gameMode = GameMode.TeamMode;
        //  HandleStats();

        Population.instance.StopAllSnakeIncrease();
        StartCoroutine("killAllSnakes");
    }

    private void EnableHomeButton()
    {
        inGameJoystickHomeButton.SetActive(true);
    }

    public void OnGameStarted()
    {
        inGameJoystickHomeButton.SetActive(false);
        loadingOverlay.SetActive(false);
        loadingGUI.SetActive(false);
        //Debug.Log("Game Started.....");
        InGameJoystickCanvas.SetActive(true);
        Invoke("EnableHomeButton", 3f);
        mainMenuCanvas.SetActive(false);
        settingsCanvas.SetActive(false);
        snakeLooknFellBoard.SetActive(false);

        //playerSnake.SetActive(true);

        InGameCanvas.SetActive(true);
        if (gameMode == GameMode.Normal)
        {
            scoreboardCanvas.SetActive(true);
            scoreboardCanvas_Team.SetActive(false);
#if EVENTS
            Dictionary<string, object> vals = new Dictionary<string, object>
            {
                { "level", "Normal" }
            };
            AppMetrica.Instance.ReportEvent("level_start", vals);
            AppMetrica.Instance.SendEventsBuffer();
#endif
        }
        else
        {
            scoreboardCanvas.SetActive(false);
            scoreboardCanvas_Team.SetActive(true);
#if EVENTS
            Dictionary<string, object> vals = new Dictionary<string, object>
            {
                { "level", "Team" }
            };
            AppMetrica.Instance.ReportEvent("level_start", vals);
            AppMetrica.Instance.SendEventsBuffer();
#endif
        }

        AddPlayer();
        // babySnake.gameObject.SetActive(true);
        AddBabySnake();
        if (PlayerPrefs.GetInt("MsportsModeSelected") == 1)
        {
            if (isMSportsMode)
            {
            }
        }
    }

    public void EnableDuelObject()
    {
        duelModeObject.SetActive(true);
    }

    public void DuelModeClicked()
    {
        loadingOverlay.SetActive(false);
        snakeLooknFellBoard.SetActive(false);
        SetDuelMode();
    }

    public void StartDuelMode()
    {
        InGameJoystickCanvas.SetActive(true);
        scoreboardCanvas_Duel.SetActive(true);
        inGameJoystickHomeButton.SetActive(false);
        mainMenuCanvas.SetActive(false);
        settingsCanvas.SetActive(false);
    }

    public void DuelModeWon()
    {
        // Debug.Log("Player won!!");
        PlayerPrefs.SetInt("PlayerWon", 1);
        duelTimer.SetActive(false);
        gameOverDuelCanvas.SetActive(true);
        duelWonImage.SetActive(true);
        gameOverImage.SetActive(false);
        InGameJoystickCanvas.SetActive(false);
        inGameJoystickHomeButton.SetActive(true);
        DestroyAllSnakesDuel();
        continueBtnDuelMode.SetActive(false);
        EggsManager.instance.ShowEggsAndGetEggs();
    }

    public void MSportsPlayCall()
    {
        PlayerPrefs.SetInt("MsportsModeSelected", 1);
        PlayerPrefs.Save();
        OnPlayWithAI();
    }

    //chaipi function
    public void AddPlayer()
    {
        IsPlayerActive = true;
        if (GameManager.instance.IsDuelMode())
        {
            SnakeSpawner.Instance.SpawnDuelPlayer(Population.instance.duelMode_Position[0]);
        }
        else
        {
            SnakeSpawner.Instance.SpawnPlayer();
        }
        //SnakeSpawner.Instance.CreateNewSnake(200,)
        //  Population.instance.AddSnake(this.playerSnake.GetComponent<Snake>(), PlayerPrefs.GetString("PlayerName", "You"));
    }

    public void AddBabySnake()
    {
        if (PlayerPrefs.GetInt("babysnake", 0) == 1)
        {
            Invoke("addBabySnake", 1f);
        }
        else
        {
            if (UnityEngine.Random.Range(0, 100) < 33)
            {
                Invoke("addBabySnake", 1f);
            }
        }
    }

    public void addBabySnake()
    {
        SnakeSpawner.Instance.SpawnBabySnake();
        babyBox.SetActive(true);
        //   babySnake.points = 0;
        //  babySnake.gameObject.SetActive(true);
        // Population.instance.AddSnake(this.babySnake, "BabySnake");
    }

    public void onMaskClick()   //Not called
    {
        mainMenuCanvas.SetActive(false);
        skinsCanvas.SetActive(false);
        maskCanvas.SetActive(true);
    }

    public void OnSkinsClick()
    {
        mainMenuCanvas.SetActive(false);
        snakeLooknFellBoard.GetComponent<RectTransform>().localPosition = snakeLooknFeel_SkinsPos;
        snakeLooknFellBoard.GetComponent<Image>().enabled = false;  //scale: 0.7753591
        snakeLooknFellBoard.transform.localScale = snakeLooknFeel_SkinsScale;
        maskCanvas.SetActive(true);
        MaskGUIScript._instance.SetMaskTab(0);
        //  skinsCanvas.SetActive(true);
    }

    public void GoToHome()
    {
        mainMenuCanvas.SetActive(true);
        maskCanvas.SetActive(false);
        skinsCanvas.SetActive(false);
        snakeLooknFellBoard.GetComponent<RectTransform>().localPosition = snakeLooknFeel_DefaultPos;    //scale: 1.27108
        snakeLooknFellBoard.GetComponent<Image>().enabled = true;
        snakeLooknFellBoard.transform.localScale = snakeLooknFeel_DefaultScale;
    }

    public bool isReplayOn = false;

    //public void OnHomeButtonPressed()
    //{
    //    if (duelModeObject.activeInHierarchy)
    //    {
    //        DestroyAllSnakesDuel();
    //        if (SnakeManager.instance.isDuelOn)
    //            SnakeManager.instance.DuelModeToggle();

    //        duelModeObject.SetActive(false);
    //        gameOverDuelCanvas.SetActive(false);
    //        InGameCanvas.SetActive(false);
    //        mainMenuCanvas.SetActive(true);
    //        snakeLooknFellBoard.SetActive(true);
    //        ResetData();
    //        return;
    //    }
    //    else
    //    {
    //        gameOverImage.SetActive(true);
    //        duelWonImage.SetActive(false);
    //        HandleStats();
    //        DestroySnake(playerSnake.GetComponent<Snake>());
    //        InGameJoystickCanvas.SetActive(false);
    //        loadingGuiText.text = "Loading..";
    //        //ISManager.instance.ShowInterstitial();
    //        StartCoroutine("killAllSnakesGameOver");
    //    }
    //}

    public void DisableLoading()
    {
        loadingGUI.SetActive(false);
    }

    public void OnGameOverPlayButton(bool special)
    {
        if (special)
        {
            SnakeSpawner.Instance.isGameStarted = false;
        }
        // Debug.Log("Inside gameoverPlayButton");
        if (gameMode == GameMode.DuelMode)
        {
            DestroyAllSnakesDuel();
            if (SnakeManager.instance.isDuelOn)
            {
                SnakeManager.instance.DuelModeToggle();
            }

            // playerSnake.GetComponent<Snake>().points = 0;
            duelModeObject.SetActive(false);
            gameOverDuelCanvas.SetActive(false);
            InGameCanvas.SetActive(false);
            loadingGUI.SetActive(true);
#if IRONSOURCE
    //        ISManager.instance.ShowInterstitial();
#endif
#if YODO
      //      YodoManager.instance.ShowInterstitial();
#endif
            Invoke("ShowAds", 0.5f);
            Invoke("DisableLoading", 1f);
            //mainMenuCanvas.SetActive(true);
            //snakeLooknFellBoard.SetActive(true);
            ResetData();
            return;
        }
        else
        {
            gameMode = GameMode.Normal;
            gameOverImage.SetActive(true);
            duelWonImage.SetActive(false);
            HandleStats();

            InGameJoystickCanvas.SetActive(false);
            //loadingGuiText.text = "Loading..";
            StartCoroutine("killAllSnakesGameOver");
        }
    }

    public void ShowAds()
    {
#if IRONSOURCE
      ISManager.instance.ShowInterstitial();
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowInterstitial("duelgameover");
#endif
#if YODO
          YodoManager.instance.ShowInterstitial();
#endif
    }

    public void OnGameOverPlayButton()
    {
        // Debug.Log("Inside gameoverPlayButton");
        if (gameMode == GameMode.DuelMode)
        {
            DestroyAllSnakesDuel();
            if (SnakeManager.instance.isDuelOn)
            {
                SnakeManager.instance.DuelModeToggle();
            }

            // playerSnake.GetComponent<Snake>().points = 0;
            duelModeObject.SetActive(false);
            gameOverDuelCanvas.SetActive(false);
            InGameCanvas.SetActive(false);
            loadingGUI.SetActive(true);
#if IRONSOURCE
    //        ISManager.instance.ShowInterstitial();
#endif
#if YODO
      //      YodoManager.instance.ShowInterstitial();
#endif

            Invoke("ShowAds", 0.5f);
            Invoke("DisableLoading", 1f);
            //mainMenuCanvas.SetActive(true);
            //snakeLooknFellBoard.SetActive(true);
            ResetData();
            return;
        }
        else
        {
            gameMode = GameMode.Normal;
            gameOverImage.SetActive(true);
            duelWonImage.SetActive(false);
            HandleStats();

            InGameJoystickCanvas.SetActive(false);
            //loadingGuiText.text = "Loading..";
            StartCoroutine("killAllSnakesGameOver");
        }
    }

    public void ResumeDuel()
    {
        gameOverDuelCanvas.SetActive(false);
        // PlayerPrefs.SetInt("PlayerWon", 0);
        duelReadyCount.SetActive(true);
        //  duelTimer.SetActive(false);
        duelTimer.GetComponent<TimerCountdown>().isExtendTimer = true;
        //  duelWonImage.SetActive(false);
        gameOverImage.SetActive(false);
        InGameJoystickCanvas.SetActive(true);
        inGameJoystickHomeButton.SetActive(false);
        Invoke("ExtendDuelMode", 2.5f);
    }

    public void GameOverDuel()
    {
        SnakeSpawner.Instance.isGameStarted = false;
        //HandleStats();
        gameOverDuelCanvas.SetActive(true);
        PlayerPrefs.SetInt("PlayerWon", 0);
        duelReadyCount.SetActive(false);
        duelTimer.SetActive(false);
        duelWonImage.SetActive(false);
        gameOverImage.SetActive(true);
        InGameJoystickCanvas.SetActive(false);
        inGameJoystickHomeButton.SetActive(true);

#if IRONSOURCE

        if (ISManager.instance.isVideoAvailable())    //All plugins disabled because of game crashing situation
        {
            continueBtnDuelMode.SetActive(true);
        }
        else
        {
            continueBtnDuelMode.SetActive(false);
        }

#endif
#if APPLOVIN

        if (AppsLovinManager.instance.isVideoAvailable())    //All plugins disabled because of game crashing situation
        {
            continueBtnDuelMode.SetActive(true);
        }
        else
        {
            continueBtnDuelMode.SetActive(false);
        }

#endif

#if YODO

        if (YodoManager.instance.IsRewardAdReady())    //All plugins disabled because of game crashing situation
        {
            continueBtnDuelMode.SetActive(true);
        }
        else
        {
            continueBtnDuelMode.SetActive(false);
        }

#endif
#if EVENTS
        Dictionary<string, object> vals = new Dictionary<string, object>
        {
            { "level", "Duel" },
            { "result", "Lose" },
            { "time", (int)duelModeTimer.timerCount },
            { "skin_name", "" },
            { "progress", 0 }
        };
        AppMetrica.Instance.ReportEvent("level_finish", vals);
        AppMetrica.Instance.SendEventsBuffer();
#endif
        //DestroyAllSnakesDuel();
        EggsManager.instance.HideEggs();
        int duelModePoints = 0;
        if (SnakeSpawner.Instance.playerSnake != null)
        {
            if (SnakeSpawner.Instance.playerSnake.isPaused)
            {
                return;
            }
            //isHomeButtonPressed = true;
            duelModePoints = SnakeSpawner.Instance.playerSnake.points;
            //Debug.Log("game manger - on game over duel");
            SnakeSpawner.Instance.DestroySnake(SnakeSpawner.Instance.playerSnake);
        }
        Debug.Log("game over snake");
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(duelModePoints, duelleaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");
                }
                else
                {
                    Debug.Log("Update Score Fail");
                }
            });
        }
        else
        {
            /* Social.localUser.Authenticate((bool success) =>
             {
                 if (success)
                 {
                     Debug.Log("Login Sucess");
                 }
                 else
                 {
                     Debug.Log("Login failed");
                 }
             });*/
        }
    }

    public void DestroyAllSnakesDuel()
    {
        /*  Population.instance.StopAllRoutines();
          for (int i = 0; i < SnakeManager.instance.usedSnakes.Count; i++)
          {
              Destroy(SnakeManager.instance.usedSnakes[i]);
          }*/
        SnakeSpawner.Instance.DestroyAllSnakes();
        // SnakeManager.instance.usedSnakes.Clear();
    }

    public void DestroyAllExceptWinnerDuel()
    {
        SnakeSpawner.Instance.DestroyAllSnakes();
        /*   Population.instance.StopAllRoutines();
           for (int i = 0; i < SnakeManager.instance.usedSnakes.Count; i++)
           {
               if (SnakeManager.instance.usedSnakes[i].GetComponent<Snake>().snakeName.Equals(Population.instance.topperName))
               {
                   continue;
               }
               Destroy(SnakeManager.instance.usedSnakes[i]);
           }*/
    }

    public void ReplayGame()
    {
        isReplayOn = false;
        PlayerPrefs.SetInt("AutomaticPlay", 0);
        PlayerPrefs.Save();
        // Debug.Log("OnGameover button");
        ResetData();
        OnPlayWithAI();
    }

    public void ResetData()
    {
        //playerSnake.SetActive(false);
        Debug.Log("Player set to inactive - ResetData");
        scoreboardCanvas.SetActive(false);
        scoreboardCanvas_Team.SetActive(false);
        InGameCanvas.SetActive(false);
        InGameJoystickCanvas.SetActive(false);
        gameOverCanvas.SetActive(false);
        mainMenuCanvas.SetActive(true);
        snakeLooknFellBoard.SetActive(true);

        if (IsDuelMode())
        {
            FoodSpawner.Instance.SwitchToDuelModeAndReset();
            return;
        }
        else
        {
            //Debug.Log("ResetData");
            Population.instance.Initialize();
            FoodSpawner.Instance.SwitchToNormalModeAndReset();
            //     FoodManager.instance.foodQuantity = 600;        //Food quantity set manually
            //     FoodManager.instance.Initialize();
        }
    }

    public void Restart_Duel()
    {
        if (duelModeObject.activeInHierarchy)
        {
            duelModeObject.SetActive(false);
        }

        EnableDuelObject();
        // Debug.Log("Restart Duel");
    }

    public void ReplayAds()
    {
#if IRONSOURCE
        ISManager.instance.ShowInterstitial();
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowInterstitial("replay");
#endif
#if YODO
            YodoManager.instance.ShowInterstitial();
#endif
    }

    public void OnReplayButtonPressed()
    {
        #region OLD_CODE

        isReplayOn = true;
        //if (AdMobHandler.m_instance.IsInterstitialReady())
        //{
        //    AdMobHandler.m_instance.ShowInterstitial();
        //}
        //else
        //{
        //    //if (!MSportsManager._instance.IsMsportsAvailable && !isMSportsMode) {
        //    AdMobHandler.m_instance.load_interstitial();
        //    //}
        //    PlayerPrefs.SetInt("points", 250);
        //    ResetData();
        //    if (isPlayingMultiplayer)
        //        OnPlayButton();
        //    else
        //        OnPlayWithAI();
        //}

        #endregion OLD_CODE

        HandleStats();
        loadingGUI.SetActive(true);
        Population.instance.StopAllSnakeIncrease();
        // Invoke("ReplayAds", 0.1f);
#if IRONSOURCE
        ISManager.instance.ShowInterstitial();
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowInterstitial("replay");
#endif
#if YODO
            YodoManager.instance.ShowInterstitial();
#endif
        if (duelModeObject.activeInHierarchy)
        {
            DestroyAllSnakesDuel();
            if (SnakeManager.instance.isDuelOn)
            {
                SnakeManager.instance.DuelModeToggle();
            }
            //  playerSnake.GetComponent<Snake>().points = 0;
            duelModeObject.SetActive(false);
            gameOverDuelCanvas.SetActive(false);
            InGameCanvas.SetActive(false);
            loadingGUI.SetActive(true);

            Invoke("DisableLoading", 0.5f);
            DuelModeRestart();
            return;
        }

        //if (isDuelModeOn)
        //{
        //    gameOverDuelCanvas.SetActive(false);
        //    Debug.Log("Before loading active - duels");
        //    loadingGUI.SetActive(true);
        //    DestroyAllSnakesDuel();
        //    ISManager.instance.ShowInterstitial();
        //    Debug.Log("Interstitial shown in duel replay");
        //    DuelModeRestart();
        //    return;
        //}

        gameOverCanvas.SetActive(false);
        gameOverDuelCanvas.SetActive(false);
        isReplayOn = true;
        StartCoroutine("killAllSnakes");
    }

    private void DuelModeRestart()
    {
        if (duelTimer.activeInHierarchy)
        {
            duelTimer.SetActive(false);
        }

        //if (loadingGUI.activeInHierarchy)
        //  loadingGUI.SetActive(false);
        duelWonImage.SetActive(false);
        gameOverImage.SetActive(true);
        ResetData();
        Restart_Duel();
    }

    private void HandleStats()
    {
        if (isPlayerActive)
        {
            IncreaseSurvivingTime();
            StopTopOneTracking();
        }

        _statsManager.SaveScore(SnakeSpawner.Instance.lastPoints);
        _statsManager.SaveKills(PlayerKills);
        _statsManager.SaveSurvivalTime(SurvivalTimeTicks);
        _statsManager.SaveTopOneTime(TopOneTimeTicks);

        lastSnakePoints = 50;
        PlayerKills = 0;
        SurvivalTimeTicks = 0;
        TopOneTimeTicks = 0;
    }

    public void StartTopOneTracking()
    {
        _isTopOneTrackingEnabled = true;
        TopOneStartTime = DateTime.Now;
    }

    public void StopTopOneTracking()
    {
        if (_isTopOneTrackingEnabled)
        {
            _isTopOneTrackingEnabled = false;
            TopOneTimeTicks += DateTime.Now.Ticks - TopOneStartTime.Ticks;
        }
    }

    private void IncreaseSurvivingTime()
    {
        SurvivalTimeTicks += DateTime.Now.Ticks - SnakeSpawner.Instance.SpawnTime.Ticks;
    }

    public void KillAllSnakes()
    {
        StartCoroutine("killAllSnakes");
    }

    private IEnumerator killAllSnakes()
    {
        InGameJoystickCanvas.SetActive(false);
        loadingGUI.SetActive(true);
        Population.instance.StopAllRoutines();
        yield return null;

        SnakeSpawner.Instance.DestroyAllSnakes();
        /*  foreach (ECSSnake snake in SnakeSpawner.Instance.snakes)
          {
              SnakeSpawner.Instance.DestroySnake(snake);
          }
          while (SnakeSpawner.Instance.snakes.Count > 0)
          {
              yield return null;
          }*/
        yield return new WaitForSeconds(1f);
        ResetData();
        //OnPlayWithAI();
        OnGameStarted();

        loadingGUI.SetActive(false);
        InGameJoystickCanvas.SetActive(true);
    }

    private IEnumerator killAllSnakesGameOver()
    {
        loadingGUI.SetActive(true);
        gameOverCanvas.SetActive(false);
        yield return new WaitForSeconds(0.5f);
#if IRONSOURCE
      ISManager.instance.ShowInterstitial();
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowInterstitial("gameover");
#endif
#if YODO
          YodoManager.instance.ShowInterstitial();
#endif
        gameOverCanvas.SetActive(false);
        yield return new WaitForSeconds(2f);

        gameOverCanvas.SetActive(false);
        loadingGUI.SetActive(false);
        Population.instance.StopAllRoutines();
        yield return null;
        SnakeSpawner.Instance.DestroyAllSnakes();

        // Invoke("DisableLoading", 0.5f);
        //Debug.Log("ResetData called");
        ResetData();
    }

    private IEnumerator AddPlayerAfterDelay()
    {
        yield return new WaitForSeconds(0.2f);
        OnPlayWithAI();
    }

    public void WatchRewardedVideo()
    {
        //Debug.Log("Rewarded video opened!");
#if IRONSOURCE
        ISManager.instance.ShowRewardVideo("");
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowRewardVideo(AppsLovinManager.RewardType.revive, "revive");
#endif
#if YODO
            YodoManager.instance.ShowRewardAd("");
#endif
    }

    public void WatchRewardedVideoEgg()
    {
        //Debug.Log("Rewarded video opened!");
#if IRONSOURCE
        ISManager.instance.ShowRewardVideo("egg");
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowRewardVideo(AppsLovinManager.RewardType.egg, "free eggs");
#endif
#if YODO
        YodoManager.instance.ShowRewardAd("egg");
#endif
    }

    public void WatchRewardedVideoDuelMode()
    {
#if IRONSOURCE
        ISManager.instance.ShowRewardVideo("duelmode");
#endif
#if APPLOVIN
        AppsLovinManager.instance.ShowRewardVideo(AppsLovinManager.RewardType.duelmode, "revive duel");
#endif
#if YODO
        YodoManager.instance.ShowRewardAd("duelmode");
#endif
    }

    public void WatchRewardedVideoAdditionSnakeLenght()
    {
#if APPLOVIN
        AppsLovinManager.instance.ShowRewardVideo(AppsLovinManager.RewardType.additionSnakeLenght, "revive");
#endif
    }

    public void OnSuccessfulPurchaseRemoveAD()
    {
        PlayerPrefs.SetInt("ads", 0);
        AdRemoveBtn.SetActive(false);
        Debug.Log("Ad button successfully removed");
    }

    public void OnSuccessfulPurchaseBabySnake()
    {
        PlayerPrefs.SetInt("babysnake", 1);
        babySnakeBtn.SetActive(false);
        Debug.Log("Baby Snake button successfully removed");
    }

    public void OnSuccessfulPurchaseTransperancy()
    {
        PlayerPrefs.SetInt("transparency", 0);
        MaskGUIScript._instance.EnableSlider();
        //  ColorSelectionManager._instance.EnableSlider();
        AchievementManager._instance.BuyTransparency();      //Commented out recently
        Debug.Log("transparency purchased");
    }

    public void OnSuccessfulPurchaseBundle()
    {
        Debug.Log("bundle purchased");
        PlayerPrefs.SetInt("ads", 0);
        AdRemoveBtn.SetActive(false);
        PlayerPrefs.SetInt("babysnake", 1);
        babySnakeBtn.SetActive(false);
        PlayerPrefs.SetInt("transparency", 0);
        maskGUI.EnableSlider();
        //  ColorSelectionManager._instance.EnableSlider();
        AchievementManager._instance.BuyTransparency();      //Commented out recently
        EggsManager.instance.IncreaseEgg(15);

        PlayerPrefs.SetInt("bundle", 1);
        bundleBtn.gameObject.SetActive(false);

        AchievementManager._instance.achivementUnlockText.text = "You purchased Bundle!";
        GameManager.instance.dialogBox.SetActive(true);
    }

    public void OnSuccessfulPurchaseEggPack(int eggs)
    {
        Debug.Log("eggs purchased");
        EggsManager.instance.IncreaseEgg(eggs);
        AchievementManager._instance.achivementUnlockText.text = "You purchased " + eggs + " Eggs!";
        GameManager.instance.dialogBox.SetActive(true);
    }

    public void SpeedUp()
    {
        if (!speedCheck)
        {
            Speed.SetActive(true);
            speedCheck = true;
        }
    }

    public void SpeedDown()
    {
        if (speedCheck)
        {
            Speed.SetActive(false);
            speedCheck = false;
        }
    }

    public void OnReviewYes()
    {
        DB1.SetActive(false);
        DB2.SetActive(true);
    }

    public void OnReviewNo()
    {
        DB1.SetActive(false);
        DB3.SetActive(true);
    }

    public void OnGiveReview()
    {
        DB2.SetActive(false);
        RateManager.instance.rateGame();
    }

    public void OnReviewSend()
    {
        DB3.SetActive(false);
        DB4.SetActive(true);
        Invoke("HideDB", 3f);
    }

    private void HideDB()
    {
        DB4.SetActive(false);
    }
}