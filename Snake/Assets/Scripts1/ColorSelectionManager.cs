﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorSelectionManager : MonoBehaviour {

	// Use this for initialization
	public static ColorSelectionManager _instance;

	public GameObject mainMenuCanvas;
	public Slider transparencySlider;		// Transparency slider
	public Text transparencyValueHolder;	// Transparency holder text

	public bool canUseColorOfFirstRing;		// Check to see if color from the first ring can be used
	public bool canUseColorOfSecondRing;	// Check to see if color from the Second ring can be used
	public bool canUseColorOfThirdRing;		// Check to see if color from the Third ring can be used
	public bool canUseTransparency;			// Check to see if can use Transparency

	public Image firstAssignedColor;
	public Image secondAssignedColor;
	public Image thirdAssignedColor;

	public RingColorSelector firstRing;
	public RingColorSelector secondRing;
	public RingColorSelector thirdRing;

	public Image selectedColorRing;
    public GameObject ring1;
    public GameObject ring2;
    public GameObject ring3;

	public Sprite toggleOffButtonSprite;
	public Sprite toggleOnButtonSprite;

	public Image secondRingToggleButton;
	public Image thirdRingToggleButton;

	public GameObject sliderBuyButton;

	public Text messageToUser;



	void Awake()
	{
		_instance = this;
        //PlayerPrefs.SetInt("SecondColorAvailability", 1);
        //PlayerPrefs.SetInt("ThirdColorAvailability", 1);
       // PlayerPrefs.SetInt("transparency", 0);
        PlayerPrefs.Save();
    }

	void OnEnable()
	{

		messageToUser.text = "Select Color Ring from Right Box";;

		if (PlayerPrefs.HasKey ("SecondColorAvailability")) {
			
		} else {
			PlayerPrefs.SetInt ("SecondColorAvailability", 0);
			PlayerPrefs.Save();
		}

		if (PlayerPrefs.HasKey ("ThirdColorAvailability")) {

		} else {
			PlayerPrefs.SetInt ("ThirdColorAvailability", 0);
			PlayerPrefs.Save();
		}
        
		if (GameManager.instance.isMSportsMode)
        {
            Debug.Log("slider buy btn disabled!");
			sliderBuyButton.SetActive (false);
			transparencySlider.enabled = true;
			if (PlayerPrefs.HasKey ("PlayerTransparency"))
            {
				transparencySlider.value = PlayerPrefs.GetFloat ("PlayerTransparency");
			}
		}
        else
        {
			if (PlayerPrefs.HasKey ("transparency"))
            {
				if (PlayerPrefs.GetInt ("transparency") == 0)
                {
					sliderBuyButton.SetActive (false);
					transparencySlider.enabled = true;
					if (PlayerPrefs.HasKey ("PlayerTransparency"))
                    {
						transparencySlider.value = PlayerPrefs.GetFloat ("PlayerTransparency");
					}
				}
                else
                {
					sliderBuyButton.SetActive (true);
					transparencySlider.enabled = false;
				}
			}
            else
            {
				PlayerPrefs.SetInt ("transparency", 1);
				PlayerPrefs.Save ();
				sliderBuyButton.SetActive (true);
				transparencySlider.enabled = false;
			}
		}
		SetSelectorColor (LooknFeelDisplay._instance.color1, LooknFeelDisplay._instance.color2, LooknFeelDisplay._instance.color3);
		SetColorRingAvailability ();
		ResetHighlighter (0);
	}


	void OnDisable()
	{
		PlayerPrefs.SetFloat ("PlayerTransparency", transparencySlider.value);
		PlayerPrefs.Save ();
	}

	void Update()
	{
		if (selectedColorRing == null) {
			messageToUser.text = "Select Color Ring from Right Box";
		} else {
			messageToUser.text = "Tap/Drag to select color";
		}
	}

	public void OnUseTransparency()
	{
        Debug.Log("On use transparency called");
#if UIP
#if UNITY_ANDROID
        UnityIAP.instance.BuyItem("com.camc.slitherio.snakes.worms.usetransparency"); 
#endif
#if UNITY_IPHONE
        UnityIAP.instance.BuyItem("com.camc.usetransparency");
#endif
#endif
        GameManager.instance.ifAdRemoveClicked = false;
	}

	void Start () {
		//Adds a listener to the transparency slider and invokes a method when the value changes.
		transparencySlider.onValueChanged.AddListener(delegate {SliderValueChangeCheck(); });
		transparencyValueHolder.text = Mathf.Round(transparencySlider.value*100) + "%";
		LooknFeelDisplay._instance.SetTransparency (1-transparencySlider.value);
	}

	public void EnableSlider()
	{
		sliderBuyButton.SetActive (false);
		transparencySlider.enabled = true;
	}

	// Invoked when the value of the slider changes.
	public void SliderValueChangeCheck()
	{
		transparencyValueHolder.text = Mathf.Round(transparencySlider.value*100) + "%";
		LooknFeelDisplay._instance.SetTransparency (1 - transparencySlider.value);
	}

	public void onBackClick()
	{
		mainMenuCanvas.SetActive(true);
		this.gameObject.SetActive (false);
	}

	public void SetSelectorColor(Color selector1Color, Color selector2Color, Color selector3Color)
	{
		firstAssignedColor.color = selector1Color;
		secondAssignedColor.color = selector2Color;
		thirdAssignedColor.color = selector3Color;
	}

	public void SetSelectedColorRing(int colorRingNumber)
	{
		if (colorRingNumber == 1)
        {
			selectedColorRing = firstAssignedColor;
            ring1.SetActive(true);
            ring2.SetActive(false);
            ring3.SetActive(false);
		}
        else if (colorRingNumber == 2)
        {
			selectedColorRing = secondAssignedColor;
            ring1.SetActive(false);
            ring2.SetActive(true);
            ring3.SetActive(false);
        }
        else if (colorRingNumber == 3)
        {
			selectedColorRing = thirdAssignedColor;
            ring1.SetActive(false);
            ring2.SetActive(false);
            ring3.SetActive(true);
        }

		//ResetHighlighter (colorRingNumber);

	}

	public void SetColorToRing(Color selectedColor)
	{
		if (selectedColorRing == null)
			return;

		selectedColorRing.color = selectedColor;

		if (selectedColorRing.GetComponent<RingColorSelector> ().ringNumber == 1) {
			string colorCode = selectedColor.r + "," + selectedColor.g + "," + selectedColor.b + "," + selectedColor.a;
			PlayerPrefs.SetString ("First_ColorValue", colorCode);
			PlayerPrefs.Save ();
		} else if (selectedColorRing.GetComponent<RingColorSelector> ().ringNumber == 2) {
			string colorCode = selectedColor.r + "," + selectedColor.g + "," + selectedColor.b + "," + selectedColor.a;
			PlayerPrefs.SetString ("Second_ColorValue", colorCode);
			PlayerPrefs.Save ();
		} else if (selectedColorRing.GetComponent<RingColorSelector> ().ringNumber == 3) {
			string colorCode = selectedColor.r + "," + selectedColor.g + "," + selectedColor.b + "," + selectedColor.a;
			PlayerPrefs.SetString ("Third_ColorValue", colorCode);
			PlayerPrefs.Save ();
		}

		SetColorRingAvailability ();
	}

	public void ResetHighlighter(int highlightedRingNumber)
	{
		firstRing.DeactivateHighlighter ();
		secondRing.DeactivateHighlighter ();
		thirdRing.DeactivateHighlighter ();

		if (highlightedRingNumber != 0) {
			if (highlightedRingNumber == 1)
				firstRing.ActivateHighlighter ();
			else if (highlightedRingNumber == 2)
				secondRing.ActivateHighlighter ();
			else if (highlightedRingNumber == 3)
				thirdRing.ActivateHighlighter ();
		}
	}

	public void SecondColorRingToggleButtonClicked()
	{
		if (canUseColorOfSecondRing == false) {
			secondRingToggleButton.sprite = toggleOnButtonSprite;
			PlayerPrefs.SetInt ("SecondColorAvailability", 1);
			PlayerPrefs.Save ();
		} else {
			if (canUseColorOfThirdRing == true) {
				ThirdColorRingToggleButtonClicked ();
			}
			
			secondRingToggleButton.sprite = toggleOffButtonSprite;
			PlayerPrefs.SetInt ("SecondColorAvailability", 0);
			PlayerPrefs.Save ();
		}

		SetColorRingAvailability ();
	}

	public void ThirdColorRingToggleButtonClicked()
	{
		if (canUseColorOfSecondRing) {
			if (canUseColorOfThirdRing == false) {
				thirdRingToggleButton.sprite = toggleOnButtonSprite;
				PlayerPrefs.SetInt ("ThirdColorAvailability", 1);
				PlayerPrefs.Save ();
			} else {
				thirdRingToggleButton.sprite = toggleOffButtonSprite;
				PlayerPrefs.SetInt ("ThirdColorAvailability", 0);
				PlayerPrefs.Save ();
			}

		}
		SetColorRingAvailability ();

	}



	public void SetColorRingAvailability()
	{
		if (PlayerPrefs.GetInt ("SecondColorAvailability") == 0) {
			canUseColorOfSecondRing = false;
			secondRing.DeactivateHighlighter ();
			if (selectedColorRing == secondAssignedColor)
				selectedColorRing = null;
			
			secondRingToggleButton.sprite = toggleOffButtonSprite;
			PlayerPrefs.SetInt ("SecondColorAvailability", 0);
			PlayerPrefs.SetInt ("ThirdColorAvailability", 0);
			PlayerPrefs.Save();
		} else {
			canUseColorOfSecondRing = true;
			secondRingToggleButton.sprite = toggleOnButtonSprite;
			PlayerPrefs.SetInt ("SecondColorAvailability", 1);
			PlayerPrefs.Save();
		}

		if (PlayerPrefs.GetInt ("ThirdColorAvailability") == 0) {
			canUseColorOfThirdRing = false;
			thirdRing.DeactivateHighlighter ();
			if (selectedColorRing == thirdAssignedColor)
				selectedColorRing = null;

			thirdRingToggleButton.sprite = toggleOffButtonSprite;
			PlayerPrefs.SetInt ("ThirdColorAvailability", 0);
			PlayerPrefs.Save();

		} else {
			canUseColorOfThirdRing = true;
			thirdRingToggleButton.sprite = toggleOnButtonSprite;
			PlayerPrefs.SetInt ("ThirdColorAvailability", 1);
			PlayerPrefs.Save();
		}

	/*	if (canUseColorOfThirdRing) {
			LooknFeelDisplay._instance.SetSnakeColorInDisplay (firstAssignedColor.color, secondAssignedColor.color, thirdAssignedColor.color);
		} else if (canUseColorOfSecondRing) {
			LooknFeelDisplay._instance.SetSnakeColorInDisplay (firstAssignedColor.color, secondAssignedColor.color, firstAssignedColor.color);
		} else {
			LooknFeelDisplay._instance.SetSnakeColorInDisplay (firstAssignedColor.color, firstAssignedColor.color, firstAssignedColor.color);
		}*/

		LooknFeelDisplay._instance.SetTransparency (1 - PlayerPrefs.GetFloat ("PlayerTransparency"));
	}
}
