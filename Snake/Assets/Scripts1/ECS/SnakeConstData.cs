﻿using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct SnakeConstData : IComponentData
{
    public float pieceForPoints;// = 19f; // Each 50p gets 1 piece //18
    public float referenceScale;// = 1f;
    public int pointsForScale;// = 500; // Each 250p the scale increases 
    public float scaleOffset;// = 0.04f;
}
