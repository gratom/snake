﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using UnityEngine;


[UpdateAfter(typeof(AIDetectSystem))]
[UpdateAfter(typeof(SnakeHeadMoveSystem))]
public class SnakeECSToGameObjectSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        Entities
            .WithoutBurst()
            .WithStructuralChanges()
            .ForEach((Entity entity,ref SnakeHeadData headData,in SnakeHeadTargetData targetData) =>
            {
                if (headData.shouldDestroy&&headData.isDead)
                {
                    Debug.Log("Snake head with id : " + headData.snakeId + " Triggered!");
                       EntityManager.DestroyEntity(targetData.ai);
                       EntityManager.RemoveComponent<SnakeHeadTargetData>(entity);
                       EntityManager.RemoveComponent<PhysicsCollider>(entity);
                       EntityManager.RemoveComponent<PhysicsVelocity>(entity);
                       EntityManager.RemoveComponent<PhysicsMass>(entity);
                  //     EntityManager.RemoveComponent<SnakePartBuffer>(entity);
                    //  DynamicBuffer<SnakePartBuffer> buffer = EntityManager.GetBuffer<SnakePartBuffer>(entity);
                    //  buffer.
                    /*      AIData ai = EntityManager.GetComponentData<AIData>(targetData.ai);
                             ai.isDead = true;
                             EntityManager.SetComponentData<AIData>(targetData.ai,ai);*/

                    headData.shouldDestroy = false;
                    //headData.isDead = true;
                    SnakeSpawner.Instance.snakes[headData.snakeId].isDestroyed = true;
                 //   ECSSnake snake = SnakeSpawner.Instance.snakes[headData.snakeId];
                 //   SnakeSpawner.Instance.DestroySnake(snake);
                }

                
            }).Run();

        Entities
           .WithoutBurst()
           .WithStructuralChanges()
           .ForEach((Entity entity, ref SnakeHeadData headData,in PlayerData player) =>
           {
               if (headData.shouldDestroy && headData.isDead)
               {
                   if (!GameManager.instance.IsDuelMode()||SnakeSpawner.Instance.snakes[headData.snakeId].isDuelModeDestroyed)
                   {
                       EntityManager.RemoveComponent<PhysicsCollider>(entity);
                       EntityManager.RemoveComponent<PhysicsVelocity>(entity);
                       EntityManager.RemoveComponent<PhysicsMass>(entity);
                     //  EntityManager.RemoveComponent<SnakePartBuffer>(entity);

                       headData.shouldDestroy = false;

                       SnakeSpawner.Instance.snakes[headData.snakeId].isDestroyed = true;
                   }
                   else
                   {
                       headData.shouldDestroy = false;
                       SnakeSpawner.Instance.snakes[headData.snakeId].sprinting = false;
                      SnakeSpawner.Instance.snakes[headData.snakeId].isDuelModeDestroyed = true;
                   }
                   
               }


           }).Run();

        return inputDeps;
    }
}
