﻿using Unity.Entities;
[GenerateAuthoringComponent]
public struct PieceNodeData : IComponentData
{
    public Entity entityToFollow;
}
