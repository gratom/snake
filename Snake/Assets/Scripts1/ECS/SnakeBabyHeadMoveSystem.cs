﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Analytics;

[UpdateAfter(typeof(SnakeResizeSystem))]
public class SnakeBabyHeadMoveSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = 0.038f;// Time.DeltaTime;
     //   float rotationSpeed = 3f;
     //   float speed = 20f;

        Entities
            .WithoutBurst()
        .ForEach((ref Translation position,ref Rotation rotation,ref SnakeHeadData snakeHeadData, ref DynamicBuffer<SnakePartBuffer> snakeParts, ref SnakeHeadTargetData targetData,ref SnakePointsData pointsData,in BabySnakeData babySnake) =>{

            if (!snakeHeadData.isDead)
            {
                ECSSnake playerSnake = SnakeSpawner.Instance.playerSnake;
                if (playerSnake != null)
                {
                    if (playerSnake.isDestroyed)
                    {
                        snakeHeadData.shouldDestroy = true;
                        snakeHeadData.isDead = true;
                    }
                    else
                    {
                        Translation playerSnakePos = EntityManager.GetComponentData<Translation>(playerSnake.snakeHead);

                        float3 heading = targetData.foodTarget - position.Value;
                        heading.y = 0;
                        if (targetData.isAIMoveDirection)
                        {


                            heading = targetData.moveDirection;

                        }

                        quaternion targetDirection = quaternion.LookRotation(heading, math.up());


                        if (!targetData.isReachedPosition)
                        {
                            quaternion curRot = math.slerp(rotation.Value, targetDirection, deltaTime * snakeHeadData.snakeRotationSpeed);
                            if (float.IsNaN(curRot.value.x))
                                Debug.LogError("IsNaN Found!");
                            else
                                rotation.Value = curRot;

                            //math.slerp(rotation.Value, targetDirection, deltaTime * snakeHeadData.snakeRotationSpeed);
                        }



                        position.Value += deltaTime * snakeHeadData.speed * snakeHeadData.speedMultiplier * math.forward(rotation.Value);

                        SnakePartBuffer buffer = snakeParts[0];
                        buffer.savedPosition = position.Value;
                        snakeParts[0] = buffer;


                        if (!targetData.isAIMoveDirection)
                        {
                            if (math.distance(position.Value, targetData.foodTarget) < 3)
                            {
                                targetData.isReachedPosition = true;

                            }
                        }


                        if (math.distance(position.Value, float3.zero) > 340f)
                        {
                            targetData.foodTarget = float3.zero;
                            targetData.isReachedPosition = false;
                            targetData.isAIMoveDirection = false;
                        }
                        else
                        {
                            if (math.distance(position.Value, playerSnakePos.Value) > 50f)
                            {
                                targetData.foodTarget = playerSnakePos.Value;
                                targetData.isReachedPosition = false;
                                targetData.isAIMoveDirection = false;
                            }
                        }

                     //   Debug.Log("baby snake moving around");
                        int pointsDiff = pointsData.points-150;
                        if (pointsDiff > 0)
                        {
                         //   Debug.Log("baby snake points managed");
                            SnakePointsData npointsData = EntityManager.GetComponentData<SnakePointsData>(playerSnake.snakeHead);
                            npointsData.points += pointsDiff;
                            EntityManager.SetComponentData<SnakePointsData>(playerSnake.snakeHead, npointsData);
                            pointsData.points = 150;
                        }
                    }
                }
                else
                {
                    snakeHeadData.shouldDestroy = true;
                   
                    snakeHeadData.isDead = true;
                }

            }

            
           

        }).Run();
        return inputDeps;
    }
}
