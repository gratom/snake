﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct SnakeGroupData : ISharedComponentData
{
    public int group;
}
