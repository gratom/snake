﻿
using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct SnakeBallColorData : IComponentData
{
    public Entity snakeBallEntity;
}
