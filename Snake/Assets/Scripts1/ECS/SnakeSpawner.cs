﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class SnakeSpawner : MonoBehaviour
{
    public static SnakeSpawner Instance;
    public GameObject snakePiecePrefab;
    public GameObject snakeHeadPrefab;
    public GameObject playerSnakeHeadPrefab;
    private Entity snakePieceEntity;
    private Entity snakeHeadEntity;
    private Entity playerSnakeHeadEntity;
    private World world;
    private EntityManager manager;
    public float maxSnakeScale = 10f;
    public ECSSnake[] snakes;
    public ECSSnake playerSnake;
    public UnityEngine.Material sprintMat;
    public UnityEngine.Material solidColor;
    public UnityEngine.Material transparentColor;
    public UnityEngine.Material maskMat;
    public UnityEngine.Material noCrown;
    public UnityEngine.Material crown;

    public Mesh quad;
    public Mesh square;
    public GameObject playerTracker;
    public CameraManager camerManager;
    public Transform[] playerSpawnPoints;
    public SpawnPoint[] aiSpawnPoints;

    //  public Sprite normalMask;
    //  public Sprite testMask;
    public Shader solidShader;

    public Shader transparentShader;
    public bool isGameStarted = false;
    private BlobAssetStore blobAssetStore;

    //  public int numOfSnake = 0;

    private List<UnityEngine.Material> stockMaterials;
    public List<ColorTemplate> stockColorTemplates;

    public DateTime SpawnTime { get; set; }

    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        world = World.DefaultGameObjectInjectionWorld;
        manager = world.EntityManager;
        blobAssetStore = new BlobAssetStore();
        GameObjectConversionSettings settings = GameObjectConversionSettings.FromWorld(world, blobAssetStore);// (new BlobAssetStore()));
        snakePieceEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(snakePiecePrefab, settings);
        snakeHeadEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(snakeHeadPrefab, settings);
        playerSnakeHeadEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(playerSnakeHeadPrefab, settings);

        stockMaterials = new List<UnityEngine.Material>();
        snakes = new ECSSnake[40];
        SetupNewColorTemplatesAndMaterialsForBots();
        //TEST CODE
        /*  for(int i=0;i<50;i++)
          {
              float x = Mathf.Sin(i) * UnityEngine.Random.Range(0, 340);
              float z = Mathf.Cos(i) * UnityEngine.Random.Range(0, 340);

              ColorTemplate colortemp = new ColorTemplate();
              colortemp.colors = new Color[2];
              colortemp.colors[0] = Color.red;
              colortemp.colors[1] = Color.yellow;

              ECSSnake newSnake = new ECSSnake(numOfSnake ,"noname", UnityEngine.Random.Range(1000, 1000), new Vector3(x, 0, z),colortemp);
              numOfSnake++;
              snakes.Add(newSnake);
         //     SpawnSnake(i,UnityEngine.Random.Range(200,200),new Vector3(x,0,z));
          }*/
    }

    public int GetEmptySnakeArrayPos()
    {
        for (int x = 0; x < snakes.Length; x++)
        {
            if (snakes[x] == null)
            {
                return x;
            }
        }

        return -1;
    }

    public int GetSnakesRealCount()
    {
        int count = 0;
        for (int x = 0; x < snakes.Length; x++)
        {
            if (snakes[x] != null)
            {
                if (!snakes[x].isDestroyed)
                {
                    count++;
                }
            }
        }

        return count;
    }

    public int lastPoints = 0;
    public float timeBetweenDestruction = 3f;
    public int numberOfSnakesToKillAtOnce = 6;
    private float timeBeforeSnakeDies = 3f;
    public bool autoDestroy = false;

    public void FixedUpdate()
    {
        /*  int matCount = 0;
          matCount += stockMaterials.Count;
          for (int x = 0; x < snakes.Count; x++)
          {
              matCount += 2;
          }
          Debug.Log("Total materials used : " + matCount);*/
        //   Debug.Log("Snakes count : " + GetSnakesRealCount().ToString());

        /* for (int x = 0; x < snakes.Length; x++)
         {
             if (snakes[x] != null)
             {
                 if (snakes[x].isDestroyed)
                     Debug.Log("destroyed snakes id : " + snakes[x].snakeId.ToString());
                 else
                     Debug.Log("snakes id : " + snakes[x].snakeId.ToString());
            }
         }*/
        if (isGameStarted)
        {
            if (playerSnake != null)
            {
                if (playerSnake.isDestroyed)
                {
                    Debug.Log("normal game over");
                    GameManager.instance.gameOverCanvas.SetActive(true);
                    GameManager.instance.OnGameOver_Event();

                    //  isGameStarted = false;
                    lastPoints = playerSnake.points;
                }

                if (playerSnake.isDuelModeDestroyed)
                {
                    SnakeSpawner.Instance.PauseAllSnakes();// PauseDuelModeSnakes();
                                                           // GameManager.instance.DestroyAllExceptWinnerDuel();
                    GameManager.instance.GameOverDuel();
                    //   isGameStarted = false;
                    lastPoints = playerSnake.points;
                }
            }

            //   }
            /*  if (playerSnake.isDestroyed)
              {
              }*/
        }
        /*  if (autoDestroy)
          {
              timeBeforeSnakeDies -= Time.fixedDeltaTime;
              if (timeBeforeSnakeDies < 0)
              {
                  if (GetSnakesRealCount() > (numberOfSnakesToKillAtOnce+1))
                  {
                      timeBeforeSnakeDies = timeBetweenDestruction;

                      ECSSnake[] chosenSnake = GetRandomSnake();
                      for(int x=0;x<chosenSnake.Length;x++)
                          DestroySnake(chosenSnake[x]);
                  }
              }
          }*/
    }

    public void PauseSnake(ECSSnake snake)
    {
        snake.isPaused = true;
        SnakeHeadData headData = manager.GetComponentData<SnakeHeadData>(snake.snakeHead);
        headData.isDead = true;
        manager.SetComponentData<SnakeHeadData>(snake.snakeHead, headData);
    }

    public void UnPauseSnake(ECSSnake snake, float time = 1f)
    {
        snake.isPaused = false;
        SnakeHeadData headData = manager.GetComponentData<SnakeHeadData>(snake.snakeHead);
        headData.isDead = false;
        headData.isImmune = true;
        manager.SetComponentData<SnakeHeadData>(snake.snakeHead, headData);
        DisableImmune(snake, time);
    }

    public void PauseAllSnakes()
    {
        for (int x = 0; x < snakes.Length; x++)
        {
            if (snakes[x] != null)
            {
                PauseSnake(snakes[x]);
            }
        }
    }

    public void UnPauseAllSnakes(float time = 1f)
    {
        for (int x = 0; x < snakes.Length; x++)
        {
            if (snakes[x] != null)
            {
                UnPauseSnake(snakes[x], time);
            }
        }
    }

    public Vector3 GetRandomSpawnPoint(ECSSnake snake)
    {
        List<SpawnPoint> spawnPointAvailable = new List<SpawnPoint>();
        for (int x = 0; x < aiSpawnPoints.Length; x++)
        {
            if (aiSpawnPoints[x].canSpawn == 0f)
            {
                if (SnakeSpawner.Instance.playerSnake != null)
                {
                    if (Vector3.Distance(aiSpawnPoints[x].transform.position, playerTracker.transform.position) > 200f)
                    {
                        spawnPointAvailable.Add(aiSpawnPoints[x]);
                    }
                }
                else
                {
                    spawnPointAvailable.Add(aiSpawnPoints[x]);
                }
            }
        }
        if (spawnPointAvailable.Count == 0)
        {
            return Vector3.zero;
        }

        int xrnd = UnityEngine.Random.Range(0, spawnPointAvailable.Count);

        Debug.Log("spawn point available : " + spawnPointAvailable.Count);
        spawnPointAvailable[xrnd].CannotSpawn(snake);
        return spawnPointAvailable[xrnd].transform.position;
    }

    public ECSSnake[] GetRandomSnake()
    {
        List<ECSSnake> availableSnakes = new List<ECSSnake>();
        ECSSnake[] snakesToKill = new ECSSnake[numberOfSnakesToKillAtOnce];
        for (int x = 0; x < snakes.Length; x++)
        {
            if (snakes[x] != null)
            {
                if (!snakes[x].isDestroyed)
                {
                    availableSnakes.Add(snakes[x]);
                }
            }
        }
        for (int x = 0; x < snakesToKill.Length; x++)
        {
            int rnd = UnityEngine.Random.Range(0, availableSnakes.Count);
            snakesToKill[x] = availableSnakes[rnd];
            availableSnakes.RemoveAt(rnd);
        }
        return snakesToKill;
    }

    public void RemoveSnake(ECSSnake snake)
    {
        if (!snake.defaultMask)
        {
            DestroyImmediate(snake.maskMat, true);
        }

        DestroyImmediate(snake.sprintMat, true);
        snakes[snake.snakeId] = null;
        // snakes.Remove(snake);
    }

    public void SetupNewColorTemplatesAndMaterialsForBots()
    {
        if (stockMaterials != null)
        {
            if (stockMaterials.Count > 0)
            {
                foreach (UnityEngine.Material mat in stockMaterials)
                {
                    DestroyImmediate(mat, true);
                }
            }
        }
        stockMaterials = new List<UnityEngine.Material>();
        stockColorTemplates = new List<ColorTemplate>();
        List<ColorTemplate> newTemp = new List<ColorTemplate>(SkinManager._instance.availableColorTemplate);
        newTemp.RemoveAt(1);

        for (int x = 0; x < newTemp.Count; x++)
        {
            if (newTemp[x].colors.Length > 3)
            {
                newTemp.RemoveAt(x);
                x--;
            }
        }

        for (int x = 0; x < 7; x++)
        {
            int rndIndex = UnityEngine.Random.Range(0, newTemp.Count);
            stockColorTemplates.Add(newTemp[rndIndex]);

            newTemp.RemoveAt(rndIndex);
        }

        for (int x = 0; x < stockColorTemplates.Count; x++)
        {
            int isTrans = UnityEngine.Random.Range(0, 7);

            for (int y = 0; y < stockColorTemplates[x].colors.Length; y++)
            {
                if (isTrans == 0)
                {
                    Color tempColor = stockColorTemplates[x].colors[y];
                    stockColorTemplates[x].colors[y] = new Color(tempColor.r, tempColor.g, tempColor.b, 0.3f);
                }
                stockMaterials.Add(GetMaterial(stockColorTemplates[x].colors[y]));
            }
        }
    }

    public UnityEngine.Material GetMaterial(Color color)
    {
        foreach (UnityEngine.Material mat in stockMaterials)
        {
            if (mat.color == color)
            {
                return mat;
            }
        }

        if (color.a == 1f)
        {
            UnityEngine.Material newMat = new UnityEngine.Material(solidColor)
            {
                color = color
            };
            stockMaterials.Add(newMat);
            return newMat;
        }
        else
        {
            UnityEngine.Material newMat = new UnityEngine.Material(transparentColor)
            {
                color = color
            };
            stockMaterials.Add(newMat);
            return newMat;
        }
    }

    private void OnDestroy()
    {
        // Dispose of the BlobAssetStore, else we're get a message:
        // A Native Collection has not been disposed, resulting in a memory leak.
        if (blobAssetStore != null)
        { blobAssetStore.Dispose(); }
    }

    public ECSSnake GetPlayerSnake()
    {
        return playerSnake;
    }

    public bool UseLastSnakePoints = false;

    public void ToggleCrown(ECSSnake snake, bool toggle)
    {
        SnakeCrownData crownData = manager.GetComponentData<SnakeCrownData>(snake.snakeHead);
        RenderMesh render = manager.GetSharedComponentData<RenderMesh>(crownData.crownEntity);
        if (toggle)
        {
            render.material = crown;
        }
        else
        {
            render.material = noCrown;
        }

        manager.SetSharedComponentData<RenderMesh>(crownData.crownEntity, render);
    }

    public void SpawnBabySnake()
    {
        if (playerSnake == null)
        {
            return;
        }

        Transform playerChosenPoint = GameManager.instance.chosenPlayerSpawnPoint;
        // int randomSpawnPoint = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
        string team = "";

        ColorTemplate colortemp = playerSnake.colorTemplate;

        Sprite mask = null;
        int playerMask = PlayerPrefs.GetInt("PlayerMask", -1);
        int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);
        if (playerMask == -1)
        {
            playerMaskTab = -1;
        }
        else
        {
            mask = MaskManager.instance.GetMask(playerMask, playerMaskTab);
        }

        int points = 150;

        CreateNewSnake(points, "BabySnake", playerChosenPoint.GetChild(0).position, colortemp, mask, false, team, true);
    }

    public void SpawnPlayer()
    {
        //  Debug.Log("spawning player");

        isGameStarted = true;
        int randomSpawnPoint = UnityEngine.Random.Range(0, playerSpawnPoints.Length);
        string team = "";
        GameManager.instance.chosenPlayerSpawnPoint = playerSpawnPoints[randomSpawnPoint];
        ColorTemplate colortemp = LooknFeelDisplay._instance.selectedColorTemplate;

        if (GameManager.instance.IsTeamMode())
        {
            colortemp = SkinManager._instance.GetTeamBasedColorTemplate("A");
            team = "A";
        }
        float transparencyVal = 1 - PlayerPrefs.GetFloat("PlayerTransparency");
        if (transparencyVal < 1f)
        {
            for (int x = 0; x < colortemp.colors.Length; x++)
            {
                Color tep = colortemp.colors[x];
                colortemp.colors[x] = new Color(tep.r, tep.g, tep.b, transparencyVal);
            }
        }

        Sprite mask = null;
        int playerMask = PlayerPrefs.GetInt("PlayerMask", -1);
        int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);
        if (playerMask == -1)
        {
            playerMaskTab = -1;
        }
        else
        {
            mask = MaskManager.instance.GetMask(playerMask, playerMaskTab);
        }

        int points = 200;
        if (GameManager.instance.IsRewardedVideo) // bonus for revarded video
        {
            points = 400;
            GameManager.instance.IsRewardedVideo = false;
        }

        if (UseLastSnakePoints)
        {
            UseLastSnakePoints = false;
            points = lastPoints;
        }

        CreateNewSnake(points, PlayerPrefs.GetString("PlayerName", "You"), playerSpawnPoints[randomSpawnPoint].position, colortemp, mask, true, team);
        lastPoints = 200;
        SpawnTime = DateTime.Now;
    }

    public void SpawnDuelPlayer(Vector3 pos)
    {
        Debug.Log("spawning duel player");
        isGameStarted = true;
        int randomSpawnPoint = UnityEngine.Random.Range(0, playerSpawnPoints.Length);

        // GameManager.instance.chosenPlayerSpawnPoint = playerSpawnPoints[randomSpawnPoint];
        ColorTemplate colortemp = LooknFeelDisplay._instance.selectedColorTemplate;
        float transparencyVal = 1 - PlayerPrefs.GetFloat("PlayerTransparency");
        if (transparencyVal < 1f)
        {
            for (int x = 0; x < colortemp.colors.Length; x++)
            {
                Color tep = colortemp.colors[x];
                colortemp.colors[x] = new Color(tep.r, tep.g, tep.b, transparencyVal);
            }
        }

        Sprite mask = null;
        int playerMask = PlayerPrefs.GetInt("PlayerMask", -1);
        int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);
        if (playerMask == -1)
        {
            playerMaskTab = -1;
        }
        else
        {
            mask = MaskManager.instance.GetMask(playerMask, playerMaskTab);
        }

        CreateNewSnake(150, PlayerPrefs.GetString("PlayerName", "You"), pos, colortemp, mask, true);
    }

    public void CreateNewSnake(int snakeSize, string name, Vector3 position, ColorTemplate colortemp, Sprite maskSelected = null, bool isPlayer = false, string team = "", bool isBabySnake = false)
    {
        int snakeNum = GetEmptySnakeArrayPos();
        ECSSnake newSnake = new ECSSnake(snakeNum, name, snakeSize, position, colortemp, maskSelected, isPlayer, team, isBabySnake);
        //  numOfSnake++;
        Debug.Log("Snake num index chosen : " + snakeNum);
        snakes[snakeNum] = newSnake;

        if (isPlayer)
        {
            playerSnake = newSnake;
            camerManager.playerSnake = playerSnake;
        }
    }

    public Entity[] SpawnSnake(ECSSnake snake, int snakeIndex, int snakeSize, Vector3 spawnPos, bool isPlayer = false, string team = "", bool isBaby = false)
    {
        Entity[] snakeFirstAndLast = new Entity[2];
        float x = Mathf.Sin(snakeIndex) * UnityEngine.Random.Range(0, 340);
        float z = Mathf.Cos(snakeIndex) * UnityEngine.Random.Range(0, 340);
        int numberOfPieces = snakeSize;
        // Debug.Log("player spawned " + isPlayer);
        Entity snakeHead = Entity.Null;// manager.Instantiate(snakeHeadEntity);
        if (!isPlayer)
        {
            snakeHead = manager.Instantiate(snakeHeadEntity);
        }
        else
        {
            snakeHead = manager.Instantiate(playerSnakeHeadEntity);
        }

        //     manager.RemoveComponent<PhysicsVelocity>(snakeHead);
        int teamid = 0;
        switch (team)
        {
            case "A":
                teamid = 0;
                break;

            case "B":
                teamid = 1;
                break;

            case "C":
                teamid = 2;
                break;

            default:
                teamid = -1;
                break;
        }
        Debug.Log("Team id : " + teamid);

        if (isBaby)
        {
            manager.AddComponentData<BabySnakeData>(snakeHead, new BabySnakeData());
            if (playerSnake != null)
            {
                if (playerSnake.teamId == -1)
                {
                    teamid = playerSnake.snakeId;
                }
                else
                {
                    teamid = playerSnake.teamId;
                }
                //  else
                //   teamid = playerSnake.teamId;
            }
        }

        manager.SetComponentData(snakeHead, new LocalToWorld
        {
            Value = new float4x4((new quaternion(0, 0, 0, 1)), spawnPos)
        });

        manager.SetComponentData(snakeHead, new Translation
        {
            Value = spawnPos
        });
        manager.SetComponentData(snakeHead, new Rotation
        {
            Value = new quaternion(0, 0, 0, 1)
        });
        manager.SetComponentData(snakeHead, new SnakeHeadData
        {
            snakeId = snakeIndex,
            speed = 25,
            snakeRotationSpeed = 8,
            speedMultiplier = 1,
            headDiff = isPlayer ? 0.35f : 0.25f,
            shouldDestroy = false,
            isDead = false,
            isImmune = true,
            teamId = (teamid == -1) ? snakeIndex : teamid,
            isDuelMode = GameManager.instance.IsDuelMode(),
            isBabySnake = isBaby
        });

        manager.AddComponentData(snakeHead, new NonUniformScale
        {
            Value = new float3(1, 1, 1)
        });
        manager.AddSharedComponentData(snakeHead, new SnakeGroupData
        {
            group = snakeIndex
        });
        int ssnakeParts = snake.GetSnakeParts();
        manager.SetComponentData(snakeHead, new SnakeHeadPartsData
        {
            snakeParts = ssnakeParts,
            snakeNewParts = ssnakeParts
        });
        manager.SetComponentData(snakeHead, new SnakePointsData
        {
            points = snake.points
        });
        manager.SetComponentData(snakeHead, new PieceScaleData
        {
            pieceIndex = 0,
            scaleData = 1f
        });
        if (!isPlayer)
        {
            SnakeHeadTargetData snakeHeadTargetData = manager.GetComponentData<SnakeHeadTargetData>(snakeHead);
            snakeHeadTargetData.foodTarget = float3.zero;
            snakeHeadTargetData.moveDirection = float3.zero;
            snakeHeadTargetData.isReachedPosition = false;
            snakeHeadTargetData.isAIMoveDirection = false;

            Entity aiEntity = snakeHeadTargetData.ai;
            AIData aiData = manager.GetComponentData<AIData>(aiEntity);
            aiData.snakeId = snakeIndex;
            aiData.counter = 0;

            aiData.teamId = (teamid == -1) ? snakeIndex : teamid;
            aiData.isDuelMode = GameManager.instance.IsDuelMode();
            aiData.isBabySnake = isBaby;
            manager.SetComponentData<AIData>(aiEntity, aiData);
            manager.SetComponentData<SnakeHeadTargetData>(snakeHead, snakeHeadTargetData);
        }

        SnakeGlowData glowData = manager.GetComponentData<SnakeGlowData>(snakeHead);
        manager.SetSharedComponentData<RenderMesh>(glowData.glowEntity, new RenderMesh
        {
            mesh = square,
            material = snake.sprintMat
        });

        SnakeBallColorData ballData = manager.GetComponentData<SnakeBallColorData>(snakeHead);
        manager.SetSharedComponentData<RenderMesh>(ballData.snakeBallEntity, new RenderMesh
        {
            mesh = quad,
            material = snake.GetNextColor()
        });
        manager.SetComponentData<Translation>(ballData.snakeBallEntity, new Translation
        {
            Value = new float3(0, 0.1f, 0)
        });

        SnakeMaskData maskData = manager.GetComponentData<SnakeMaskData>(snakeHead);
        manager.SetSharedComponentData<RenderMesh>(maskData.entityData, new RenderMesh
        {
            mesh = square,
            material = snake.maskMat
        });

        DynamicBuffer<SnakePartBuffer> snakePartBufferList = manager.AddBuffer<SnakePartBuffer>(snakeHead);
        snakePartBufferList.Add(new SnakePartBuffer { savedPosition = spawnPos });

        for (int u = 0; u < numberOfPieces; u++)
        {
            snakePartBufferList.Add(new SnakePartBuffer { savedPosition = spawnPos });
        }

        //    NativeArray<Entity> pieces = new NativeArray<Entity>(numberOfPieces,Allocator.TempJob);
        //  manager.Instantiate(snakePieceEntity,pieces);
        snakeFirstAndLast[0] = snakeHead;
        Entity lastEntity = snakeHead;
        for (int i = 0; i < numberOfPieces; i++)
        {
            // if (snake.isDestroyed)
            //    break;

            Entity pieceEntity = manager.Instantiate(snakePieceEntity);
            manager.SetComponentData(pieceEntity, new Translation
            {
                Value = spawnPos
            });
            manager.AddComponentData(pieceEntity, new NonUniformScale
            {
                Value = new float3(1, 1, 1)
            });

            manager.SetComponentData(pieceEntity, new PieceData
            {
                snakeId = snakeIndex,
                pieceIndex = i + 1,
                positionToMove = new float3(0, 0, 0),
                teamId = (teamid == -1) ? snakeIndex : teamid
            });
            manager.SetComponentData(pieceEntity, new PieceScaleData
            {
                pieceIndex = i + 1,
                scaleData = 1f
            });
            manager.SetComponentData(pieceEntity, new PieceNodeData
            {
                entityToFollow = lastEntity
            });

            manager.AddSharedComponentData(pieceEntity, new SnakeGroupData
            {
                group = snakeIndex
            });

            glowData = manager.GetComponentData<SnakeGlowData>(pieceEntity);
            manager.SetSharedComponentData<RenderMesh>(glowData.glowEntity, new RenderMesh
            {
                mesh = square,
                material = snake.sprintMat
            });

            ballData = manager.GetComponentData<SnakeBallColorData>(pieceEntity);
            manager.SetSharedComponentData<RenderMesh>(ballData.snakeBallEntity, new RenderMesh
            {
                mesh = quad,
                material = snake.GetNextColor()
            });
            manager.SetComponentData<Translation>(ballData.snakeBallEntity, new Translation
            {
                Value = new float3(0, ((i + 1) * -0.001f), 0)
            });
            lastEntity = pieceEntity;
        }
        manager.SetComponentData(snakeHead, new SnakeLastPartData
        {
            lastPiece = lastEntity
        });

        //  pieces.Dispose();

        snakeFirstAndLast[1] = lastEntity;

        // manager.RemoveComponent<SnakePartBuffer>(snakeHead);

        return snakeFirstAndLast;
    }

    public void DisableImmune(ECSSnake snake, float timer = 2f)
    {
        StartCoroutine(disableImmune(snake, timer));
    }

    private IEnumerator disableImmune(ECSSnake snake, float timer = 2f)
    {
        Color normalColor = Color.white;
        Color alphaColor = new Color(1, 1, 1, 0);
        snake.sprintMat.color = normalColor;
        yield return new WaitForSeconds(timer);
        snake.sprintMat.color = alphaColor;
        SnakeHeadData headData = manager.GetComponentData<SnakeHeadData>(snake.snakeHead);
        headData.isImmune = false;
        manager.SetComponentData<SnakeHeadData>(snake.snakeHead, headData);
    }

    private void Start()
    {
    }

    /*  public ECSSnake GetSnakeFromId(int id)
      {
          for (int x = 0; x < snakes.Count; x++)
          {
              if (snakes[x] != null)
              {
                  if (snakes[x].snakeId == id)
                  {
                      return snakes[x];
                  }
              }
          }

          return null;
      }*/

    public void UpdateSnakeHead(ECSSnake snake)
    {
        SnakeHeadData headData = manager.GetComponentData<SnakeHeadData>(snake.snakeHead);
        headData.speedMultiplier = snake.speedMultiplier;
        headData.speed = snake.speed;
        headData.speedMultiplier = snake.speedMultiplier;
        headData.headDiff = snake.MOVlerpTime;
        manager.SetComponentData<SnakeHeadData>(snake.snakeHead, headData);
        /* manager.SetComponentData(snake.snakeHead, new SnakeHeadData {
                 snakeId = snake.snakeId,
                 speed = snake.speed,
                 snakeRotationSpeed = snake.rotatingSpeed,
                 speedMultiplier = snake.speedMultiplier,
                 headDiff = snake.MOVlerpTime
         });*/
    }

    public void UpdateSnakeHeadPoints(ECSSnake snake)
    {
        manager.SetComponentData(snake.snakeHead, new SnakePointsData
        {
            points = snake.points
        });
    }

    public void DestroyAllSnakes()
    {
        StopAllCoroutines();
        foreach (ECSSnake snake in snakes)
        {
            if (snake != null)
            {
                snake.dontSpawnFood = true;

                DestroySnake(snake);
            }
        }
        SetupNewColorTemplatesAndMaterialsForBots();
    }

    public Vector3 GetSnakeHeadPosition(ECSSnake snake)
    {
        Translation snakeHeadPos = manager.GetComponentData<Translation>(snake.snakeHead);
        Vector3 getHeadPos = new Vector3(snakeHeadPos.Value.x, 0, snakeHeadPos.Value.z);

        return getHeadPos;
    }

    public void DestroySnake(ECSSnake snake)
    {
        SnakeHeadData headData = manager.GetComponentData<SnakeHeadData>(snake.snakeHead);

        headData.shouldDestroy = true;
        headData.isDead = true;

        manager.SetComponentData<SnakeHeadData>(snake.snakeHead, headData);

        //  StartCoroutine(destroySnake(snake));
    }

    public IEnumerator destroySnake(ECSSnake snake)
    {
        for (int x = 0; x < snake.colorTempMats.Length; x++)
        {
            Color color = snake.colorTempMats[x].color;
            snake.colorTempMats[x] = new UnityEngine.Material(transparentColor)
            {
                color = color
            };
        }
        float lerp = 0;
        while (lerp < 1f)
        {
            lerp += Time.fixedDeltaTime;
            for (int x = 0; x < snake.colorTempMats.Length; x++)
            {
                Color normalColor = snake.colorTempMats[x].color;
                Color alphaColor = new Color(normalColor.r, normalColor.g, normalColor.b, 0.3f);
                snake.colorTempMats[x].color = Color.Lerp(normalColor, alphaColor, lerp);
            }
            if (lerp > 0.3f)
            {
                if (!snake.isDestroyed)
                {
                    snake.isDestroyed = true;
                }
            }
            yield return new WaitForFixedUpdate();
        }
        yield return null;
    }

    // Update is called once per frame
    private void Update()
    {
        /* if (Input.GetMouseButtonUp(0))
                 {
              float x = Mathf.Sin(numOfSnake) * UnityEngine.Random.Range(0, 340);
                float z = Mathf.Cos(numOfSnake) * UnityEngine.Random.Range(0, 340);

                ColorTemplate colortemp = new ColorTemplate();
                colortemp.colors = new Color[2];
                colortemp.colors[0] = Color.blue;
                colortemp.colors[1] = Color.green;

                CreateNewSnake(1000,"",new Vector3(x,0,z),colortemp);
             }*/

        /*   if (Input.GetMouseButtonUp(0))
           {
               for (int x = 0; x < snakes.Count; x++)
                   snakes[x].IncreasePoints(100);//.newSnakePieces+=10;
           }

           if (Input.GetMouseButtonUp(1))
           {
               for (int x = 0; x < snakes.Count; x++)
                   snakes[x].DecreasePoints(100);
           }

       if (Input.GetMouseButtonDown(0))
       {
           for (int x = 0; x < snakes.Count; x++)
               StartSprint(snakes[x]);
       }

       if (Input.GetMouseButtonUp(0))
       {
           for (int x = 0; x < snakes.Count; x++)
               snakes[x].sprinting = false;
       }*/
    }

    public void StartSprint(ECSSnake snake)
    {
        if (!snake.sprinting)
        {
            StartCoroutine(snake.sprint());
        }
    }
}