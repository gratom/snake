﻿using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct SnakeMaskData : IComponentData
{
    public Entity entityData;
}
