﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LooknFeelDisplay : MonoBehaviour {

	// Use this for initialization
	public static LooknFeelDisplay _instance;
	public GameObject smiley;
	public Image smiley_image;
	public Image maskRenderer;
	public Image hatRenderer;
	public Sprite defaultSkin;
	public Image firstColorRenderer;
	public Image secondColorRenderer;
	public Image thirdColorRenderer;
	public Image fourthColorRenderer;
	public Image fifthColorRenderer;
	public Image sixthColorRenderer;
	public Color defaultSkinColor;
	public Color color1;
	public Color color2;
	public Color color3;
	public GameObject eyes;
	public ColorTemplate selectedColorTemplate;

	void Awake()
	{
		_instance = this;

	}

	void OnEnable()
	{
        DeactivateAllAnimationComponent ();
        if(PlayerPrefs.GetInt("PlayerWon",0) == 1)
        {
            hatRenderer.gameObject.SetActive(true);
        }
        else
        {
            hatRenderer.gameObject.SetActive(false);
        }
		StartCoroutine (StartAnimation ());
	}

	void OnDisable()
	{
		DeactivateAllAnimationComponent ();
	}

	void Start ()
    {
        GameManager.instance.GoToHome();

        if (PlayerPrefs.HasKey("skin_index"))
        {
            SetSnakeColorInDisplay(SkinManager._instance.availableColorTemplate[PlayerPrefs.GetInt("skin_index", 0)]);
        }
        else
        {
            ColorTemplate defaultcolor = new ColorTemplate();
            defaultcolor.colors = new Color[1];

            defaultcolor.colors[0] = defaultSkinColor;
            SetSnakeColorInDisplay(defaultcolor);
        }


      /*  if (PlayerPrefs.HasKey ("First_ColorValue")) {
			ExtractColor (PlayerPrefs.GetString ("First_ColorValue"),1);
		} else {
			color1 = defaultSkinColor;
			color2 = defaultSkinColor;
			color3 = defaultSkinColor;

			string firstColorCode = defaultSkinColor.r + "," + defaultSkinColor.g + "," + defaultSkinColor.b + "," + defaultSkinColor.a;
			PlayerPrefs.SetString ("First_ColorValue", firstColorCode);
			PlayerPrefs.SetString (GameUtils.PREFS_INITIAL_SNAKE_COLOUR, firstColorCode+firstColorCode+firstColorCode);
			PlayerPrefs.Save ();
		}

		if (PlayerPrefs.HasKey ("Second_ColorValue")) {
			ExtractColor (PlayerPrefs.GetString ("Second_ColorValue"),2);
		}

		if (PlayerPrefs.HasKey ("Third_ColorValue")) {
			ExtractColor (PlayerPrefs.GetString ("Third_ColorValue"),3);
		}

		if (!PlayerPrefs.HasKey ("PlayerTransparency")) {
			PlayerPrefs.SetFloat ("PlayerTransparency",0);
		}*/

		//SetSnakeColorInDisplay (color1, color2, color3);
		SetTransparency (1 - PlayerPrefs.GetFloat ("PlayerTransparency"));

		if (PlayerPrefs.HasKey ("PlayerMask")) 
		{
			int playerMaskNumber = PlayerPrefs.GetInt ("PlayerMask");
            int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);

			Debug.Log ("Got mask number ========================== #############: " + playerMaskNumber);
			if (playerMaskNumber != -1&& playerMaskTab!=-1) 
			{
				if (playerMaskNumber >= MaskManager.instance.masks.Length)
				{
					playerMaskNumber = 0;
					PlayerPrefs.SetInt("PlayerMask", 0);
				}
				SetSnakeMaskOnDisplay (MaskManager.instance.GetMask(playerMaskNumber,playerMaskTab),false,false);
				//eyes.SetActive (false);
			} 
			else 
			{
				eyes.SetActive (true);
				LooknFeelDisplay._instance.ChangeMaskColorAsFirstColor ();
			}
		} 
		else 
		{
			PlayerPrefs.SetInt ("PlayerMask", -1);
			PlayerPrefs.Save ();
			LooknFeelDisplay._instance.ChangeMaskColorAsFirstColor ();
		}
		//PlayerPrefs.DeleteKey ("First_ColorValue");
	}

	public void DeactivateAllAnimationComponent()
	{
		RectTransform resetPosition = new RectTransform ();
		maskRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//maskRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
		firstColorRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//firstColorRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
		secondColorRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//secondColorRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
		thirdColorRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//thirdColorRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
		fourthColorRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//fourthColorRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
		fifthColorRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//fifthColorRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
		sixthColorRenderer.gameObject.GetComponent<Floater> ().enabled = false;
		//sixthColorRenderer.gameObject.GetComponent<RectTransform> ().anchoredPosition.y = -4.5f;
	}

	IEnumerator StartAnimation()
	{
		maskRenderer.gameObject.GetComponent<Floater> ().enabled = true;
		yield return new WaitForSeconds (0.2f);
		firstColorRenderer.gameObject.GetComponent<Floater> ().enabled = true;
		yield return new WaitForSeconds (0.2f);
		secondColorRenderer.gameObject.GetComponent<Floater> ().enabled = true;
		yield return new WaitForSeconds (0.2f);
		thirdColorRenderer.gameObject.GetComponent<Floater> ().enabled = true;
		yield return new WaitForSeconds (0.2f);
		fourthColorRenderer.gameObject.GetComponent<Floater> ().enabled = true;
		yield return new WaitForSeconds (0.2f);
		fifthColorRenderer.gameObject.GetComponent<Floater> ().enabled = true;
		yield return new WaitForSeconds (0.2f);
		sixthColorRenderer.gameObject.GetComponent<Floater> ().enabled = true;
	}

	public void ExtractColor(string colorCode, int colorNumber)
	{
		Debug.Log ("################: " + colorCode);
		string[] RGBColorValues = colorCode.Split(',');

		Color newcolor = new Color ();
		newcolor.r = float.Parse(RGBColorValues [0]);
		newcolor.g = float.Parse(RGBColorValues [1]);
		newcolor.b = float.Parse(RGBColorValues [2]);
		newcolor.a = float.Parse(RGBColorValues [3]);


		if (colorNumber == 1) {
			color1 = newcolor;
			color2 = newcolor;
			color3 = newcolor;
		} else if (colorNumber == 2) {
			if (PlayerPrefs.GetInt ("SecondColorAvailability") == 1) {
				color2 = newcolor;
				color3 = color1;
			}
		} else if (colorNumber == 3) {
			
			if (PlayerPrefs.GetInt ("ThirdColorAvailability") == 1) {
				color3 = newcolor;
			}
		}

		//SetSnakeColorInDisplay (color1, color2, color3);

	}

	public void SetSnakeDisplay(Sprite maskImage, Color firstColor, Color secondColor, Color thirdColor)
	{
		int playerMaskNumber = PlayerPrefs.GetInt ("PlayerMask");
		Debug.Log ("Called in SetSnake Display " + playerMaskNumber);
        if (!MaskManager.instance.IsHat(playerMaskNumber + 1))
        {
            maskRenderer.sprite = maskImage;		//Disabled due to mask copyright
			//maskRenderer.sprite = defaultSkin;
			//hatRenderer.gameObject.SetActive(false);

		}
        else
        {
            hatRenderer.gameObject.SetActive(true);
            ShowHat(maskImage);
        }

        smiley.SetActive (false);

		if (MaskManager.instance.IsSmiley (playerMaskNumber)) {
			smiley.SetActive (true);
			smiley.GetComponent<Image> ().color = firstColor;
			smiley_image.sprite = maskImage;
		}

		firstColorRenderer.color = firstColor;
		secondColorRenderer.color = secondColor;
		thirdColorRenderer.color = thirdColor;
		fourthColorRenderer.color = firstColor;
		fifthColorRenderer.color = secondColor;
		sixthColorRenderer.color = thirdColor;
	}

    public void SetSnakeDisplay(Sprite maskImage,ColorTemplate colors)
    {
        int playerMaskNumber = PlayerPrefs.GetInt("PlayerMask");
        Debug.Log("Called in SetSnake Display " + playerMaskNumber);
        if (!MaskManager.instance.IsHat(playerMaskNumber + 1))
        {
            maskRenderer.sprite = maskImage;        //Disabled due to mask copyright
                                                    //maskRenderer.sprite = defaultSkin;
                                                    //hatRenderer.gameObject.SetActive(false);

        }
        else
        {
            hatRenderer.gameObject.SetActive(true);
            ShowHat(maskImage);
        }

        smiley.SetActive(false);

        if (MaskManager.instance.IsSmiley(playerMaskNumber))
        {
            smiley.SetActive(true);
            smiley.GetComponent<Image>().color = colors.colors[0];
            smiley_image.sprite = maskImage;
        }
        int counter = 0;
        for (int x = 0; x < colors.colors.Length; x++)
        {
            switch (x)
            {
                case 0: firstColorRenderer.color = colors.colors[counter]; break;
                case 1: secondColorRenderer.color = colors.colors[counter]; break;
                case 2: thirdColorRenderer.color = colors.colors[counter]; break;
                case 3: fourthColorRenderer.color = colors.colors[counter]; break;
                case 4: fifthColorRenderer.color = colors.colors[counter]; break;
                case 5: sixthColorRenderer.color = colors.colors[counter]; break;
            }

            counter++;
            if (counter > colors.colors.Length)
                counter = 0;
        }
    }

    public void SetSnakeMaskOnDisplay(Sprite maskImage,bool isAuto,bool noMask)
	{
		int playerMaskNumber = PlayerPrefs.GetInt ("PlayerMask");
		if (playerMaskNumber != GameManager.instance.playerMaskNo) {
			PlayerPrefs.SetInt (GameUtils.PREFS_MASK_CHANGED, 1);
			PlayerPrefs.Save ();
            AchievementManager._instance.CheckIfColorAndMaskChanged ();   //Commented out recently
        }
       // int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);
        eyes.SetActive (false);
        Debug.Log("Called in SetSnakeMaskOnDisplay " + playerMaskNumber);

        int tabNum = MaskManager.instance.tabIndex;
        if (!isAuto)
            tabNum = PlayerPrefs.GetInt("TabIndex", -1);
       

        if (tabNum == 6)
        {
            Debug.Log("Coming to change hat " + playerMaskNumber);
            //hatRenderer.gameObject.SetActive(true);
            ShowHat(maskImage);
           
        }else
        {
            // Color whiteColor = Color.white;
            maskRenderer.sprite = maskImage;        //Disabled due to mask copyright
                                                    //maskRenderer.sprite = defaultSkin;		
            maskRenderer.color = Color.white;        //Code to edit for crown
            //hatRenderer.gameObject.SetActive(false);
            Debug.Log(" not Coming to change hat " + playerMaskNumber);
        }

       

        if (tabNum == 2)
        {
			smiley.SetActive (true);
			smiley.GetComponent<Image> ().color = selectedColorTemplate.colors[0];
            maskRenderer.color = selectedColorTemplate.colors[0];
            smiley_image.sprite = maskImage;		//Disabled due to mask copyright
			//smiley_image.sprite = defaultSkin;
		}
        else 
            smiley.SetActive (false);

        if (noMask)
            smiley.SetActive(false);

	}

	public void ChangeMaskColorAsFirstColor()
	{
		maskRenderer.color = selectedColorTemplate.colors[0];
	}

	public void ShowHat(Sprite hatImage)
	{
		Color whiteColor = Color.white;
		maskRenderer.sprite = defaultSkin;
		maskRenderer.color = whiteColor;
		maskRenderer.color = color1;

		eyes.SetActive (true);
		hatRenderer.sprite = hatImage;

	}

	public void SetSnakeColorInDisplay(ColorTemplate colors)
	{
        
        /*	color1 = firstColor;
            color2 = secondColor;
            color3 = thirdColor;*/
        selectedColorTemplate = colors;
        int counter = 0;
        for (int x = 0; x < 6; x++)
        {
            switch (x)
            {
                case 0: firstColorRenderer.color = colors.colors[counter]; break;
                case 1: secondColorRenderer.color = colors.colors[counter]; break;
                case 2: thirdColorRenderer.color = colors.colors[counter]; break;
                case 3: fourthColorRenderer.color = colors.colors[counter]; break;
                case 4: fifthColorRenderer.color = colors.colors[counter]; break;
                case 5: sixthColorRenderer.color = colors.colors[counter]; break;
            }

            counter++;
            if (counter == colors.colors.Length)
                counter = 0;
        }
        smiley.GetComponent<Image> ().color = colors.colors[0];

		

		if(PlayerPrefs.GetInt ("PlayerMask") == -1)// || hatRenderer.gameObject.activeInHierarchy)
			maskRenderer.color = colors.colors[0];


       /* string new_snake_color = color1.r + "," + color1.g + "," + color1.b + "," + color1.a + color2.r + "," + color2.g + "," + color2.b + "," + color2.a + color3.r + "," + color3.g + "," + color3.b + "," + color3.a;
		PlayerPrefs.SetString (GameUtils.PREFS_CURRENT_SNAKE_COLOUR, new_snake_color);
		PlayerPrefs.Save ();*/
		//ColorSelectionManager._instance.SetSelectorColor (color1, color2, color3);
	}

	public void SetTransparency(float transparencyValue)
	{
		Color newColor = new Color ();

		//newColor = maskRenderer.color;
		//newColor.a = transparencyValue;
		//maskRenderer.color = newColor;

		newColor = firstColorRenderer.color;
		newColor.a = transparencyValue;
		firstColorRenderer.color = newColor;
		//smiley.GetComponent<Image> ().color = newColor;

		newColor = secondColorRenderer.color;
		newColor.a = transparencyValue;
		secondColorRenderer.color = newColor;

		newColor = thirdColorRenderer.color;
		newColor.a = transparencyValue;
		thirdColorRenderer.color = newColor;

		newColor = fourthColorRenderer.color;
		newColor.a = transparencyValue;
		fourthColorRenderer.color = newColor;

		newColor = fifthColorRenderer.color;
		newColor.a = transparencyValue;
		fifthColorRenderer.color = newColor;

		newColor = sixthColorRenderer.color;
		newColor.a = transparencyValue;
		sixthColorRenderer.color = newColor;


	}

}
