﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DuelReadyCountdown : MonoBehaviour
{
    public DuelModeController duelModeController;
    public GameObject timer;
    public Text countdownText;
    public float countdownValue;
    private float count;
    
    private void OnEnable()
    {
        count = countdownValue;
    }

    private void Update()
    {
        countdownText.text = count.ToString("0");
        count -= Time.deltaTime;
        if (count < 1)
        {
            //Timer ends here
            duelModeController.ActivateSnakes();
            timer.SetActive(true);
            if(!SnakeManager.instance.isDuelOn)
                SnakeManager.instance.DuelModeToggle();
            gameObject.SetActive(false);
        }
    }

}
