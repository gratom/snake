﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DuelModeController : MonoBehaviour
{
    public GameObject camera;
    public GameObject lookingForPlayersPanel;
  //  public GameObject playerSnake;
  //  public GameObject playerHead;
    public GameObject readyCountText;

    private void OnEnable()
    {
        GameManager.instance.DuelModeClicked();
        PlayerPrefs.SetInt("points", 50);

        //if (GameManager.instance.loadingGUI.activeInHierarchy)
          //  GameManager.instance.loadingGUI.SetActive(false);
        lookingForPlayersPanel.SetActive(true);

        Population.instance.StopSnakeLoading();
        SnakeSpawner.Instance.DestroyAllSnakes();
      //  SnakeManager.instance.DestroyAllSnakes();
       // Population.instance.allSnakes.Clear();
       // FoodManager.instance.Initialize();
        FoodSpawner.Instance.SwitchToDuelModeAndReset();
        Invoke("SetSnakesDuel", 1.5f);
        Invoke("StartDuel", Random.Range(3f, 4f));
        
        camera.GetComponent<CameraManager>().enabled = false;
        camera.transform.position = new Vector3(0f, 50f, 0f);
        camera.GetComponent<Camera>().orthographicSize = 50f;
       // playerHead.transform.localScale = new Vector3(1f, 1f, 1f);
    }

    public void SetSnakesDuel()
    {
        Population.instance.AddSnakesDuel();

      //  playerSnake.SetActive(true);
        GameManager.instance.AddPlayer();

        SnakeSpawner.Instance.PauseAllSnakes();
        //playerSnake.transform.position = new Vector3(-50f, 0f, -25f);
     //   playerSnake.transform.position = Population.instance.duelPlayer_position;
      //  playerSnake.transform.GetChild(0).transform.localPosition = new Vector3(0f, 0f, 0f);
     //   playerSnake.GetComponent<Snake>().isLoaded = false;
      //  playerSnake.GetComponent<Snake>().points = 0;
        //Debug.Log("Player isLoaded = " + playerSnake.GetComponent<Snake>().isLoaded);
    }

    public void StartDuel()
    {
        GameManager.instance.StartDuelMode();
        lookingForPlayersPanel.SetActive(false);
        
        readyCountText.SetActive(true);
    }

    private void OnDisable()
    {
        camera.GetComponent<CameraManager>().enabled = true;
        GameManager.instance.SetNormalMode();
       // if(playerSnake.GetComponent<Snake>() != null)
       //     playerSnake.GetComponent<Snake>().enabled = true;
       
       // SnakeManager.instance.DestroyCompleteSnake(playerSnake.GetComponent<Snake>());

        GameManager.instance.scoreboardCanvas_Duel.SetActive(false);
      //  playerSnake.GetComponent<Snake>().points = 0;
      //  playerSnake.GetComponent<Snake>().snakePieces.Clear();
        GameManager.instance.lastSnakePoints = 0;
    }

    public void ActivateSnakes()
    {
        //playerSnake.GetComponent<Snake>().isLoaded = true;
        SnakeManager.instance.ActivateSnakesAI_Duel();
    }
    
}
