﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinManager : MonoBehaviour {

	// Use this for initialization

	public static SkinManager _instance;

	public ColorTemplate[] availableColorTemplate;
    public ColorTemplate[] teamBasedColorTemplate;

	void Awake()
	{
		_instance = this;
	}

	void Start () {
		
	}

	public ColorTemplate GetARandomColorTemplate()
	{
        Debug.Log("template");
		int randomNumber = Random.Range (1, availableColorTemplate.Length - 1);
		return availableColorTemplate[randomNumber];
	}

    public void ShuffleTeamColors()
    {
        List<ColorTemplate> newArray = new List<ColorTemplate>();
        List<ColorTemplate> lastArray = new List<ColorTemplate>();
        for (int x = 0; x < teamBasedColorTemplate.Length; x++)
        {
            lastArray.Add(teamBasedColorTemplate[x]);
        }
        for (int x = 0; x < teamBasedColorTemplate.Length; x++)
        {
            int rnd = Random.Range(0, lastArray.Count);
            newArray.Add(lastArray[rnd]);
            lastArray.RemoveAt(rnd);
        }

        teamBasedColorTemplate = newArray.ToArray();
    }

    public ColorTemplate GetTeamBasedColorTemplate(string team)
    {
        Debug.Log("team template "+ team);
        if (team == "A")
        {
            return teamBasedColorTemplate[0];
        }
        else if (team == "B")
        {
            return teamBasedColorTemplate[1];
        }
        else if (team == "C")
        {
            return teamBasedColorTemplate[2];
        }
        else
        {
            Debug.Log("team template " + team);
            return teamBasedColorTemplate[0];
        }
    }

}
