﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour
{

    public float foodValue;
    bool beingEat;
    // float timerToBeActive = 2;
    float timerToDestroy = 30;
    bool destruction;
    public SpriteRenderer spriteRenderer;
    public SphereCollider col;

    public void SetColor(Color col)
    {
        spriteRenderer.color = col;
        //StartCoroutine(fadeStartEffect());
    }
    public void Init()
    {
        beingEat = false;
        destruction = false;
        //timerToBeActive = 0;
        timerToDestroy = 30;
    }
    public void SetSize(float value)
    {
        foodValue = value;
        transform.localScale = new Vector3(1 + value, 1 + value, 1 + value);


    }
    IEnumerator fadeStartEffect()
    {
        Color col = spriteRenderer.color;
        Color origin = col;
        origin.a = 0;
        float lerper = 0;
        float lerperTime = 1f;
        while (lerper <= 1)
        {
            lerper += Time.deltaTime / lerperTime;
            spriteRenderer.color = Color.Lerp(origin, col, lerper);
            yield return new WaitForEndOfFrame();
        }
    }

    /*  void Update() {


       /*   if (timerToBeActive >= 0)
          {
              timerToBeActive -= Time.deltaTime;
          }*/
    /*
    if (timerToDestroy >= 0)
    {
        timerToDestroy -= Time.deltaTime;
        if (timerToDestroy <= 0) {
            destruction = true;
        }


    }
    */

    //       if (destruction)
    //     {            
    //			if (Vector3.Distance(Camera.main.transform.position, this.transform.position) > 100)
    //            {
    //				//Destroy(this.gameObject);
    //				gameObject.GetComponent<SphereCollider>().enabled = false;
    //				this.enabled = false;
    //				transform.position = Vector3.up * 1000f;
    //				FoodManager.instance.foodDiedList.Add (this);
    //
    //
    //            }            
    //        }

    //  }


    /*    void OnTriggerEnter(Collider obj) {
            if (timerToBeActive >= 0) return;
            if (beingEat) return;


         /*   if (obj.transform.tag == "Snake") {

                Snake snakeParam = obj.transform.root.GetComponent<Snake>();
    //            if (snakeParam.isPlayer)
            //    {
                    snakeParam.points += Mathf.RoundToInt(foodValue * 2);
                    if (snakeParam != null) {
                        snakeParam.ControlSnakeScale ();
                    }
                    if (snakeParam.netPlayerData != null) {
                        if (snakeParam.netPlayerData.aiReport != null) {
                            Debug.Log("Score update getting called now on....................");
                            snakeParam.netPlayerData.aiReport.updatedscore += Mathf.RoundToInt (foodValue * 2);
                        }
                    }
           //     }
                StartCoroutine(moveAndDisappear(obj.transform));
            }*/

    // }

    public void MoveAndDisappear(Transform targetTransform)
    {
        StartCoroutine(moveAndDisappear(targetTransform));
    }

    IEnumerator moveAndDisappear(Transform targetTransform)
    {

        //Debug.Log ("Eating Food......");

        beingEat = true;
        col.enabled = false;
        float lerper = 0;
        float lerperTime = 0.3f;
        Vector3 currentPosition = transform.position;
        while (lerper <= 1)
        {
            lerper += Time.deltaTime / lerperTime;
            try
            {
                transform.position = Vector3.Lerp(currentPosition, targetTransform.position - Vector3.up, lerper);
            }
            catch
            {
                lerper = 1;
            }
            yield return new WaitForEndOfFrame();
        }

        if (FoodManager.instance.isduelModeOn)
        {
            FoodManager.instance.SpawnFood(Random.Range(1, 2.5f), FoodManager.instance.foodColorRandomList[Random.Range(0, FoodManager.instance.foodColorRandomList.Length)], Vector3.zero);
        }
        else
        {
            if((Random.value > 0.5 ? 0 : 1) == 1)
            {
                FoodManager.instance.SpawnFood(Random.Range(1, 2.5f), FoodManager.instance.foodColorRandomList[Random.Range(0, FoodManager.instance.foodColorRandomList.Length)], Vector3.zero);
            }
        }

        beingEat = false;
        //Destroy(this.gameObject);
        //this.gameObject.SetActive(false);
        //col.enabled = false;
        this.enabled = false;
        transform.position = Vector3.up * 1000f;
        FoodManager.instance.foodDiedList.Add(this);
    }


}
