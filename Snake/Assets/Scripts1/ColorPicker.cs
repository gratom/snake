﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour {
	
	public Image targetImage;
	Sprite colorPlate;

	float imageWidth,imageHeight;

	void Start () {
		RectTransform rect = GetComponent<RectTransform> ();
		colorPlate = GetComponent<Image> ().sprite;
		imageWidth = rect.rect.width;
		imageHeight = rect.rect.height;
	}
	

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			targetImage.color = GetColor ();
		}
	}

	Color GetColor(){
		Vector3 relativePosition = transform.InverseTransformPoint (Input.mousePosition);
		relativePosition.x /= imageWidth;
		relativePosition.y /= imageHeight;
		return colorPlate.texture.GetPixelBilinear (relativePosition.x, relativePosition.y);
	}


}
