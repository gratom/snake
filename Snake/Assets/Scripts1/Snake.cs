﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CnControls;
using UnityEngine.Profiling;
using Random = UnityEngine.Random;

public class Snake : MonoBehaviour
{
    
    public enum snakeType {small, medium, big, reallybig, superbig};
    public snakeType val;
    public string snakeName;
    public float speed = 5;
    public float rotatingSpeed = 0.7f;
    public int points;
    public int speedMultiplier = 2;
    int originalSpeedMultiplier;
    int startingPoints = 50; // You can't go lower than this value
    float pieceForPoints = 19f; // Each 50p gets 1 piece //18

    int pointsForScale = 500; // Each 250p the scale increases 
    float scaleOffset = 0.04f; // by this value
    
    public SnakeHead snakeHead;
    public List<Piece> snakePieces = new List<Piece>();
   // public int colorTemplateNo = 0;
    public int curMaskNo = -1;
    public ColorTemplate colorTemplate;
    //List<ColorTemplate> colorTemplates = new List<ColorTemplate>();
    public AI aiModule;
    public static Snake player;
    public bool isPlayer;
    public bool dieing;
    public float referenceScale;
    public bool isTransparent;
    public bool isGlowing;
    public bool hasNoMask = false;
    public GameObject crownObject;
    public GameObject smileyObject;
    public SpriteRenderer smileyRenderer;
    public GameObject mask, hat;
    public TextMesh txtName = null;
    public Transform[] spawnPoints;         // Spawn points for snake
    
    public bool isLoaded = false;
    public bool isPieceMovement = false;
    private bool isDuelOn = false;
    private float MOVlerpTime = 0.50f;
    private IEnumerator _glowCoroutine;
    public string snakeTeam;
    private bool _isUpdateGlowAlpha;
    public float GlowAlpha = 0f;
    private int count = 0;
    public bool isPaused = false;
    private bool isSnakePieceSpawn = false;
    public float immuneTimer = 0f;
    public bool FollowPlayer=false;
    public Snake PlayerToFollow;
    public bool IsBabySnake=false;
    public DateTime SpawnTime { get; set; }

    public void Awake()
    {
        originalSpeedMultiplier = speedMultiplier;
        
      /*  ColorTemplate[] colorTemps = FindObjectsOfType<ColorTemplate>();

        for (int i = 0; i < colorTemps.Length; i++)
        {
            colorTemplates.Add(colorTemps[i]);
        }

        if (!isPlayer)
            colorTemplateNo = Random.Range(0, colorTemplates.Count - 1);
        else
            colorTemplateNo = 7;*/

        count++;
    }



    public ColorTemplate GetColorTemplate()
    {
        if (!isPlayer)
        {
            if (!IsBabySnake)
            {
                if (!GameManager.instance.IsTeamMode())
                {
                    if (colorTemplate == null)
                    {
                        colorTemplate = SkinManager._instance.availableColorTemplate[Random.Range(2, SkinManager._instance.availableColorTemplate.Length - 1)];
                    }
                }
                else
                {
                    if (colorTemplate == null)
                    {
                        Debug.Log("colors");
                        colorTemplate = SkinManager._instance.GetTeamBasedColorTemplate(snakeTeam);
                    }
                }
                return colorTemplate;
            }
            else
            {
                if (!GameManager.instance.IsTeamMode())
                {
                    return LooknFeelDisplay._instance.selectedColorTemplate;
                }
                else
                {
                    return SkinManager._instance.GetTeamBasedColorTemplate(snakeTeam);
                }
            }

            
        }
        else
        {
            
            if (!GameManager.instance.IsTeamMode())
            {
                return LooknFeelDisplay._instance.selectedColorTemplate;
            }
            else
            {
                return SkinManager._instance.GetTeamBasedColorTemplate(snakeTeam);
            }
        }
    }

    public void ApplySnakeColor()
    {
      /*  if (colorTemplateNo == colorTempNo)
            return;
        colorTemplateNo = colorTempNo;*/
        snakeHead.SetColorBasedOnTemplate();
        for (int i = 1; i < snakePieces.Count; i++)
        {
            SetColorBasedOnTemplate(i);
        }
    }

    public void ApplyMask(int maskNo,int maskTab)
    {
        curMaskNo = maskNo;
        MaskScript maskScript = GetComponent<MaskScript>();
        if (maskScript)
        {
            maskScript.SetMask(curMaskNo,maskTab, GetColorTemplate().colors[0]);
        }
    }

    public void OnEnable()
    {
        //GameManager.instance.lastSnakePoints = 0;
        immuneTimer = 0f;
        isPaused = false;
        isRestartDeath = false;
        isLoaded = false;
        dieing = false;
        snakePieces.Clear();
        snakeHead.sc.enabled = true;
        isPlayer = snakeHead.IsPlayer;
        snakeTeam = "";
        aiModule.aiType = AIType.Normal;
        colorTemplate = null;
        if (GameManager.instance.IsDuelMode())
        {
            isDuelOn = true;
        }
        else
        {
            isDuelOn = false;
        }

        if (this.isPlayer)
        {
            player = this;
            GameManager.instance.dev_SpeedUp = false;
            referenceScale = 1f;
            SpawnTime = DateTime.Now;
            points = GameManager.instance.lastSnakePoints; //PlayerPrefs.GetInt("points", 50);
            StartCoroutine(InitializeLength(true));
            int playerMask = PlayerPrefs.GetInt("PlayerMask", -1);
            int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);
            if (playerMaskTab == -1)
                playerMask = -1;
            ApplyMask(playerMask, playerMaskTab);        //Disable temporarily due to mask copyright
            SnakeManager.instance.isPlayerDead = false;
            if (!GameManager.instance.IsDuelMode())
            {
                SetRandomSpawnPosition();
                GameManager.instance.AddBabySnake();
            }

          //  if (GameManager.instance.IsTeamMode())
                snakeTeam = "A";
          //  else
           //     snakeTeam = "";

            

        }
        else if (IsBabySnake)
        {
            
            snakeTeam = "A";
          //  GameManager.instance.playerSnake.GetComponent<Snake>().snakeTeam="A";
            SetBabySpawnPoint();
          //  Invoke("MaskFunctionAI", 0.5f);

            int playerMask = PlayerPrefs.GetInt("PlayerMask", -1);
            int playerMaskTab = PlayerPrefs.GetInt("TabIndex", -1);
            if (playerMaskTab == -1)
                playerMask = -1;
            ApplyMask(playerMask, playerMaskTab);
            Invoke("MaskFunctionBabyAI", 0.5f);
        }
        else
        {


            if (count == 1)
            {
                count++;
                return;
            }

            Invoke("MaskFunctionAI", 0.5f);
        }

        crownObject.SetActive(false);
        StopCoroutine("consumeSnakeIfSprint");
        StartCoroutine("consumeSnakeIfSprint");
        
    }

    public Vector3 GetSnakeHeadPosition()
    {
        return snakeHead.transform.position;
    }

    private void MaskFunctionBabyAI()
    {
        StartCoroutine(InitializeLength(true));
        ApplySnakeColor();
    }

    private void MaskFunctionAI()
    {
        StartCoroutine(InitializeLength(true));
        if (MaskManager.instance != null)
        {
            if (hasNoMask)
            {
                //Debug.Log("No mask");
                ApplyMask(-1,-1);        //Disable temporarily due to mask copyright

            }
            else
            {
                //Debug.Log("Mask applied");
                int maskTab = MaskManager.instance.GetRandomTab();
                ApplyMask(MaskManager.instance.GetRandomMask(maskTab), maskTab);      //Disable temporarily due to mask copyright
            }
        }
        // colorTemplate = SkinManager._instance.availableColorTemplate[Random.Range(0, SkinManager._instance.availableColorTemplate.Length-1)];
        // colorTemplateNo =  Random.Range(0, colorTemplates.Count - 1);
       // Debug.Log("snake team " + snakeTeam);
        ApplySnakeColor();
    }

    IEnumerator InitializeLength(bool shouldWait)
    {
        if (shouldWait)
            yield return new WaitForSeconds(0.2f);
        ControlInitialLength();
        ControlSnakeScale();

    }

    // It will just relocate the snake to random position so it look like snake has been instatitaed at random spawn point
    public void SetRandomSpawnPosition()
    {
        int randomSpawnPoint = Random.Range(0, spawnPoints.Length);
        transform.position = spawnPoints[randomSpawnPoint].position;
        snakeHead.gameObject.transform.position = transform.position;
        GameManager.instance.chosenPlayerSpawnPoint = spawnPoints[randomSpawnPoint];
    }

    public void SetBabySpawnPoint()
    {
        transform.position = GameManager.instance.chosenPlayerSpawnPoint.GetChild(0).position;
        snakeHead.gameObject.transform.position = transform.position;
    }

    private void Update()
    {
        if (isLoaded)
        {
            if (isPaused)
                return;

            if (immuneTimer > 0)
            {
                immuneTimer -= Time.deltaTime;
                if (immuneTimer < 0)
                    immuneTimer = 0;
            }

            if (isDuelOn)
                snakeHead.Duel_HeadUpdate();
            else
                snakeHead.HeadUpdate();

            var MOVlerpTime = GetSnakePieceMoveLerp();
            int count = 0;

            foreach (var snakePiece in snakePieces)
            {
            //    if(snakePiece.reference == null)
            //    {
            //        Debug.Log("Snake piece reference NULL");
            //        if (count == 0)
            //        {
            //            snakePiece.reference = snakeHead.transform;
            //            Debug.Log("snake head null");
            //        }

            //        continue;
            //    }

            //    if(!snakePiece.reference.gameObject.activeInHierarchy)
            //    {
            //        Debug.Log("Snake piece reference not active in hierarchy!");
            //        continue;

            //    }

            //    if (snakePiece == null)
            //    {
            //        Debug.Log("Snake piece NULL");
            //        continue;
            //    }

                snakePiece.Move(MOVlerpTime);

                if (_isUpdateGlowAlpha)
                {
                    snakePiece.UpdateGlowAlpha(GlowAlpha);
                }

                count++;
            }

            _isUpdateGlowAlpha = false;
        }
    }

    public float GetSnakePieceMoveLerp()
    {
        if (isPlayer)
        {
            if (speedMultiplier > 1f)
                return MOVlerpTime = .25f;
            else
                return MOVlerpTime = .15f;
        }
        else
        {
            if (speedMultiplier > 1f)
                return MOVlerpTime = .21f;
            else
                return MOVlerpTime = .11f;
        }
    }

    public void FixedUpdate()
    {
        if (!isLoaded)
            return;

        ControlSnakeScale();      //Commented out to keep head size of snake constant
        ControlSnakeLenght();

        ControlSprint();
    }

    public void UpdateSnake()
    {
        ControlSprint();   
    }

    public bool IsAutoPlayer()
    {
        return !isPlayer;
    }

    IEnumerator consumeSnakeIfSprint()
    {
        while (true)
        {
            if (speedMultiplier != 1)
            {
                if (isPlayer)
                {
                    points -= 5; // Consume only the main player
                }
            }
            yield return new WaitForSeconds(0.25f);
        }
    }

    void ControlSprint()
    {

        if (points <= 60) 
        { 
            speedMultiplier = 1;
            
            if (isGlowing)
            {
                SetGlow(false);
            }
            return; 
        }
        // IF IS PLAYER
        if (isPlayer)
        {
            //DEV_EDIT || SNAKE SPEED CHANGE 1 
            if (GameManager.instance.dev_SpeedUp)
            {
                //    Debug.Log("Speed multiplier values is: " + originalSpeedMultiplier);
                speedMultiplier = originalSpeedMultiplier;
                //    Debug.Log("Speeding up");
                if (!isGlowing)
                {
                    SetGlow(true);
                }
            }
            else
            {
                speedMultiplier = 1;
                if (isGlowing)
                {
                    SetGlow(false);
                }
            }
            return;
        }
        // IF IS BOT
        if (!isPlayer)
        {
            if (aiModule.sprint)
            {
                speedMultiplier = originalSpeedMultiplier;
                if (!isGlowing)
                {
                    SetGlow(true);
                }
            }
            else
            {
                speedMultiplier = 1;
                if (isGlowing)
                {
                    SetGlow(false);
                }
            }
        }
    }

    void SetGlow(bool shouldGlow)
    {
        isGlowing = shouldGlow;
        
        if (_glowCoroutine != null) StopCoroutine(_glowCoroutine);

        _glowCoroutine = shouldGlow ? FadeInGlow() : FadeOutGlow();
        StartCoroutine(_glowCoroutine);
    }
    
    private IEnumerator FadeInGlow()
    {
        for (var i = GlowAlpha; i <= 1f; i += 10 * Time.deltaTime)
        {
            SetUpdateGlowAlpha(i);
            yield return new WaitForEndOfFrame();
        }

        SetUpdateGlowAlpha(1f);
    }

    private IEnumerator FadeOutGlow()
    {
        for (var i = GlowAlpha; i >= 0; i -= 10 * Time.deltaTime)
        {
            SetUpdateGlowAlpha(i);

            yield return new WaitForEndOfFrame();
        }
        
        SetUpdateGlowAlpha(0f);
    }
    
    private void SetUpdateGlowAlpha(float i)
    {
        _isUpdateGlowAlpha = true;
        GlowAlpha = i;
    }
    
    public void ControlSnakeScale()
    {
        float scalechange = (float)points / pointsForScale;  

        float scale = 1 + scalechange * scaleOffset;
        if (scale > GameManager.instance.MaxSnakeScale)
        {
            scale = GameManager.instance.MaxSnakeScale;
        }
        referenceScale = Mathf.Lerp(referenceScale, scale, Time.deltaTime * 1);
        if (1 + scalechange * scaleOffset < GameManager.instance.MaxSnakeScale)
        {
            scale = (float)points / pointsForScale;
            scale *= 0.1f;
            float newdistance = 1 + scale;
            pieceDistanceOffset = newdistance;  // Mathf.Lerp(pieceDistanceOffset, newdistance,Time.deltaTime*1);
        }
        else
        {
            scale = (GameManager.instance.MaxSnakeScale - 1) / scaleOffset;
            scale *= 0.1f;
            pieceDistanceOffset = 1 + scale;
        }
    }

    void ControlSnakeLenght()
    {
        if (points < startingPoints) points = startingPoints;
        int snakeParts = GetSnakeParts();//Mathf.RoundToInt(points / (pieceForPoints * referenceScale));
        //if (isPlayer)
        //{
        //    Debug.Log("SnakeParts: "+ snakeParts + " SnakePiecesCount:- " + snakePieces.Count);
        //}

        if (snakePieces.Count < snakeParts && snakePieces.Count < 300)
        {
            AddNewPart();
        }
        else if (snakePieces.Count > snakeParts)
        {
            RemovePart();
        }
    }

    public int GetSnakeParts()
    {
        if (points > 1500)
        {
            int diff = Mathf.RoundToInt(1500 / (pieceForPoints * referenceScale));
            diff += Mathf.RoundToInt((points - 1500) / (30 * referenceScale));
            return diff;
        }
        else if (points > 650)
        {
            int diff = Mathf.RoundToInt(650 / (pieceForPoints * referenceScale));
            diff += Mathf.RoundToInt((points - 650) / (22 * referenceScale));
            return diff;
        }
        else
        {
            return Mathf.RoundToInt(points / (pieceForPoints * referenceScale));
        }
    }

    void ControlInitialLength()
    {
        StartCoroutine("controlInitialLength");
    }

    IEnumerator controlInitialLength()
    {
        while (!SnakeManager.instance.isLoaded)
            yield return null;

        if (points < startingPoints) points = startingPoints;

        int snakeParts = GetSnakeParts();////Mathf.RoundToInt(points / (pieceForPoints * referenceScale));
                                         //  Debug.Log(snakeParts);
       // Debug.Log("Inside control initial length"+ snakeParts);
        snakeHead.gameObject.transform.position = transform.position;

        //mobile infinite pieces bug..
        if (isPlayer)
        {
            if (snakeParts > 3)
                snakeParts = 3;
        }
        snakePieces = new List<Piece>();
        if (!isSnakePieceSpawn)
        {
            isSnakePieceSpawn = true;
            for (int i = 0; i < snakeParts; i++)
            {
                int nl = snakePieces.Count - 1;

                //newPiece = SnakeManager.instance.InstantiatePiece(snakeHead.transform.position, Quaternion.identity);


                //Piece pieceParams = SnakeManager.instance.InstantiatePiece(snakeHead.transform.position, Quaternion.identity); //newPiece.GetComponent<Piece>();
                //snakePieces.Add(pieceParams);

                //nl++;
                //pieceParams.InitializePiece(nl, this);
                //InitializePiece(nl);
            }
        }
        
        SetGlow(false);
        for (int i = 0; i < snakePieces.Count; i++)
        {
            snakePieces[i].gameObject.SetActive(true);
            snakePieces[i].transform.position = this.transform.position;
            if (snakePieces[i].reference == null)
            {
                if (i > 0)
                {
                    snakePieces[i].reference = snakePieces[i - 1].transform;
                }
                else
                    snakePieces[i].reference = snakeHead.transform;

            }
            yield return null;

        }
        if (isDuelOn)
            isLoaded = false;
        else
            isLoaded = true;
    }

    void RemovePart()
    {
        if (isPieceMovement)
            return;

        StartCoroutine("removePart");
    }

    IEnumerator removePart()
    {
        isPieceMovement = true;
        /*   snakePieces[snakePieces.Count - 1].position = Vector3.up * 1000f;
           snakePieces[snakePieces.Count - 1].SetParent(null);
           snakePieces[snakePieces.Count - 1].position = GameManager.instance.hidePos;
           snakePieces[snakePieces.Count - 1].GetComponent<SphereCollider>().enabled = false;
           GameManager.instance.pieceDiedList.Add(snakePieces[snakePieces.Count - 1]);
           Color glowEffectColor = snakePieces[snakePieces.Count - 1].GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color;
           glowEffectColor.a = 0;
           snakePieces[snakePieces.Count - 1].GetChild(0).GetChild(1).GetComponent<SpriteRenderer>().color = glowEffectColor;*/
        if (snakePieces.Count > 0)
        {
            SnakeManager.instance.DestroyPiece(snakePieces[snakePieces.Count - 1]);

            snakePieces.RemoveAt(snakePieces.Count - 1);
        }

        yield return null;
        isPieceMovement = false;

    }

    void AddNewPart()
    {
        if (isPieceMovement)
            return;
        StartCoroutine("addNewPart");
    }

    IEnumerator addNewPart()
    {
        isPieceMovement = true;
        bool isPresent = false;
       
        int nl = snakePieces.Count - 1;
        Piece pieceParams = SnakeManager.instance.InstantiatePiece(snakeHead.transform.position, Quaternion.identity);

        /*for (int i = 0; i < SnakeManager.instance.usedSnakes.Count; i++)
        {
            Snake snake = SnakeManager.instance.usedSnakes[i].GetComponent<Snake>();
            if(snake.snakePieces.Contains(pieceParams))
            {
                Debug.Log("Piece already exists!!");
                StartCoroutine("addNewPart");
                isPresent = true;
            }
            
        }
        
        if (!snakePieces.Contains(pieceParams) && !isPresent)
        {
            Debug.Log("No problem with piece");
            snakePieces.Add(pieceParams);

            nl++;
            pieceParams.InitializePiece(nl, this);
            InitializePiece(nl);

            // RescaleSnake();
            yield return null;
            isPieceMovement = false;
        }*/

        snakePieces.Add(pieceParams);

        nl++;
        pieceParams.InitializePiece(nl, this);
        InitializePiece(nl);
        
        yield return null;
        isPieceMovement = false;

    }

    public void DeathRoutine()
    {
        if (dieing) return;
        dieing = true;
        if (isDuelOn && isPlayer)
        {
            PlayerDeathDuel();
            return;
        }

        foreach (Piece piece in snakePieces)
        {
            Vector3 randomCircle = Random.insideUnitSphere * 3;
            randomCircle.y = 0;
            int value = Random.Range(Mathf.RoundToInt(points / 2 / snakePieces.Count), Mathf.RoundToInt(points / 3 / snakePieces.Count));
            value = Mathf.Clamp(value, 1, 20);
            FoodManager.instance.SpawnFood(value, FoodManager.instance.foodColorRandomList[Random.Range(0, FoodManager.instance.foodColorRandomList.Length)], piece.transform.position + randomCircle, false);
            piece.sc.enabled = false;
        }

        //Debug.Log("death snake name:- " + gameObject.name);
        if (isDuelOn && isPlayer)
        {
            Debug.Log("Inside isduelon and isplayer");
            PlayerDeathDuel();
        }
        else
        {
           // Debug.Log("Inside else deathroutine");
            StartCoroutine(FadeToDeathRoutine());
        }
    }

    public void PlayerDeathDuel()
    {
        SnakeManager.instance.PauseDuelModeSnakes();
        immuneTimer = 1f;
        dieing = false;
      /*  if (mask != null && hat != null)
        {
            mask.SetActive(false);
            hat.SetActive(false);
        }

        //Debug.Log("Player died in Duel - GOver canvas active!");
        SnakeManager.instance.isPlayerDead = true;
        points = 0;*/
        //GameManager.instance.DestroySnake(this);

        //if (GameManager.instance.isHomeButtonPressed)
        //{
        //    GameManager.instance.isHomeButtonPressed = false;
        //    gameObject.SetActive(false);
        //}

        GameManager.instance.GameOverDuel();
    }
    
    IEnumerator FadeToDeathRoutine()
    {
        if (mask != null && hat != null)
        {
            mask.SetActive(false);
            hat.SetActive(false);
        }

        float lerper = 1;
        float lerpTime = 0.5f;
        if (Vector3.Distance(snakeHead.transform.position, GameManager.instance.cam.transform.position) <= 375) //250)
        {
            if (!isRestartDeath)
            {
                //Debug.Log("Inside Restart Death");
                while (lerper >= 0)
                {
                    lerper -= Time.deltaTime / lerpTime;
                    for (int i = 1; i < snakePieces.Count; i++)
                    {
                        SpriteRenderer spriteRenderer = snakePieces[i].spriteRenderer;
                        Color spritecol = spriteRenderer.color;
                        spritecol.a = lerper;
                        spriteRenderer.color = spritecol;
                    }
                    Color spriteHeadCol = snakeHead.spriteRenderer.color;
                    spriteHeadCol.a = lerper;
                    snakeHead.spriteRenderer.color = spriteHeadCol;
                    snakeHead.gameObject.tag = "Untagged";
                    yield return new WaitForEndOfFrame();

                }
            }
            else
            {
                for (int i = 1; i < snakePieces.Count; i++)
                {
                    SpriteRenderer spriteRenderer = snakePieces[i].spriteRenderer;
                    Color spritecol = spriteRenderer.color;
                    spritecol.a = lerper;
                    spriteRenderer.color = spritecol;
                }
                Color spriteHeadCol = snakeHead.spriteRenderer.color;
                spriteHeadCol.a = lerper;
                snakeHead.spriteRenderer.color = spriteHeadCol;
                snakeHead.gameObject.tag = "Untagged";
            }
        }

        if (isPlayer)
        {
            if (!isRestartDeath)
            {
                yield return new WaitForSeconds(0.2f);
            }

            //if(GameManager.instance.isDuelModeOn)
            //{
            //    Debug.Log("Player died in duel mode!");
            //    GameManager.instance.gameOverDuelCanvas.SetActive(true);
                
            //}
            //GameManager.instance.DestroySnake(this);

            //if (GameManager.instance.isHomeButtonPressed)
            //{
            //    Debug.Log("Inside ishome button pressed");
            //    //GameManager.instance.isHomeButtonPressed = false;
            //    GameManager.instance.DestroySnake(this);
            //    gameObject.SetActive(false);
            //    GameManager.instance.OnGameOver_Event();
            //}
            //else
            //{
                Debug.Log("Inside fade death");
                GameManager.instance.DestroySnake(this);
                GameManager.instance.gameOverCanvas.SetActive(true);
                GameManager.instance.OnGameOver_Event();
                gameObject.SetActive(false);
            //}
            
        }
        else if (IsAutoPlayer())
        {

            GameManager.instance.DestroySnake(this);
        }
        else
        {
            GameManager.instance.DestroySnake(this);
        }
    }
    public bool isRestartDeath = false;

    float pieceDistanceOffset = 1f;
    float pieceYDistanceOffset = 0.01f;
    public SpriteRenderer glow;
    
    public void InitializePiece(int index)
    {
        snakePieces[index].transform.localScale = new Vector3(referenceScale, referenceScale, referenceScale);
        SetColorBasedOnTemplate(index);
    }
    
    void SetColorBasedOnTemplate(int index)
    {
        int colorIndex = index;
        while (colorIndex >= GetColorTemplate().colors.Length)
        {
            colorIndex -= GetColorTemplate().colors.Length;
        }

        //Color pieceColor = GetColorTemplate().colors[Random.Range(0,colorIndex)];
        Color[] color = GetColorTemplate().colors;
        Color pieceColor = color[index % color.Length];

        if (isPlayer)
        {
            pieceColor.a = (1 - PlayerPrefs.GetFloat("PlayerTransparency"));
        }
        else
        {
            if (isTransparent)
                pieceColor.a = 0.5f;
            else
                pieceColor.a = 1.0f;
        }
        snakePieces[index].spriteRenderer.color = pieceColor;
        snakePieces[index].transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = snakePieces.Count;
    }

    public void SetCrown(bool shouldShow)
    {
        if (shouldShow)
        {
            if (isPlayer && !crownObject.activeSelf)
            {
                GameManager.instance.StartTopOneTracking();
            }
            crownObject.SetActive(true);
        }
        else
        {
            if (isPlayer && crownObject.activeSelf)
            {
                GameManager.instance.StopTopOneTracking();
            }
            crownObject.SetActive(false);   
        }
    }

    void OnDisable()
    {
        StopAllCoroutines();
        isPieceMovement = false;
        isGlowing = false;
        isLoaded = false;
        if(isPlayer)
        GameManager.instance.babySnake.DeathRoutine();
       // GameManager.instance.lastSnakePoints = 0;
    }

    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }
}
