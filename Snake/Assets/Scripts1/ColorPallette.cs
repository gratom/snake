﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ColorPallette : MonoBehaviour, IDragHandler {

	public Image targetImage;
	Sprite colorPlate;

	float imageWidth,imageHeight;

	void Start () {


		RectTransform rect = GetComponent<RectTransform> ();
		colorPlate = GetComponent<Image> ().sprite;
		imageWidth = rect.rect.width*rect.localScale.x;
		imageHeight = rect.rect.height*rect.localScale.y;

//		Debug.Log ("Current scale is:************* " + rect.localScale);

	}


	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Color selectedColor = GetColor ();
			ColorSelectionManager._instance.SetColorToRing (selectedColor);
		}
	}

	Color GetColor(){
		Vector3 relativePosition = transform.InverseTransformPoint (Input.mousePosition);
//		Debug.Log ("Color picking position: " + relativePosition);
		relativePosition.x /= imageWidth;
		relativePosition.y /= imageHeight;
		return colorPlate.texture.GetPixelBilinear (relativePosition.x, relativePosition.y);


	}



	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
		/*
		//Once we have the UV coordinates, we use a function called GetPixelBilinear on the texture. This will return the colour of the texture at the given UV coordinates. 
		Color selectedColor = GetColor ();

		if(selectedColor.a != 0)
			ColorSelectionManager._instance.SetColorToRing (selectedColor);
		*/
	}


	#endregion
}

