﻿using UnityEngine;
using System.Collections;

public class AdHandler : MonoBehaviour {
	/*
	private static AdHandler instance;

	[SerializeField] bool showBannerAdmob = false;
	[SerializeField] bool showInterstatialAdmob = false;
	[SerializeField] bool showInterstatialChartBoost = false;
	[SerializeField] bool showVideoChartboost = false;
	[SerializeField] bool showVideoUnity = false;



	[SerializeField] string ad_mob_banner_id_ios = "";
	[SerializeField] string ad_mob_interstatial_id_ios = "";

	[SerializeField] string ad_mob_banner_id_android = "";
	[SerializeField] string ad_mob_interstatial_id_android = "";

	// [SerializeField] string vungle_appid_android = "Test_Android";
	// [SerializeField] string vungle_appid_ios = "Test_iOS";

	[SerializeField] string unity_gameID = "33675";

	public static AdHandler GetInstance()
	{
		return instance;
	}

	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
		{
			//if not, set instance to this
			instance = this;
			//Sets this to not be destroyed when reloading scene
			DontDestroyOnLoad(gameObject);
		}
		//If instance already exists and it's not this:
		else if (instance != this)
		{
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    
		}
	}

	void Start()
	{
		if(PlayerPrefs.GetInt("ads", 1) == 1) //1=show-ad 0=dont-show-ad
		{
			InitailizeAdNetworks();
			if(showBannerAdmob)
			{
				Invoke("StartBanner", 2f);
			}
		}
		InitailizeVideoNetworks();
	}

	void StartBanner()
	{
		AdHandler.GetInstance().showAdmobBanner();
	}

	void InitailizeVideoNetworks()
	{
		//  if(showVideoVungle)
		//  {
		//   Vungle.init (vungle_appid_android, vungle_appid_ios, "vungleTest");
		//   Vungle.onAdFinishedEvent += OnVungleAdFinished;
		//  }

		if(showInterstatialChartBoost)
		{
			ChartboostHandler.initialize();
		}
	}

	void InitailizeAdNetworks()
	{
		if(showBannerAdmob && showInterstatialAdmob)
		{
			#if UNITY_IOS
			AdMobHandler.initialize (ad_mob_banner_id_ios, ad_mob_interstatial_id_ios);
			#endif
			#if UNITY_ANDROID
			#endif
		}
		else
		{
			if(showBannerAdmob)
			{
				#if UNITY_IOS
				AdMobHandler.initialize (ad_mob_banner_id_ios, "");
				#endif
				#if UNITY_ANDROID
				#endif
			}
			else if(showInterstatialAdmob)
			{
				#if UNITY_IOS
				AdMobHandler.initialize ("", ad_mob_interstatial_id_ios);
				#endif
				#if UNITY_ANDROID
				#endif
			}
		}
	}

	// Called when the player pauses
	void OnApplicationPause(bool pauseStatus) {
		//  if (pauseStatus)
		//   Vungle.onPause();
		//  else
		//   Vungle.onResume();
	}

	public void showAdmobBanner()
	{
		if(PlayerPrefs.GetInt("ads", 1) == 0)
			return;

		if(showBannerAdmob)
		{
		}
	}

	public void hideAdmobBanner()
	{
		if(PlayerPrefs.GetInt("ads", 1) == 0)
			return;

		if(showBannerAdmob)
		{
		}
	}

	public void showInterstatial()
	{
		if(PlayerPrefs.GetInt("ads", 1) == 0)
			return;

		if(ChartboostHandler.isInterstitalCached() && showInterstatialChartBoost)
		{
			ChartboostHandler.showInterstatialAd();
		}
		else if(showInterstatialAdmob)
		{
		}
	}

	public void showRewardedVideo()
	{
		if(ChartboostHandler.isRewardedCached())
		{
			ChartboostHandler.showRewardedVideo();
		}
	}

	// void OnVungleAdFinished(AdFinishedEventArgs args)
	// {
	//  VideoAdFinishAction();
	// }

	void VideoAdFinishAction()
	{
		RewardOnVideoFinish();
	}

	public void RewardOnVideoFinish()
	{
		PlayerPrefs.SetInt("WatchedAd", 1);
		PlayerPrefs.SetInt("points", Snake.player.points);
		Application.LoadLevel(Application.loadedLevel);
	}

	public bool IsRewardedAdAvailable()
	{
		if(ChartboostHandler.isRewardedCached())
		{
			return true;
		}
		return false;
	}
	*/
}