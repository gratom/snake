﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
#if GPG
using GooglePlayGames.BasicApi;
using GooglePlayGames;
#endif
public class MainMenuGUI : MonoBehaviour
{
    public InputField nameField;
    public InputField wormlenghtField;
    public string androidNewAppLink;
    public string iosNewAppLink;
    public string zombieGameLink;
    public string puzzleGameLink;

    public GameObject normalPlayButton;
    public GameObject dummyMSportsButton;
    public GameObject msportsTournamentButton, noAdsButton, babySnakeButton;
    public GameObject settingsPanel;
    public GameObject msportPromotionPage;

    public GameObject[] zombieDownloadPopup;

    public Text scoreLast;

    void OnEnable()
    {
        StartCoroutine(CheckIfColorAndMaskChangedNow());
        msportPromotionPage.SetActive(false);
    }

    void Start()
    {
        string username = PlayerPrefs.GetString(Constants.prefstr_Username);
        if (!username.Equals(""))
        {
            nameField.text = username;
        }
        var se = new InputField.SubmitEvent();
        se.AddListener(OnNameEnter);

        if (nameField != null)
            nameField.onEndEdit = se;


    /*    if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            SnakeManager.instance.isLoaded = true;
        }*/
    }

    public void CheckMSportsAvailability()
    {
        //		if (MSportsManager._instance.IsMsportsAvailable) {
        //			if (!MSportsManager._instance.isFirstTime) {
        //				dummyMSportsButton.SetActive (true);
        //				//normalPlayButton.SetActive (false);
        //				msportsTournamentButton.SetActive (false);
        //			} else {
        //				dummyMSportsButton.SetActive (false);
        //				msportsTournamentButton.SetActive (true);
        //			}
        //
        //
        //			if (!PlayerPrefs.HasKey (GameUtils.PREFS_MSPORTS_UPDATE)) {
        //				msportsTournamentButton.SetActive (false);
        //				dummyMSportsButton.SetActive (true);
        //
        //			}
        //
        //		} else {
        //			
        //			//normalPlayButton.SetActive (true);
        //
        //		}
    }

    public void OpenZombiePopup()
    {
        zombieDownloadPopup[0].SetActive(true);
        zombieDownloadPopup[1].SetActive(true);
    }

    public void OpenZombie()
    {
#if UNITY_ANDROID
        Application.OpenURL(zombieGameLink);
#elif UNITY_IPHONE
        Application.OpenURL(puzzleGameLink);
#endif
    }

    public void CloseZombiePopup()
    {
        zombieDownloadPopup[0].SetActive(false);
        zombieDownloadPopup[1].SetActive(false);
    }

    public void InitializePlayVersion()
    {
        if (this.gameObject.activeSelf)
        {

            //normalPlayButton.SetActive (true);
            msportsTournamentButton.SetActive(false);

        }
    }

    public void CheckMsportsTournament()
    {
        msportsTournamentButton.SetActive(false);
        //dummyMSportsButton.SetActive (true);
        //MSportsManager._instance.StartMSportsTournament ();
        msportPromotionPage.SetActive(false);
    }

    IEnumerator CheckIfColorAndMaskChangedNow()
    {
        yield return new WaitForSeconds(1);
        AchievementManager._instance.CheckIfColorAndMaskChanged();    //Commented out recently
    }

    void OnNameEnter(string name)
    {
        if (name != null && name.Length != 0)
        {
            PlayerPrefs.SetString(Constants.prefstr_Username, name);
            PlayerPrefs.Save();
        }

    }

    public void OnRateButtonClick()
    {
        #if EVENTS
        Dictionary<string, object> vals = new Dictionary<string, object>();
        vals.Add("show_reason", "rate button clicked");
        vals.Add("rate_result", 5);
        AppMetrica.Instance.ReportEvent("rate_us", vals);
#endif
#if UNITY_ANDROID
        Application.OpenURL(androidNewAppLink);
#elif UNITY_IPHONE
        Application.OpenURL(iosNewAppLink);
#endif
    }

    public static void OnShareButtonClick()
    {
        AchievementManager._instance.SocialMediaShare();  //Commented out recently
    }

    public void OnClickNewGame()
    {
#if UNITY_ANDROID
        Application.OpenURL(androidNewAppLink);
#endif

#if UNITY_IOS
		Application.OpenURL(iosNewAppLink);
#endif
    }

    public void OnRemoveAdsButtonClick()
    {
#if UIP
#if UNITY_ANDROID
        UnityIAP.instance.BuyItem("com.camc.slitherio.snakes.worms.adremove");    //Commented out recently
#endif
#if UNITY_IPHONE
        UnityIAP.instance.BuyItem("com.camc.removeads");
#endif
#endif
        GameManager.instance.ifAdRemoveClicked = true;
    }

    public void OnBabySnakeButtonClick()
    {
#if UIP
#if UNITY_ANDROID
        UnityIAP.instance.BuyItem("com.camc.slitherio.snakes.worms.babysnake");    //Commented out recently
#endif
#if UNITY_IPHONE
        UnityIAP.instance.BuyItem("com.camc.babysnake");
#endif
#endif
      //  GameManager.instance.ifAdRemoveClicked = true;
    }

    public void OnBundleButtonClick()
    {
#if UIP
#if UNITY_ANDROID
        UnityIAP.instance.BuyItem("com.camc.slitherio.snakes.worms.bundle");    //Commented out recently
#endif
#if UNITY_IPHONE
        UnityIAP.instance.BuyItem("com.camc.bundle");
#endif
#endif
        //  GameManager.instance.ifAdRemoveClicked = true;
    }

    public void OnTestPurchaseClicked()
    {
#if UIP
#if UNITY_ANDROID
        Debug.Log("purchased test id");
        UnityIAP.instance.BuyItem("android.test.purchased");    //Commented out recently
#endif
#endif
    }

    public void OnEggButtonClick(string eggName)
    {
#if UIP
#if UNITY_ANDROID
        UnityIAP.instance.BuyItem("com.camc.slitherio.snakes.worms."+eggName);    //Commented out recently
#endif
#if UNITY_IPHONE
        UnityIAP.instance.BuyItem("com.camc."+eggName);
#endif
#endif
        //  GameManager.instance.ifAdRemoveClicked = true;
    }

    public void OnRestoreAdsButtonClick()
    {
#if UIP
        UnityIAP.instance.RestorePurchase();  //Commented out recently
#endif
    }

    public void OnChangeMaxLength()
    {
        int length = 0;
        if (!int.TryParse(wormlenghtField.text, out length))
        {
            length = 0;
        }
        if (length <= 10)
        {
            length = 10;
        }
        GameManager.instance.MaxWormPieceLength = length;
    }
    
    public void OnSettingsButtonClicked()
    {
        settingsPanel.SetActive(true);
    }

    public void OnLeaderboardClick()
    {
        if (Social.localUser.authenticated)
        {
#if UNITY_ANDROID
#if GPG
            //Social.ShowLeaderboardUI();
            // ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GPGSID.leaderboard_leaderboard);   //Commented out recently
            PlayGamesPlatform.Instance.ShowLeaderboardUI();
#endif
#else
            //Social.ShowLeaderboardUI();
            Social.Active.ShowLeaderboardUI ();
#endif

        }
        else
        {
            Debug.Log("Inside leaderboard else");
#if GPG
            Social.localUser.Authenticate((bool success) =>
               {
                   if (success)
                   {
#if UNITY_ANDROID
                       Social.ShowLeaderboardUI();
                       //((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GPGSID.leaderboard_leaderboard);    //Commented out recently
                       PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSID.leaderboard_slither_high_score);
#else
						//Social.ShowLeaderboardUI();
						Social.Active.ShowLeaderboardUI ();
#endif
                   }
                   else
                   {
                       Debugger.instance.DebugLog("Authentication failed");
                   }
               });
#endif
        }
    }

    public void OnDuelModeLeaderboardClick()
    {
        if (Social.localUser.authenticated)
        {
#if UNITY_ANDROID
#if GPG
            //Social.ShowLeaderboardUI();
            // ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GPGSID.leaderboard_leaderboard);   //Commented out recently
            PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSID.leaderboard_duel_mode_leaderboard);
#endif
#else
            //Social.ShowLeaderboardUI();
            Social.Active.ShowLeaderboardUI ();
#endif

        }
        else
        {
            Debug.Log("Inside leaderboard else");
#if GPG
            Social.localUser.Authenticate((bool success) =>
            {
                if (success)
                {
#if UNITY_ANDROID
                    Social.ShowLeaderboardUI();
                    //((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(GPGSID.leaderboard_leaderboard);    //Commented out recently
                    PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGSID.leaderboard_duel_mode_leaderboard);
#else
						//Social.ShowLeaderboardUI();
						Social.Active.ShowLeaderboardUI ();
#endif
                }
                else
                {
                    Debugger.instance.DebugLog("Authentication failed");
                }
            });
#endif
        }
    }

    public void ShareScore()
    {
        NativeShare ns = new NativeShare();
        ns.SetTitle("Got new score!");
        string link = "https://play.google.com/store/apps/details?id=com.carmin.slitherio.snake.worm";
#if UNITY_IPHONE
        link = "https://apps.apple.com/us/app/worm-eater-teams-in-snake/id1219674818";
#endif
        string scoreText = string.Format("My last score was {0} can you beat my score? \n "+link, scoreLast.text);
        ns.SetText(scoreText);
        ns.Share();
        AchievementManager._instance.SocialMediaShare();
    }

    public void OnAchievementButtonClicked()
    {

        Debug.Log("Achievement button clicked");
        if (Social.localUser.authenticated)
        {
#if UNITY_ANDROID
#if GPG
             Social.ShowAchievementsUI();
            ((PlayGamesPlatform)Social.Active).ShowAchievementsUI();  //Commented out recently
#endif
#else
            //Social.ShowLeaderboardUI();
            Social.Active.ShowAchievementsUI();
#endif

        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                   if (success)
                   {
                       Debug.Log("Login Sucess");
#if UNITY_ANDROID
#if GPG
                        Social.ShowAchievementsUI();
                        ((PlayGamesPlatform)Social.Active).ShowAchievementsUI();   //Commented out recently
#endif
#else
                    //Social.ShowLeaderboardUI();
                    Social.Active.ShowAchievementsUI();
#endif
                   }
                   else
                   {
                       Debugger.instance.DebugLog("Authentication failed");
                       Debug.Log("Login failed");
                   }
            });
        }

        }
    }
