﻿#if GPG
using GooglePlayGames.BasicApi;
using GooglePlayGames;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SnakeManager : MonoBehaviour {

    public static SnakeManager instance;

    public Image loadingSlider;
    public SnakeSpawner spawner;
  //  public int totalPieces;
    public int totalSnakes;

  //  public GameObject piecePrefab;
  //  public GameObject snakePlayerPrefab;
    public GameObject babySnakePrefab;

  //public List<GameObject> activePieces;
  //public List<GameObject> notActivePieces;

 //   public List<GameObject> usedSnakes;
 //   public List<GameObject> notUsedSnakes;

    public int TeamASnakes;
    public int TeamBSnakes;
    public int TeamCSnakes;

    public bool isLoaded = false;
    public bool isDuelOn = false;
    public GameObject menuUI;
    public GameObject lnf;
    public GameObject loadingUI;
    public bool isPlayerDead = false;
    public SpawnPoint[] spawnPoints;
 
    //public TrashMan poolManager;
    //public bool isMovingPiece = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Initiate();
            DontDestroyOnLoad(this.gameObject);
        }
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {

#if GPG
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();
#endif
        Debug.Log("play games platform activated!");
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Logged in to google play services!!!!");
                AchievementManager._instance.PlaySessionCount();
                AchievementManager._instance.UnlockAllProMasks();
            }
            else
            {
                Debug.LogError("Unable to sign into google play service");
            }
            SnakeManager.instance.isLoaded = true;
        });

    }

    private void Update()
    {
        if(isDuelOn)
        {
            if (SnakeSpawner.Instance.GetSnakesRealCount() < 2 && !GameManager.instance.isDuelTimerOver && SnakeSpawner.Instance.playerSnake!=null)
            {
                PlayerWonDuel();
            }
        }

      /*  if(notUsedSnakes.Count > 35)
        {
            notUsedSnakes.RemoveAt(notUsedSnakes.Count - 1);
        }*/
    }

   

    private void PlayerWonDuel()
    {
        SnakeSpawner.Instance.isGameStarted = false;
        int duelModeWins = PlayerPrefs.GetInt("DuelModeWins", 0);
        duelModeWins++;
        PlayerPrefs.SetInt("DuelModeWins", duelModeWins);
        GameManager.instance.UpdateDuelModeStates();
        GameManager.instance.DuelModeWon();
        DuelModeToggle();

#if EVENTS
        Dictionary<string, object> vals = new Dictionary<string, object>();
        vals.Add("level", "Duel");
        vals.Add("result", "Won");
        vals.Add("time", (int)GameManager.instance.duelModeTimer.timerCount);
        vals.Add("skin_name", "");
        vals.Add("progress", 0);
        AppMetrica.Instance.ReportEvent("level_finish", vals);
        AppMetrica.Instance.SendEventsBuffer();
#endif

    }

    public void DuelModeToggle()
    {
        if (isDuelOn)
            isDuelOn = false;
        else
            isDuelOn = true;
    }

  /*  public Transform GetRandomSpawnPoint(Snake snakeParams)
    {
      

        List<SpawnPoint> spawnPointAvailable = new List<SpawnPoint>();
        for (int x = 0; x < spawnPoints.Length; x++)
        {
            if (spawnPoints[x].canSpawn == 0f)
            {
                if (SnakeSpawner.Instance.playerSnake!=null)
                {
                    if (Vector3.Distance(spawnPoints[x].transform.position, GameManager.instance.GetPlayerSnakeParams().GetSnakeHeadPosition()) > 200f)
                        spawnPointAvailable.Add(spawnPoints[x]);
                }
                else
                {
                    spawnPointAvailable.Add(spawnPoints[x]);
                }
            }
        }
        if (spawnPointAvailable.Count == 0)
            return null;

        int xrnd = Random.Range(0, spawnPointAvailable.Count);
        
        Debug.Log("spawn point available : "+spawnPointAvailable.Count);
        spawnPointAvailable[xrnd].CannotSpawn(snakeParams);
        return spawnPointAvailable[xrnd].transform;
    }*/

    public void ResetSpawnPoints()
    {
        foreach (SpawnPoint spawnPoint in spawnPoints)
        {
            spawnPoint.Reset();
        }
    }


    public void DuelTimerOverDecision()
    {
        int highestCount;
        int snakeNo;

        if(SnakeSpawner.Instance.GetSnakesRealCount() > 0 && SnakeSpawner.Instance.playerSnake!=null)
        {
            highestCount = spawner.playerSnake.points;
            snakeNo = -1;
            for (int i = 0; i < spawner.snakes.Length; i++)
            {
                if (spawner.snakes[i] != null)
                {
                    ECSSnake snakeAI = spawner.snakes[i];//.GetComponent<Snake>();
                    if (snakeAI.points > highestCount)
                    {
                        highestCount = snakeAI.points;
                        snakeNo = i;
                    }
                }
            }

            if(snakeNo == -1)
            {
                
                PlayerWonDuel();
               
                isPlayerDead = true;
                GameManager.instance.DestroyAllSnakesDuel();   //Destroy other snakes and player too
                
            }
            else
            {
                //  Debug.Log("Inside else decision");
                PauseDuelModeSnakes();
               // GameManager.instance.DestroyAllExceptWinnerDuel();
                GameManager.instance.GameOverDuel();
            }
        }
    }

    public void PauseDuelModeSnakes()
    {
      
        for (int x = 0; x < spawner.snakes.Length; x++)
        {
            if (spawner.snakes[x] != null)
                spawner.PauseSnake(spawner.snakes[x]);
        }
    }

    public void UnpauseDuelModeSnakes()
    {

        for (int x = 0; x < spawner.snakes.Length; x++)
        {
            if (spawner.snakes[x] != null)
                spawner.UnPauseSnake(spawner.snakes[x]);
        }
    }

 

    public void Initiate()
    {
        if (isLoaded)
            return;

      //  usedSnakes = new List<GameObject>();
       // notUsedSnakes = new List<GameObject>();
        StartCoroutine(loadNumerically());
    }

    IEnumerator loadNumerically()
    {
        /*     for (int x = 0; x < totalSnakes; x++)
             {
                var snake = Instantiate(snakePlayerPrefab);
                snake.SetActive(false);
                notUsedSnakes.Add(snake);
                yield return new WaitForEndOfFrame();
             }

             yield return new WaitForSeconds(1f);*/
        while (!isLoaded)
            yield return null;
      //  isLoaded = true;
        yield return new WaitForSeconds(1f);
        //   while (spawner.snakes.Count < (30)) //3
        yield return null;

        lnf.SetActive(true);
        menuUI.SetActive(true);
        loadingUI.SetActive(false);
    }

    public GameObject InstantiateForDuel(int index)
    {
      /*  GameObject snakeDuel;
        if (index == 0)
        {
            snakeDuel = Instantiate(snakePlayerPrefab, Population.instance.duelBot1_position, Quaternion.identity);
        }
        else if (index == 1)
        {
            snakeDuel = Instantiate(snakePlayerPrefab, Population.instance.duelBot2_position, Quaternion.identity);
        }
        else
        {
            snakeDuel = Instantiate(snakePlayerPrefab, Population.instance.duelBot3_position, Quaternion.identity);
        }

        var snake = snakeDuel.GetComponent<Snake>();
        snake.hasNoMask = true;
        int rndMaskTab = MaskManager.instance.GetRandomTab();
        snake.ApplyMask(MaskManager.instance.GetRandomMask(rndMaskTab), rndMaskTab);        //Disable temporarily due to mask copyright
        snake.crownObject.SetActive(false);
     //   snake.colorTemplate = SkinManager._instance.availableColorTemplate[Random.Range(0, SkinManager._instance.availableColorTemplate.Length - 1)];
        // int colorTemplateNo = Random.Range(0, 7);
        snake.ApplySnakeColor();
        Population.instance.AddSnake(snake, Population.instance.GetRandomName());

        snakeDuel.GetComponent<Snake>().isLoaded = false;
        snakeDuel.GetComponent<Snake>().points = 0;       
        snakeDuel.SetActive(true);
        usedSnakes.Add(snakeDuel);
        return snakeDuel;*/
        return null;
    }

    public void DestroyPiece(Piece _piece)
    {
        if (_piece == null)
            return;

        _piece.transform.parent = null;
        _piece.reference = null;
        _piece.transform.localPosition = new Vector3(0, 0, -3.21f);
        _piece.sc.enabled = true;

        TrashMan.despawn(_piece.gameObject);
    }

    public Piece InstantiatePiece(Vector3 pos,Quaternion rot)
    {
        //int rndPick = Random.Range(0, notActivePieces.Count - 100);
        //GameObject nP = TrashMan.spawn(piecePrefab);//notActivePieces[0];
        /*   GameObject nP = GameObject.Instantiate(piecePrefab);//notActivePieces[0];

           Piece p = nP.GetComponent<Piece>();

           // notActivePieces.RemoveAt(0);
           nP.tag = "Snake";
         //  activePieces.Add(nP);
           nP.transform.position = pos;
           nP.transform.rotation = rot;

          // nP.SetActive(true);
           return p;*/
        return null;
    }

    public void DestroySnake(GameObject _snake)
    {
     /*   if (!_snake.GetComponent<Snake>().isPlayer)
        {
            //Debug.Log("Not player");
            notUsedSnakes.Add(_snake);
            usedSnakes.Remove(_snake);
        }
        else
        {
            //Debug.Log("Snake player Destroy");
            if (usedSnakes.Contains(_snake))
            {
               // Debug.Log("Snake player in used list!");
                usedSnakes.Remove(_snake);
            }
        }
        Debug.Log("destroyed snake");
       
        _snake.SetActive(false);*/
    }

    public GameObject InstantiateSnake(Vector3 pos, Quaternion rot)
    {
        /*   GameObject nP = notUsedSnakes[0];
           notUsedSnakes.RemoveAt(0);

           if (nP.GetComponent<Snake>().IsBabySnake)
           {
               nP = notUsedSnakes[0];
               notUsedSnakes.RemoveAt(0);

           }

           if (!nP.GetComponent<Snake>().isPlayer)
           {
               usedSnakes.Add(nP);
               nP.transform.position = pos;
               nP.transform.rotation = rot;
            //   nP.SetActive(true);
           }
           else
           {
               //Debug.Log("Player in instantiate snake - Used list");
               nP.transform.position = pos;
               nP.transform.rotation = rot;
            //   nP.SetActive(true);
           }

           return nP;*/
        return null;
    }

    public void InstantiateSnake_Duel(Vector3 pos, Quaternion rot, Snake item)
    {
      //  notUsedSnakes.Remove(item.gameObject);
      //  usedSnakes.Add(item.gameObject);
       // item.gameObject.SetActive(true);
        //isDuelGameplay = true;
    }

    IEnumerator destroyAllSnakes()
    {
        isLoaded = false;
        /*    while(usedSnakes.Count > 0)
            {
                Snake snakeParam = usedSnakes[0].GetComponent<Snake>();
                for (int x = 0; x < snakeParam.snakePieces.Count; x++)
                {
                    DestroyPiece(snakeParam.snakePieces[x]);
                   // yield return null;
                }
                DestroySnake(usedSnakes[0]);
               // DestroyCompleteSnake(usedSnakes[0].GetComponent<Snake>());
                yield return null;
            }*/

        yield return null;
     
        isLoaded = true;
    }

    public void DestroyAllSnakes()
    {
        StartCoroutine("destroyAllSnakes");
        /*  isLoaded = false;

          while (usedSnakes.Count > 0)
          {
              Snake snakeParam = usedSnakes[0].GetComponent<Snake>();
              for (int x = 0; x < snakeParam.snakePieces.Count; x++)
              {
                  DestroyPiece(snakeParam.snakePieces[x].gameObject);
              //    yield return null;
              }
              DestroySnake(usedSnakes[0]);
              // DestroyCompleteSnake(usedSnakes[0].GetComponent<Snake>());
           //   yield return null;
          }
          isLoaded = true;*/
        //  StartCoroutine(destroyAllSnakes());
        ResetSpawnPoints();
    }

    public void DestroyAllSnakePieces(Snake snakeParam)
    {
        StartCoroutine(destroyAllSnakePieces(snakeParam));
    }

    IEnumerator destroyAllSnakePieces(Snake snakeParam)
    {
        for (int x = 0; x < snakeParam.snakePieces.Count;x++)
        {
            DestroyPiece(snakeParam.snakePieces[x]);
            yield return null;
        }
        
    }

    public void DestroyCompleteSnake(Snake snakeParam)
    {
        for (int x = 0; x < snakeParam.snakePieces.Count; x++)
        {
            DestroyPiece(snakeParam.snakePieces[x]);
          //  yield return null;
        }
        
        
        DestroySnake(snakeParam.gameObject);

       // StartCoroutine(destroyCompleteSnake(snakeParam));
    }

  /*  IEnumerator destroyCompleteSnake(Snake snakeParam)
    {

    }*/

    public void ActivateSnakesAI_Duel()
    {
        spawner.UnPauseAllSnakes();
      /*  for(int i = 0; i < usedSnakes.Count; i++)
        {
            var snakeItem = usedSnakes[i].GetComponent<Snake>();
            snakeItem.isLoaded = true;
        }*/
    }
    
}
