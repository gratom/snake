﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EggsManager : MonoBehaviour
{
    public static EggsManager instance;
    public Text[] eggNumbers;
    public int eggs=0;
    public GameObject duelEggBox;
   
    public MaskSelector selector;
    public Button btnDaily;
    public Animator eggAnim;
    public DailyReward dailyReward;
    public void Awake()
    {
       // PlayerPrefs.DeleteAll();
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
      //  PlayerPrefs.SetInt("eggs", 100);
        eggs = PlayerPrefs.GetInt("eggs", 9);
        UpdateText();
        StartCoroutine(checkDailyReward());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator checkDailyReward()
    {
        while (true)
        {
            if (dailyReward.CheckAvailableReward())
            {
                btnDaily.interactable = true;
                eggAnim.SetBool("twitch", true);
            }
            else
            {
                btnDaily.interactable = false;
                eggAnim.SetBool("twitch", false);
            }
           

            yield return new WaitForSeconds(1f);
        }
    }

    public void ToggleDailyRewardBtns(bool trig)
    {
        btnDaily.interactable = trig;
        eggAnim.SetBool("twitch", trig);
    }

    public void UpdateText()
    {
        for (int x = 0; x < eggNumbers.Length; x++)
        {
            eggNumbers[x].text = eggs.ToString();
        }
    }

    public void IncreaseEgg(int num = 1)
    {
        eggs += num;
        PlayerPrefs.SetInt("eggs", eggs);
        UpdateText();
    }

    public bool DecreaseEgg(int num = 1)
    {
        if (eggs - num < 0)
            return false;
        eggs -= num;
        PlayerPrefs.SetInt("eggs",eggs);
        UpdateText();
        return true;
    }

    public void ShowEggsAndGetEggs()
    {
        duelEggBox.SetActive(true);
        IncreaseEgg();
    }

    public void HideEggs()
    {
        Debug.Log("hide eggs");
        duelEggBox.SetActive(false);
    }

    public void YesToBuy()
    {
        if (DecreaseEgg(selector.price))
        {
            selector.UnlockEggMaskAndSelect();

        }
        else
        {
            AchievementManager._instance.achivementUnlockText.text = "You need to get more eggs\n win more duels to earn eggs";
            GameManager.instance.dialogBox.SetActive(true);
        }

        GameManager.instance.eggBuyBox.SetActive(false);
    }

    public void NoToBuy()
    {
        GameManager.instance.eggBuyBox.SetActive(false);
    }
    
}
