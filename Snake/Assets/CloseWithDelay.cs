﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseWithDelay : MonoBehaviour
{
    float time = 0f;
    public float TotalTime = 2f;
    private void OnEnable()
    {
        time = TotalTime;

    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 0f)
        {
            time -= Time.deltaTime;
            if (time <= 0f)
            {
                time = 0f;
                this.gameObject.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        this.gameObject.SetActive(false);
    }
}
