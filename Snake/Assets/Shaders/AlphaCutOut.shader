﻿Shader "Spine/LitNormalSkeleton" {
 
    Properties {
       _Color ("Main Color", Color) = (1,1,1,1)
       _MainTex ("Texture Atlas", 2D) = "white" {}
       _BumpMap ("Normalmap", 2D) = "bump" {}
       _Brightness ("Brightness Boost", Range(0,3)) = 2
       _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
 
    SubShader {
       Tags {"IgnoreProjector"="True" "RenderType"="TransparentCutout"}
       Fog { Mode Off }
       Blend One OneMinusSrcAlpha
       
       CGPROGRAM
          #pragma surface surf Lambert alphatest:_Cutoff
         
          sampler2D _MainTex;
          sampler2D _BumpMap;
          fixed4 _Color;
          half _Brightness;
         
          struct Input {
             float2 uv_MainTex;
             fixed4 color : COLOR;
             float2 uv_BumpMap;
          };
         
          void surf (Input IN, inout SurfaceOutput o) {
             fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * IN.color * _Color;
             o.Albedo = c.rgb * _Brightness;
             o.Alpha = c.a;
             o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
          }
       ENDCG
    }
 
    FallBack "Transparent/Cutout/Bumped Diffuse"
}