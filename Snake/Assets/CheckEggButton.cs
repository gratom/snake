﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckEggButton : MonoBehaviour
{
    public Button get2EggsBtn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
#if YODO
        if (YodoManager.instance.IsRewardAdReady())
        {
            get2EggsBtn.interactable = (true);
        }
        else
            get2EggsBtn.interactable = (false);
#endif

#if IRONSOURCE
    if (ISManager.instance.isVideoAvailable())//.IsRewardAdReady())
        {
            get2EggsBtn.interactable = (true);
        }
        else
            get2EggsBtn.interactable = (false);
#endif

#if APPLOVIN
    if (AppsLovinManager.instance.isVideoAvailable())//.IsRewardAdReady())
        {
            get2EggsBtn.interactable = (true);
        }
        else
            get2EggsBtn.interactable = (false);
#endif
    }
}
