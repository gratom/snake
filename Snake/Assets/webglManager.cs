﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class webglManager : MonoBehaviour
{
    public GameObject[] buttons;
    // Start is called before the first frame update
    void Start()
    {
#if UNITY_WEBGL
        for (int x = 0; x < buttons.Length; x++)
        {
            buttons[x].SetActive(false);
        }
#endif 
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
