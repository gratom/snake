﻿#if YODO
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YodoManager : MonoBehaviour
{
    public static YodoManager instance;
    public string RewardType = "";
    public string adType = "";
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

    }
    // Start is called before the first frame update
    void Start()
    {
         Yodo1U3dAds.InitializeSdk();

        Yodo1U3dSDK.setInterstitialAdDelegate((Yodo1U3dConstants.AdEvent adEvent, string error) => {
            Debug.Log("InterstitialAdDelegate:" + adEvent + "\n" + error);
            switch (adEvent)
            {
                case Yodo1U3dConstants.AdEvent.AdEventClick:
                    Debug.Log("Interstital ad has been clicked.");
                 /*   if (adType == "pause")
                        GameObject.FindObjectOfType<UI_Manager>().pauseAd = false;

                    if (adType == "game")
                        GameObject.FindObjectOfType<Loading_Manager>().HideBlackPanel();*/

                    adType = "";
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventClose:
                    Debug.Log("Interstital ad has been closed.");
                   /* if (adType == "pause")
                        GameObject.FindObjectOfType<UI_Manager>().pauseAd = false;

                    if (adType == "game")
                        GameObject.FindObjectOfType<Loading_Manager>().HideBlackPanel();*/

                    adType = "";
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventShowSuccess:
                    Debug.Log("Interstital ad has been shown successful.");
                    
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventShowFail:
                    GameManager.instance.OnGameOverPlayButton();
                    Debug.Log("Interstital ad has been show failed, the error message:" + error);
                  /*  if (adType == "pause")
                        GameObject.FindObjectOfType<UI_Manager>().pauseAd = false;

                    if (adType == "game")
                        GameObject.FindObjectOfType<Loading_Manager>().HideBlackPanel();*/

                   adType = "";
                    break;
            }
            
            
        });

        Yodo1U3dSDK.setRewardVideoDelegate((Yodo1U3dConstants.AdEvent adEvent, string error) =>
        {
            Debug.Log("RewardVideoDelegate:" + adEvent + "\n" + error);
            switch (adEvent)
            {
                case Yodo1U3dConstants.AdEvent.AdEventClick:
                    Debug.Log("Rewarded video ad has been clicked.");
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventClose:
                    Debug.Log("Rewarded video ad has been closed.");
                     isAdClosed = true;
                    //  GameManager.instance.OnGameOverPlayButton();
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventShowSuccess:
                    isAdClosed = true;
                    isRewarded = true;
                    Debug.Log("Rewarded video ad has shown successful.");
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventShowFail:
                      GameManager.instance.OnGameOverPlayButton();
                    Debug.Log("Rewarded video ad show failed, the error message:" + error);
                    break;
                case Yodo1U3dConstants.AdEvent.AdEventFinish:
                    Debug.Log("Rewarded video ad has been played finish, give rewards to the player.");
                    ShowRewardAdSuccess();
                    break;
            }
        });
    }

    // Update is called once per frame  bool isAdClosed = false;
    bool isAdClosed = false;
    bool isRewarded = false;

    void Update()
    {
        if (isAdClosed)
        {
            if (isRewarded)
            {
                if (RewardType == "")
                {
                    //Debug.Log("is rewarded :- " + isRewarded);
                    PlayerPrefs.SetInt("WatchedAd", 1);
                    //PlayerPrefs.SetInt("points", Snake.player.points);
                    GameManager.instance.loadingOverlay.SetActive(true);
                    GameManager.instance.KillAllSnakes();

                }
                else if (RewardType == "egg")
                {
                    int gotEggs = 2;
                    EggsManager.instance.IncreaseEgg(gotEggs);
                    AchievementManager._instance.achivementUnlockText.text = "You got " + gotEggs.ToString() + " Eggs!";
                    GameManager.instance.dialogBox.SetActive(true);
                    RewardType = "";
                }
                else
                {
                    GameManager.instance.ExtendDuelModeTimer();
                    RewardType = "";
                }
                isRewarded = false;
            }
            else
            {
                // Ad closed but user skipped ads, so no reward 
                // Ad your action here 
            }
            isAdClosed = false;  // to make sure this action will happen only once.
        }
    }

     public void ShowInterstitial(string type="")
    {
        if (PlayerPrefs.GetInt("ads", 1) == 0)
            return;

        adType = type;
        if(IsInterstitialReady())
        Yodo1U3dAds.ShowInterstitial();
    }

    public bool IsInterstitialReady()
    {
        return Yodo1U3dAds.InterstitialIsReady();
    }

    public void ShowRewardAd(string reward)
    {
        if (IsRewardAdReady())
        {
            RewardType = reward;
            Yodo1U3dAds.ShowVideo();
        }
    }

    public bool IsRewardAdReady()
    {
       return Yodo1U3dAds.VideoIsReady();
    }

    public void ShowRewardAdSuccess()
    {
     /*   if (RewardType == "Fuel")
        {
            Invoke("GiveReward_Fuel", 0.1f);
        }
        else if (RewardType == "Health")
        {
            Invoke("GiveReward_Health", 0.1f);
        }
        else if (RewardType == "DoubleReward")
        {
            Invoke("GiveReward_DoubleReward", 0.1f);
        }
        else if (RewardType == "GameContinue")
        {
            Invoke("GiveReward_GameContinue", 0.1f);
        }
        else if (RewardType == "FreeCoins")
        {
            Invoke("GiveReward_FreeCoins", 0.1f);
        }*/
        RewardType = "";
    }

  /*  void GiveReward_DoubleReward()
    {
        GameObject.FindObjectOfType<Game_Manager>().GiveReward_DoubleReward();
    }

    void GiveReward_GameContinue()
    {
        GameObject.FindObjectOfType<Game_Manager>().GiveReward_GameContinue();
    }

    void GiveReward_Fuel()
    {
        GameObject.FindObjectOfType<RewardedVideo_Popup>().GiveReward_Fuel();
    }

    void GiveReward_Health()
    {
        GameObject.FindObjectOfType<RewardedVideo_Popup>().GiveReward_Health();
    }

    void GiveReward_FreeCoins()
    {
        GameObject.FindObjectOfType<Video_Ad_Store>().GiveReward_FreeCoins();
    }*/
}
#endif