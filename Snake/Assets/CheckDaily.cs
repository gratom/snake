﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckDaily : MonoBehaviour
{
    public Button btn;
    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        
            anim.SetBool("Pulse", btn.IsInteractable());
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
