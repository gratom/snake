﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DailyReward : MonoBehaviour
{
    
    public Image[] dailyRewards;
    public GameObject[] claimButtons;
    public GameObject[] shineButtons;
    public Color normalRewardColor;
    public Color takenRewardColor;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        SetupDailyRewardScreen();
    }

    public void SetupDailyRewardScreen()
    {
        RefreshPage();
        
    }

    public bool CheckAvailableReward()
    {
        if (PlayerPrefs.GetString("lastclaimtime", "") == "")
        {
            Debug.Log("no time");
            return true;
        }

        string strDate = PlayerPrefs.GetString("lastclaimtime", "");

        DateTime now1 = DateTime.FromBinary(long.Parse(strDate));
        DateTime newTime = now1.AddHours(24);
      //  Debug.Log(newTime + " " + DateTime.Now);
        DateTime newTime2 = now1.AddHours(48);
        if (DateTime.Now > newTime2)
        {
            PlayerPrefs.SetInt("rewardNum", 0);
            return true;
        }
        else if (DateTime.Now > newTime)
        {
            Debug.Log(newTime + " " + DateTime.Now + " is great ");
            return true;
        }
        else
            return false;

    }

    public int GetRemainingTimeNormal()
    {
        if (PlayerPrefs.GetString("lastclaimtime", "") == "")
        {
          //  Debug.Log("no time");
            return 0;
        }

        string strDate = PlayerPrefs.GetString("lastclaimtime", "");

        DateTime now1 = DateTime.FromBinary(long.Parse(strDate));
     //   DateTime newTime = now1.AddHours(24);
      //  TimeSpan subNow = now1.Subtract(newTime);
        DateTime currentTime = DateTime.Now;
        TimeSpan newSpan = currentTime.Subtract(now1);
        return ((24*60*60)-(int)newSpan.TotalSeconds);
      /*  float normalizedValue = Mathf.InverseLerp(0, subNow.Minutes, newSpan.Minutes);
        float result = Mathf.Lerp(0f, 1f, normalizedValue);
      
        return result;*/
    }

    public void GetRewardEgg()
    {
        int num = PlayerPrefs.GetInt("rewardNum", 0);
        int gotEggs = 0;
        switch (num)
        {
            case 0:
            case 1:

                gotEggs = 3;
                break;
            case 2:
            case 3:
                gotEggs = 5;
                break;
            case 4:
            case 5:
                gotEggs = 7;
                break;
            case 6:
                gotEggs = 10;
                break;
        }
        num++;

        PlayerPrefs.SetInt("rewardNum", num);
        DateTime now1 = DateTime.Now;
        string strDate = now1.ToBinary().ToString();
        // DateTime now2 = DateTime.FromBinary(long.Parse(strDate));

        PlayerPrefs.SetString("lastclaimtime", strDate);
        DateTime newTime = now1.AddHours(24);
        GameManager.instance.SetNotification("Don't miss out yoir daily rewards", "Get a new mask now 🐍 !!!", newTime);
       
        
        EggsManager.instance.IncreaseEgg(gotEggs);
        AchievementManager._instance.achivementUnlockText.text = "You got "+gotEggs.ToString() + " Eggs!";
        GameManager.instance.dialogBox.SetActive(true);
        //EggsManager.instance.ToggleDailyRewardBtns(false);
        this.gameObject.SetActive(false);
        
    }

    public void RefreshPage()
    {
        int num = PlayerPrefs.GetInt("rewardNum", 0);
        bool rewardAvail = CheckAvailableReward();
        for (int x = 0; x < 7; x++)
        {
            shineButtons[x].SetActive(false);
            claimButtons[x].SetActive(false);
            dailyRewards[x].color = normalRewardColor;
          //  claimedText[x].SetActive(false);
        //    selectedImg[x].sprite = selectedSpr;
        //    claimBtn[x].SetActive(false);


        }

        //  if (num > 4)
        //    num = 3;
        if (rewardAvail && num == 7)
        {
            PlayerPrefs.SetInt("rewardNum", 0);
            num = 0;
            //return;
        }

        for (int x = 0; x < num + 1; x++)
        {


            if (x == num && rewardAvail)
            {
                claimButtons[x].SetActive(true);
                shineButtons[x].SetActive(true);
                //     claimBtn[x].SetActive(true);
            }
            else if (x < num)
            {
                dailyRewards[x].color = takenRewardColor;
                //  claimedText[x].SetActive(true);
                //    selectedImg[x].sprite = unselectedSpr;
                //  claimBtn[x].SetActive(false);
            }

        }
    }
}
